-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2015 at 11:38 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vc_dial`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL,
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL,
  `user_agent` varchar(50) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_data` tinyint(50) NOT NULL,
  `logged_in` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=9053 ;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `session_id`, `ip_address`, `user_agent`, `last_activity`, `user_id`, `user_data`, `logged_in`) VALUES
(9042, 'f8b92ceb84777cc5b3d3cb78631c6e99', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437976216, 0, 0, 0),
(9043, 'e70cd9ceba52fb22c5a95129c70402c7', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437976846, 0, 0, 0),
(9044, '495ad2ded3d493b90e19da4664dd5353', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437976892, 0, 0, 0),
(9045, '3cbc5d2b6e912f211e690e1f838b2a6b', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437976940, 0, 0, 0),
(9046, '69a8c546557e9311c605675d148ba68d', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437977018, 0, 0, 0),
(9047, 'd9ba7f670a301290147d08d47a2be2cc', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437977103, 0, 0, 0),
(9048, 'c59ad9a2cea3e585fb9397b541630eab', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437977142, 0, 0, 0),
(9049, '8bd0166e6eaeab3e56f2027caf3bc2f4', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437977184, 0, 0, 0),
(9050, 'b6e5cf2c694f3af4215c04c287fe428a', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437977201, 0, 0, 0),
(9051, '0ccfc58193ba8d4a7b563d8b190fe836', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437977224, 0, 0, 0),
(9052, 'e85a83ddc86d28b28976c1fe2fe60189', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko', 1437977269, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
