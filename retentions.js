function start() {
    $.ajax({
        url: BASEPATH + 'c_complaints/allOpenInteractions',
        success: function(msg) {
            $('#divOpenIssues').html(msg)
        }
    })
}

   
jQuery(function($) {
    $.extend({
        serializeJSON: function(obj) {
            var t = typeof(obj);
            if(t != "object" || obj === null) {
                // simple data type
                if(t == "string") obj = '"' + obj + '"';
                return String(obj);
            } else {
                // array or object
                var json = [], arr = (obj && obj.constructor == Array);
                $.each(obj, function(k, v) {
                    t = typeof(v);
                    if(t == "string") v = '"' + v + '"';
                    else if (t == "object" & v !== null) v = $.serializeJSON(v)
                    json.push((arr ? "" : '"' + k + '":') + String(v));
                });
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    });
});

function checkBox(str, first, last) {
        var st=str.length;
        var b=str.search(first);
        var c=str.search(last);
        var s=b+3;
        var d=c-s;
        var a=str.substr(s,d);
        return a;
}


function showComplaint(complaint_id){
    $.ajax({
       url: BASEPATH + 'c_complaints/showComplaint/'+complaint_id+'/'+user_code,
       success: function(msg){
         //  $('#divInteractionDetails').html(msg);
        //   $('#divInteractionDetails').fadeIn(500);
       },
       error: function(){
         alert("Error");  
       }
   })
}


function showInteractionHistory(cust_id){
    if(cust_id != '') {
        var str='Complaints';
        $.ajax({
       url: BASEPATH + 'c_complaints/displayRetentionHistory/'+cust_id+'/'+str,
        success: function(msg){
            $('#divCustomerInteractionParticulars').html(msg);
        },
       error: function(){
         alert("Error");  
       }
   })
    }
}

function showfindInteractionHistory(retention_id){
//I have modified it from customer.js. Now it connects to table complaints_history
    $.ajax({
        url: BASEPATH + 'c_complaints/showfindInteractionHistory/'+retention_id,
        success: function(msg){
            $('#divInteractionDetails').html(msg);
        }
    })
}

function newCaseHistory(){
if ($('#tdRetentionComplaintID').index() > 0 ) {
    var retention_id=$('#inpInteractionHistoryID').val();
//    alert(retention_id);
//    return;
    $.ajax({
        url: BASEPATH + 'c_complaints/newCaseHistory/'+retention_id,
        success: function(msg){
            $('#divInteractionDataEntry').html(msg);
            $('#btnNewCaseHistory').fadeOut();
            if(msg=='Issue resolved') {
                $('#btnNewCaseHistory').fadeIn();
            }
            
            
        }
    })
    
    }
else {
    alert(0)
}    
}


function showIHDetails(interaction_id){
    alert(interaction_id)
}

function setNewStatus1(){
        $('#inpStatus1').val($('#ddStatus1 :selected').val());    
}

function setNewStatus2(){
        $('#inpStatus2').val($('#ddStatus2 :selected').val());    
}

function setOutcome1(){
        $('#inpOutcome1').val($('#ddOC1 :selected').text());    
}

function setOutcome2() {
        $('#inpOutcome2').val($('#ddOC2 :selected').text());    
}

function saveNewCase(){
    if($('#ddAllocatedTo :selected').val()== 0 || $('#ddAllocatedTo :selected').val()== '') {
        alert('Some data missing');
        return;
    }
    var ans=confirm('Press OK to save');
    if(ans==false) {
        return;
    }
    var interaction=$('#inpInteractionHistoryID').val()
   // var retention_id
    var x=$('#formNewCase').serializeArray();
    var d=$.serializeJSON(x); 
//    alert(d); return;
    $.ajax({
        url: BASEPATH + 'c_complaints/saveNewCase',
        type: 'POST',
        data:{'rpt' : d},
        success: function(msg){
            var str=checkBox(msg, '<x>', '</x>') ;
            if(str > 0){
            alert('Saved');
            showfindInteractionHistory(interaction);
            start(); 
            //$('#divCustomerInteractionParticulars').html('');
            //$('#divInteractionDetails').html('');
            
            
            $('#divInteractionDataEntry').html('');
            $('#btnNewCaseHistory').fadeIn();
        }
        else {
            alert('Could not save case history');
            $('#btnNewCaseHistory').fadeIn();
        }
        },
        error: function() {
            alert('Error');
        }
    })
}

function cancelNewCase(){
var ans=confirm('Press OK you want to abort new case');
if(ans==true){
    $('#divInteractionDataEntry').html('');
    $('#btnNewCaseHistory').fadeIn();
}
}

function selectAllocatedTo(){
    if($('#ddAllocatedTo :selected').val() > 0) {
        $('#inpAllocatedTo').val($('#ddAllocatedTo :selected').val());    
    }   
}

