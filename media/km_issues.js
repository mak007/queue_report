jQuery(function($) {
    $.extend({
        serializeJSON: function(obj) {
            var t = typeof(obj);
            if(t != "object" || obj === null) {
                // simple data type
                if(t == "string") obj = '"' + obj + '"';
                return String(obj);
            } else {
                // array or object
                var json = [], arr = (obj && obj.constructor == Array);
                $.each(obj, function(k, v) {
                    t = typeof(v);
                    if(t == "string") v = '"' + v + '"';
                    else if (t == "object" & v !== null) v = $.serializeJSON(v)
                    json.push((arr ? "" : '"' + k + '":') + String(v));
                });
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    });
});
//--------called from view------------//
function getInsertIssue(){
	
   
    $.ajax({
        url : BASEPATH + 'c_km_issues/getInsertIssue/',
        success: function(msg){
            $('#displayInsertIssue').html(msg);
			
        }
    })

}


function selectcid(){
    var val=$('#cid option:selected').val();
	$('#inpCid').val(val);	
	 $.ajax({
        url: BASEPATH + 'c_km_issues/selectedClient/'+val,
        type: 'POST',
    })
}


function selectpid(){
    var val=$('#pid option:selected').val();
	$('#inpPid').val(val);
}


function saveNewIssueForm(){	
	var x = $('#frm_SaveIssue').serializeArray();
    var d = $.serializeJSON(x);
	 
	 $.ajax({
        url: BASEPATH + 'c_km_issues/saveNewIssueForm',
        type: 'POST',
        data:{'rpt' : d},
        success: function(msg){
            alert('New Issue record created ');
        },
        error: function() {
            alert('Error');
        }
    })
    }

