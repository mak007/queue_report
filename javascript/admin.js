$(document).ready(function () {

    $('#inpEndDate1').datepicker({
        dateFormat: 'dd-mm-yy',
        numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateEndDate(selected);
            updateReportEndDate(selected);
            updateUsersLogEndDate(selected);

        }
    });
    $('#inpDateApproval').datepicker({
        dateFormat: 'dd-mm-yy',
        numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateInpDate(selected);
            updateReportStartDate(selected);
            updateUsersLogStartDate(selected);


        }
    });


    $('#off_time_end').timepicker({
        dateFormat: 'H:i:s',
        onSelect: function (selected, evnt) {
            //updateEndDate(selected);
        }
    });
    $('#off_time_start').timepicker({
        dateFormat: 'H:i:s',
        onSelect: function (selected, evnt) {
        }
    });

})
function updateInpDate(value) {
    $('#inpLogdate').val(value)
}
function updateEndDate(value) {
    $('#inpEnddate').val(value)
}
//////////////////
function updateReportStartDate(value) {
    $('#inpReportStartDate').val(value)
}
function updateReportEndDate(value) {
    $('#inpReportEndDate').val(value)
}
///////////////

function updateUsersLogStartDate(value) {
    $('#inpUserLogStartDate').val(value)
}
function updateUsersLogEndDate(value) {
    $('#inpUsersLogEndDate').val(value)
}
///////////////

//Following is not working but still add
$(function () {
    $("#divTable").dataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
    });
    /*    $('#example2').dataTable({
     "bPaginate": true,
     "bLengthChange": false,
     "bFilter": false,
     "bSort": true,
     "bInfo": true,
     "bAutoWidth": false
     }); */
});


function approvalList() {
    $.ajax({
        url: BASEPATH + 'c_admin/archivedApproval',
        success: function (msg) {
            $('#divArchive').html(msg);
        }
    })
}
function archivedPayment_pending_list() {
    $.ajax({
        url: BASEPATH + 'c_admin/archivedPayment_pending_list',
        success: function (msg) {
            $('#divArchive').html(msg);
        }
    })
}

function checkBox(str, first, last) {
    var st = str.length;
    var b = str.search(first);
    var c = str.search(last);
    var s = b + 3;
    var d = c - s;
    var a = str.substr(s, d);
    return a;
}



function approveOrder(order_id) {
    $.ajax({
        url: BASEPATH + 'c_admin/apprvoveOrder/' + order_id,
        success: function (msg) {
            $('#divApprovalOrder').html('');
            $('#divApprovalOrder').html(msg);
            //  $('#divApprovalOrder').fadeIn();
        }
    })
}
function apprvoveUnpaid_list(order_id) {
    $.ajax({
        url: BASEPATH + 'c_admin/apprvoveUnpaid_list/' + order_id,
        success: function (msg) {
            $('#divApprovalOrder').html('');
            $('#divApprovalOrder').html(msg);
            $('#inpRecdDate').datepicker({dateFormat: "dd-mm-yy"})

            //  $('#divApprovalOrder').fadeIn();
        }
    })
}

function showAdminPanelDiv() {
    $('#divWebSite').fadeIn(500);
}
function closeWebDiv() {
    $('#divWebSite').fadeOut(500);
}

jQuery(function ($) {
    $.extend({
        serializeJSON: function (obj) {
            var t = typeof (obj);
            if (t != "object" || obj === null) {
                // simple data type
                if (t == "string")
                    obj = '"' + obj + '"';
                return String(obj);
            } else {
                // array or object
                var json = [], arr = (obj && obj.constructor == Array);
                $.each(obj, function (k, v) {
                    t = typeof (v);
                    if (t == "string")
                        v = '"' + v + '"';
                    else if (t == "object" & v !== null)
                        v = $.serializeJSON(v)
                    json.push((arr ? "" : '"' + k + '":') + String(v));
                });
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    });
});


function listOpenComplaints() {
    $.ajax({
        url: BASEPATH + 'c_complaints/listOpenComplaints',
        success: function (msg) {
            $('#divStats').html(msg);
        }
    })
}

function showComplaint(complaint_id) {
    $.ajax({
        url: BASEPATH + 'c_complaints/showComplaint/' + complaint_id + '/' + user_code,
        success: function (msg) {
            $('#divDetails').html(msg);
            $('#divDetails').fadeIn(500);
            //    $('#inpDT').datepicker();
        },
        error: function () {
            alert("Error");
        }
    })
}
function showInterationHistory(cust_id) {
//    var cust_id=$('#inpCustID').val();
    if (cust_id != '') {
//     alert(cust_id)   
        $.ajax({
            url: BASEPATH + 'c_customer/displayInteractionHistory/' + cust_id,
            success: function (msg) {
                $('#divCurrentInteractions').html(msg);
            },
            error: function () {
                alert("Error");
            }
        })
    }
}
function  paramInteractionHistory(id) {
    //id of radio button;
    var cust_id = $('#inpCustID').val();
    $.ajax({
        url: BASEPATH + 'c_customer/paramInteractionHistory/' + id + '/' + cust_id,
        success: function (msg) {
            $('#divCurrentInteractions').html(msg);
        }

    })

}

function selectPaymentStatus() {
    $('#inpPaymentStatus').val($('#ddPayStatus :selected').text());
}

function selectPaymentMode() {
    $('#inpPaymentMode').val($('#ddPayMode :selected').val());
}

function cancelApprovedOrder() {
    var ans = confirm('Are you sure to cancel ?');
    if (ans == true) {
        $('#divApprovalOrder').html('');
    }
}

function saveApprovedOrder(order_id) {
    var x = $('#formApprovalOrder').serializeArray();
    var d = $.serializeJSON(x);
    //  alert(d);
    // return;
    $.ajax({
        url: BASEPATH + 'c_admin/saveApprovedOrder/' + order_id,
        type: 'POST',
        data: {'rpt': d},
        success: function (msg) {
            var str = checkBox(msg, '<x>', '</x>');
            alert(str)
            approvalList();
            $('#divApprovalOrder').html('');

        }
    })
}
function saveApprvoveUnpaid_list(order_id) {
    var x = $('#formApprovalOrder').serializeArray();
    var d = $.serializeJSON(x);
    //  alert(d);
    //   return;
    $.ajax({
        url: BASEPATH + 'c_admin/saveApprvoveUnpaid_list/' + order_id,
        type: 'POST',
        data: {'rpt': d},
        success: function (msg) {
            var str = checkBox(msg, '<x>', '</x>');
            alert(str)
            //archivedPayment_pending_list();

            $('#divApprovalOrder').html('');
            archivedPayment_pending_list();
        }
    })
}
function selectOrderStatus() {
    $('#inpOrderStatus').val($('#ddOrderStatus :selected').val())
}

function getApprovalOrdersForDate() {
    var x = $('#formDateUnpaid').serializeArray();
    var d = $.serializeJSON(x);
    alert(d);
    return;
    $.ajax({
        url: BASEPATH + 'c_admin/getApprovalOrdersForDate',
        type: 'POST',
        data: {'rpt': d},
        success: function (msg) {
            var str = checkBox(msg, '<x>', '</x>')
            $('#divArchive').html(str);
        }
    })
}
//---------Date: 1/12/2016  --- Mangesh kamble ---------------------------
function save_reports_config() {
    //var x = $('#report_config_form').serializeArray();
    //var d = $.serializeJSON(x);
    //alert(d);
    //return;
    var gmt_offset =encodeURI($('#gmt_offset').val());
    var sl_threshold=$('#sl_threshold').val();
    var sa_threshold=$('#sa_threshold').val();
    var refresh_rate=$('#refresh_rate').val();
    var did_no=$('#did_no').val();
    var off_time_start=encodeURI($('#off_time_start').val());
    var off_time_end=encodeURI($('#off_time_end').val());
    var arr={GMT_offset:gmt_offset,service_level_threshold:sl_threshold,short_abandon_threshould:sa_threshold,refresh_rate:refresh_rate,did_no:did_no,off_time_start:off_time_start,off_time_end:off_time_end};
    $.ajax({
        url: BASEPATH + 'c_report/saveReportConfig',
        type: 'POST',
        data: {'rpt': arr},
        async: false,
        success: function (msg) {
            alert('configurations updated successfully!!');
        }
    })
}
function getReportsConfig() {
    $.ajax({
        url: BASEPATH + 'c_report/getReportsConfig',
        async: false,
        success: function (response) {
            var data = jQuery.parseJSON(response);
            $('#gmt_offset').val(data[0].gmt_offset);
            $('#sl_threshold').val(data[0].slt);
            $('#sa_threshold').val(data[0].sat);
            $('#refresh_rate').val(data[0].rr);
            $('#did_no').val(data[0].did_no);
            $('#off_time_start').val(data[0].off_time_start);
            $('#off_time_end').val(data[0].off_time_end);
        }
    })
}

function save_addUser() {
    //var x = $('#report_addUser').serializeArray();
    var name = $('#agent_name').val();
    var extn = $('#extn').val();
    var obteam = $('#oblist').val();
    var queue = $('#queuename').val();
    if (obteam == '"0"') {
        alert('Please select atleast one team!!');
        return;
    }
    if (name == '') {
        alert('Username is missing!!');
        return;
    }
    if (extn == '') {
        alert('Extension missing!!');
        return;
    }
    if (queue == '"0"') {
        alert('Please select only single Queue!!');
        return;
    }
    var arr = {name: name, extn: extn, queue: queue,obteam:obteam}
    $.ajax({
        url: BASEPATH + 'c_report/save_addUser',
        type: 'POST',
        data: {'rpt': arr},
        async: false,
        success: function (msg) {
            //alert(msg);
            $('#agent_name').val('');
            $('#extn').val('');
            $('#queuename').val('');
            $('#obteam').val('');
            alert('User Added successfully!!');
        }
    });
}

function getUserList() {
    $("#tbodyUser").dataTable().fnDestroy();    
    $('#UserList').empty();
    $.ajax({
        url: BASEPATH + 'c_report/getUserList',
        success: function (msg) {
            $('#UserList').html(msg);
            $('#tbodyUser').DataTable({
            });
        }

    })
}
function edit_user(id) {
    document.getElementById("agent_name" + id).removeAttribute("readonly");
    document.getElementById("iqueue_name" + id).removeAttribute("readonly");
    document.getElementById("ob_team" + id).removeAttribute("readonly");
    $('#edit' + id).hide();
    $('#save' + id).show();
}
function save_user(id) {
    var agent = $("#agent_name" + id).val();
    var iqueue_name = $("#iqueue_name" + id).val();
    var extension_name = $("#extension_name" + id).val();
    var ob_team = $("#ob_team" + id).val();
    var arr = {agent: agent, iqueue_name: iqueue_name, extension_name: extension_name,ob_team:ob_team}
    $.ajax({
        url: BASEPATH + 'c_report/save_user',
        type: 'post',
        data: {'rpt': arr},
        success: function (msg) {
            alert('User data updated successfully');
            document.getElementById("agent_name" + id).readOnly = true;
            document.getElementById("iqueue_name" + id).readOnly = true;
            document.getElementById("ob_team" + id).readOnly = true;
            $('#edit' + id).show();
            $('#save' + id).hide();
            getUserList();
        }

    })

}

/*Added code by Rajnandini*/

function save_addDid() {
    var conf_name = $('#conf_name').val();
    var did = $('#did').val();
   
    if (conf_name == '') {
        alert('Conference name is missing!!');
        return;
    }
    if (did == '') {
        alert('DID no is missing!!');
        return;
    }
    
    var arr = {conf_name: conf_name, did: did}
    $.ajax({
        url: BASEPATH + 'c_report/save_addDid',
        type: 'POST',
        data: {'rpt': arr},
        async: false,
        success: function (msg) {
            $('#conf_name').val('');
            $('#did').val('');
            alert('Added successfully!!');
        }
    });
}

function save_addContact() {
    var didno_list = $("#select_didno_list option:selected").val();
    var contact_no = $('#contact_no').val();
    if (didno_list == '' || didno_list==0) {
        alert('Select DID No!!');
        return;
    }
    if (contact_no == '') {
        alert('Contact no is missing!!');
        return;
    }
    
    var arr = {didno_list: didno_list, contact_no: contact_no}
    $.ajax({
        url: BASEPATH + 'c_report/save_addContact',
        type: 'POST',
        data: {'rpt': arr},
        async: false,
        success: function (msg) {
            $('#contact_no').val('');
            alert('Added successfully!!');
        }
    });
}

function select_didno(){
        $.ajax({
         url: BASEPATH + 'c_report/get_didno_list',
        success: function (response) {
                var mySelect = $('#select_didno_list');
                mySelect.empty();

                var val, text;
                var data = jQuery.parseJSON(response);
                var x = 0;
                mySelect.append(
                        $('<option></option>').val(x).html('--All DID No--')
                        );
                for (var i = 0; i < data.length; i++)
                {
                    val = data[i].confid;
                    text = data[i].conf_name;

                    mySelect.append(
                            $('<option></option>').val(val).html(text)
                            );
                }
        }
    })
    }
    
    function getDidContact(){
        var conf_id = $('#select_didno_list').val();
        $.ajax({
         url: BASEPATH + 'c_report/get_didcontact_List/'+conf_id,
        success: function (response) {
                var tableData = "";
                $("#displayContact").empty();
                var data = jQuery.parseJSON(response);
                tableData+='<form id="confContact">';
                for (var i = 0; i < data.length; i++) {
                    tableData += '<input readonly type="text" value="'+data[i].contact_no+'" name="display_contact'+i+'" id="display_contact'+i+'">&nbsp&nbsp';
                    tableData += '<i onclick="deleteContact('+data[i].id+');" class="fa fa-fw fa-trash-o"></i>';

                }
                tableData += '<br><br><button type="button" onclick="initiate_conference();" id="call_conference" name="call_conference">Initiate Conference </button>';
                tableData+='</form>';
                $("#displayContact").html(tableData);
        }
    })
    }
    
    function initiate_conference(j) {
    var didno = $("#select_didno_list option:selected").val();
    var x = $('#confContact').serializeArray();
    var d = $.serializeJSON(x);    
    $.ajax({
        url: BASEPATH + 'c_report/start_conference/'+didno,
        type: 'POST',
        data: {'rpt': d},
        async: false,
        success: function (msg) {
            //toastr.success('Confer Disconnected..');
           
        }
    });
}

    function deleteContact(id) {
    $.ajax({
        url: BASEPATH + 'c_report/deleteContact/'+id,
        success: function (msg) {
            getDidContact();
            alert('Contact deleted successfully..');
            
        }
    });
}
    
    /*end here*/
