jQuery(function ($) {
    $.extend({
        serializeJSON: function (obj) {
            var t = typeof (obj);
            if (t != "object" || obj === null) {
                // simple data type
                if (t == "string")
                    obj = '"' + obj + '"';
                return String(obj);
            } else {
                // array or object
                var json = [], arr = (obj && obj.constructor == Array);
                $.each(obj, function (k, v) {
                    t = typeof (v);
                    if (t == "string")
                        v = '"' + v + '"';
                    else if (t == "object" & v !== null)
                        v = $.serializeJSON(v)
                    json.push((arr ? "" : '"' + k + '":') + String(v));
                });
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    });
});

function bridge_login(){
    var username = $('#bridge_username').val();
    var password = $('#bridge_password').val();
    if(username==''){
        alert("Please Enter the Username");
        $('#bridge_username').css('border','1px solid #e93939');// Username border as red
        return;
    }if(password==''){
        alert("Please Enter the Password");
        $('#bridge_password').css('border','1px solid #e93939');// Password border as red
        return;
    }
    var dataString = {u_name: username,u_pass:password};
    $.ajax({
        url: BASEPATH + 'c_bridge/bridge_login_check',
        type: 'POST',
        data: {data: dataString},
        success: function (msg){
            if(msg){
                $("#queue-report-login-page-div").fadeOut(400);
                $("#queue-report-bridge-menu-page-div").delay(450).fadeIn(400);
                get_did_number();
                get_did_number_forReset();
            }else{
                alert("Invalid Username or Password !");
                $('#bridge_username').css('border','1px solid #e93939');// Username border as red
                $('#bridge_password').css('border','1px solid #e93939');// Password border as red
            }
        }
    });
}

function get_did_number(){
    $('#bridge_did_numr').empty(); // Empty the DID number dropdown of reset bridge password
    $('#bridge_did_numr4Contact').empty();; // Empty the DID number dropdown of Add contact
    $('#select_didno_list_panel').empty();; // Empty the DID number dropdown of Add contact
    $.ajax({
        url: BASEPATH + 'c_bridge/get_did_number',
        type: 'POST',
        success: function (msg) {
            $('#bridge_did_numr').html(msg); // For Reset Password Form
            $('#select_didno_list').html(msg); // For Add Contact Form
            $('#select_didno_list_panel').html(msg); // For Add Control Panel Form
        }
    });
}

function get_did_number_forReset(){
    $('#bridge_did_numr').empty(); // Empty the DID number dropdown of reset bridge password
    $('#bridge_did_numr4Contact').empty();; // Empty the DID number dropdown of Add contact
    $('#select_didno_list_panel').empty();; // Empty the DID number dropdown of Add contact
    $.ajax({
        url: BASEPATH + 'c_bridge/get_did_number_forReset',
        type: 'POST',
        success: function (msg) {
            $('#bridge_did_numr').html(msg); // For Reset Password Form
        }
    });
}

/* Checking the all Empty field and validation OLd password Correct or not.
 * if Not then shows alert if coreect then password rest*/
function bridge_reset_pass(){
    
    //var u_did_no= $('#bridge_did_numr').val();
    var u_did_no= $("#bridge_did_numr option:selected").val();
    var u_old_pwd= $('#bridge_old_password').val();
    var u_new_pass= $('#bridge_new_password').val();
    var u_confirm_pass= $('#bridge_confirm_password').val();
    
    if(u_did_no == '')
    {
        alert('Please Enter DID Number');
        $('#bridge_did_numr').css('border-color','red');
        return;
    }
     if(u_old_pwd == '')
    {
        alert('Please Enter Old Password');
        $('#bridge_old_password').css('border-color','red');
        return;
    }
    if(u_new_pass == '')
    {
        alert('Please Enter New Password');
        $('#bridge_new_password').css('border-color','red');
        return;
    }
    if(u_confirm_pass == '')
    {
        alert('Please Enter Confirm Password');
        $('#bridge_confirm_password').css('border-color','red');
        return;
    }
    var dataString = {u_did_no: u_did_no, u_old_pwd: u_old_pwd,u_new_pass:u_new_pass,u_confirm_pass:u_confirm_pass};
    $.ajax({
        url: BASEPATH + 'c_bridge/bridge_reset_pass',
        type: 'POST',
        data: {data: dataString},
        success: function (msg) {
            if(msg==1){
                alert("Password Reset Successfull");
                $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val(''); // All input field empty
                $('#errorMessage').html('');// Error Message empty
            }else{
                alert("please Enter the correct Old Password");
            }
        }
    });
}

function save_addDid() {
    var conf_name = $('#conf_name').val();
    var did = $('#did').val();
   
    if (conf_name == '') {
        alert('Please enter conference name.');
        return;
    }
    if (did == '') {
        alert('Please enter DID number.');
        return;
    }
    var arr = {conf_name: conf_name, did: did}
    $.ajax({
        url: BASEPATH + 'c_bridge/save_addDid',
        type: 'POST',
        data: {'rpt': arr},
        async: false,
        success: function (msg) {
            if(msg){
                $('#conf_name').val('');
                $('#did').val('');
                alert('DID number Added successfully!!');
            }else{
                alert(" Error DID Noumber not added ");
            }
        }
    });
}

function save_addContact() {
    var didno_list = $("#select_didno_list option:selected").val();
    var contact_no = $('#contact_no').val();
    if (didno_list == '' || didno_list==0) {
        alert('Select DID No!!');
        return;
    }
    if (contact_no == '') {
        alert('Contact no is missing!!');
        return;
    }
    
    var arr = {didno_list: didno_list, contact_no: contact_no}
    $.ajax({
        url: BASEPATH + 'c_bridge/save_addContact',
        type: 'POST',
        data: {'rpt': arr},
        async: false,
        success: function (msg) {
            $('#contact_no').val('');
            alert('Added successfully!!');
        }
    });
}

function getDidContact(){
    var conf_id = $('#select_didno_list_panel').val();
    $.ajax({
        url: BASEPATH + 'c_bridge/get_didcontact_List/' + conf_id,
        success: function (response) {
            var data = jQuery.parseJSON(response);
            var tableData = "";
            $("#displayContactNumber").empty();
            tableData+='<form id="confContact">';
            for (var i = 0; i < data.length; i++) {
                tableData += '<div class="col-sm-3"><div class="input-group">';
                tableData += '<input type="text" class="form-control" value="' + data[i].contact_no + '" name="display_contact' + i + '" id="display_contact' + i + '">';
                tableData += '<span class="input-group-addon delete'+data[i].id+'" onclick="deleteContact('+data[i].id+');"><i class="glyphicon glyphicon-remove"></i></span>';
                tableData += '</div><br></div>';
            }
            tableData +='</form>';
            $("#displayContactNumber").html(tableData);
        }
    });
}

function initiate_conference() {
    var didno = $("#select_didno_list_panel option:selected").val();
    var x = $('#confContact').serializeArray();
    var d = $.serializeJSON(x);
    $.ajax({
        url: BASEPATH + 'c_bridge/start_conference/' + didno,
        type: 'POST',
        data: {'rpt': d},
        async: false,
        success: function (msg) {
            //toastr.success('Confer Disconnected..');
        }
    });
}

$('#bridge_confirm_password').on('keyup', function () {
    if ($('#bridge_new_password').val() == $('#bridge_confirm_password').val())
    {
      $('#errorMessage').html('Password matching').css('color', 'green');
    }else{
      $('#errorMessage').html('Password not matching').css('color', 'red');
    }
});

function deleteContact(e) {
    //alert(e);return;
    var checkstr = confirm('Are you sure to remove this contact..?');
    if (checkstr == true) {
        $(".delete"+e).parent().parent().remove();
    } else {
        return false;
    }
}
