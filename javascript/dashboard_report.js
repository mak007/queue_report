jQuery(function ($) {
    $.extend({
        serializeJSON: function (obj) {
            var t = typeof (obj);
            if (t != "object" || obj === null) {
                // simple data type
                if (t == "string")
                    obj = '"' + obj + '"';
                return String(obj);
            } else {
                // array or object
                var json = [], arr = (obj && obj.constructor == Array);
                $.each(obj, function (k, v) {
                    t = typeof (v);
                    if (t == "string")
                        v = '"' + v + '"';
                    else if (t == "object" & v !== null)
                        v = $.serializeJSON(v)
                    json.push((arr ? "" : '"' + k + '":') + String(v));
                });
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    });
});
var refresh=8000;
function init() {
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getrefreshrate',
        async: false,
        success: function (getrefreshrate) {
            select_queue();
            getForTheDayData();
            refresh=getrefreshrate;
            getCurrentIntervalData();
            //getGraphData();
           /* Commented By Mangesh on 25-1-17*/
           //getRealtimeQueueAPIData();
        }
    });
}

function select_queue(){
        $.ajax({
        url: BASEPATH + 'c_outbound_report_cdr/get_queue_list',
        success: function(msg){
            $('#select_queue_list').html(msg);
        }
        })
    }

function select_queue1(){
        $.ajax({
        url: BASEPATH + 'c_outbound_report_cdr/get_queue_list1',
        success: function(msg){
            $('#select_queue_list').html(msg);
        }
        })
    }

//function getQueuename() {
//    $.ajax({
//        url: BASEPATH + 'c_dashboard_report/getQueuename',
//        success: function (response) {  
//            $('#queueDiv').html(str);
//        }
//    })
//}
function getForTheDayData() {
    var queue = $('#select_queue_list').val();
    if (queue == null) {
        queue = '"0"';
    }
    var d = encodeURI(queue);
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getForTheDayData',
        type: 'POST',
        data: {'rpt': d},
        success: function (response) {
            var data = jQuery.parseJSON(response);
            $('#coftd').text(data[0].coftd);
            $('#caftd').text(data[0].caftd);
            $('#slftd').text(data[0].slftd);
            $('#ciqftd').text(data[0].ciqftd);
            $('#abftd').text(data[0].abftd);
            $('#ablftd').text(data[0].ablftd);
            $('#anslftd').text(data[0].anslftd);
            $('#asa_ftd').text(data[0].asa);
            $('#att_ftd').text(data[0].att);
        }
    })
 //   setTimeout(getForTheDayData, 180000);
}
function getCurrentIntervalData() {
    var queue = $('#select_queue_list').val();
    if (queue == null) {
        queue = '"0"';
    }
    var d = encodeURI(queue);
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getCurrentIntervalData',
        type: 'POST',
        data: {'rpt': d},
        success: function (response) {
            var data = jQuery.parseJSON(response);
            $('#cofci').text(data[0].coftd);
            $('#cafci').text(data[0].caftd);
            $('#slfci').text(data[0].slftd);
            $('#ciqfci').text(data[0].ciqftd);
            $('#abfci').text(data[0].abftd);
            $('#ablfci').text(data[0].ablftd);
            $('#anslfci').text(data[0].anslftd);
            $('#asa_ci').text(data[0].asa);
            $('#att_ci').text(data[0].att);
            if(data[0].refreshFTD==1){
                getForTheDayData();
            }
        }
    })
    setTimeout(getCurrentIntervalData, refresh);
}

function getGraphData() {
    var queue = $('#select_queue_list').val();
    if (queue == null) {
        queue = '"0"';
    }
    var d = encodeURI(queue);
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getGraphData',
        type: 'POST',
        data: {'rpt': d},
        success: function (response) {


            var data = jQuery.parseJSON(response);
            var co = [];
            var ca = [];
            var sl = [];
            var y  = [];
            for (var i = 0; i < data.length; i++) {
                co.push(Number(data[i].calls_offered));
                ca.push(Number(data[i].calls_answered));
                sl.push(Number(data[i].service_level));
                y.push(data[i].date1);
            }
        var chart=null;
        chart=new Highcharts.Chart('container', {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Service Level Details'
        },
        subtitle: {
            text: 'Calls Offered VS Calls Answered'
        },
        xAxis: [{
            categories: y,
            crosshair: true
        }],
        yAxis: [{ // Secondary yAxis
            title: {
                text: 'Service Level ',
                style: {
                    color: Highcharts.getOptions().colors[8]
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        },{ // Primary yAxis
            labels: {
                format: '{value} Calls',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'No of Calls',
                style: {
                    color: Highcharts.getOptions().colors[8]
                }
            }
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Calls Offered',
            type: 'column',
            yAxis: 1,
            data: co,
            tooltip: {
                valueSuffix: ' calls'
            }

        },{
            name: 'Calls Answered',
            type: 'column',
            yAxis: 1,
            data: ca,
            tooltip: {
                valueSuffix: 'calls'
            }

        }, {
            name: 'Service Level',
            type: 'spline',
            data: sl,
            tooltip: {
                valueSuffix: '%'
            }
        }]
    });


        }
    });
    setTimeout(getGraphData, refresh);
}

function getRealtimeQueueAPIData(){
    var queue = $('#select_queue_list').val();
    if (queue == null || queue==0) {
        queue = '"0"';
    }
    //alert(queue);
    var d = encodeURI(queue);
    
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getRealtimeQueueAPIData',
        type: 'POST',
        data: {'rpt': d},
        success: function (response) {
            var data = jQuery.parseJSON(response);  
            $('#total_login').text(data[0].total_login);
            $('#count_pause').text(data[0].count_pause);
            $('#count_incall').text(data[0].count_incall);
            $('#total_hold').text(data[0].total_hold);
            $('#total_ready').text(data[0].total_ready);
        }
    });
     setTimeout(getRealtimeQueueAPIData, 9000);
}

var refresh_rtm=8000;
function init_rtm() {
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getrefreshrate',
        async: false,
        success: function (getrefreshrate) {
            refresh_rtm = getrefreshrate;
            display_rtm_report(refresh_rtm);
            display_rtm_Extnreport(refresh_rtm);
            getLiveExtensionDetails();
        }
    });
}

function display_rtm_report(getrefreshrate){
    var queue = $('#select_queue_list').val();
    if(queue=="--Select--"){
        queue='';
    }
    $.ajax({
        type:'POST',
        data:{'queue':queue},
        url: BASEPATH + 'c_dashboard_report/display_rtm_report',
        success: function (response) {
        var tabledata="";
        if(response!=false){
            var data1 = jQuery.parseJSON(response);
            var data = data1.rtm;  
            var x= data1.queue;
            for(var i=0;i<data.length;i++){
//                tabledata += "<tr>";
//                if(data[i].extension_no==0){
//                tabledata += "<td>-</td>";    
//                }else{
//                tabledata += "<td>"+data[i].extension_no+"</td>";
//                }
//                if(data[i].active_call==null){
//                tabledata += "<td>-</td>";    
//                }else{
//                tabledata += "<td>"+data[i].active_call+"</td>";
//                }
//                if(data[i].on_call==null){
//                tabledata += "<td>-</td>";    
//                }else{
//                tabledata += "<td>"+data[i].on_call+"</td>";
//                }
//                if(data[i].other_queue==null){
//                tabledata += "<td>-</td>";    
//                }else{
//                tabledata += "<td>"+data[i].other_queue+"</td>";
//                }
//                if(data[i].pause_call==null){
//                tabledata += "<td>-</td>";    
//                }else{
//                tabledata += "<td>"+data[i].pause_call+"</td>";
//                }
//                if(data[i].acw_call==null){
//                tabledata += "<td>-</td>";    
//                }else{
//                tabledata += "<td>"+data[i].acw_call+"</td>";
//                }
//                if(data1.queue==0){
//                tabledata += "<td>-</td>";    
//                }else{
//                tabledata += "<td>"+data1.queue+"</td>";
//                }
//                tabledata += "</tr>";
                
                
                
                
                $("#logged_in").text(data[i].extension_no); 
                $("#available").text(data[i].active_call); 
                $("#available").text(data[i].active_call); 
                $("#on_call").text(data[i].on_call); 
                $("#other_queue").text(data[i].other_queue);
                $("#pause").text(data[i].pause_call);
                $("#acw").text(data[i].acw_call);
                $("#call_in_queue").text(x);




            }
        }else{
            tabledata += "<tr>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "</tr>";
        }
            $('#rtm_tbl_body').html(tabledata);
            setTimeout(display_rtm_report, refresh_rtm);
        }
    })
}

function display_rtm_Extnreport(getrefreshrate){
    //$("#extn_tbl").dataTable().fnDestroy();
    var queue = $('#select_queue_list').val();
    if(queue=="--Select--"){
        queue='';
    }
    $.ajax({
        type:'POST',
        data:{'queue':queue},
        url: BASEPATH + 'c_dashboard_report/display_rtm_Extnreport',
        success: function (response) {
            if(response!=false){
            var data = jQuery.parseJSON(response);
            var tabledata="";
            
                for(var i=0;i<data.length;i++){
                    tabledata += "<tr>";
                    tabledata += "<td>"+data[i].extension_no+"</td>";
                    if(data[i].status=="ACTIVE"){
                        tabledata += '<td class="text-center bg-success"><h5 class="insideTable-icons-RTM"><span class="glyphicon glyphicon-ok text-info" aria-hidden="true" title="Available"></span></h5></td>';
                    }
                    else if(data[i].status=="PAUSE"){
                        tabledata += '<td class="text-center bg-danger"><h5 class="insideTable-icons-RTM"><span class="glyphicon glyphicon-pause text-danger" aria-hidden="true" title="Pause"></span></h5></td>';
                    }
                    else if(data[i].status=="ONCALL"){
                        tabledata += '<td class="text-center bg-warning"><h5 class="insideTable-icons-RTM"><span class="glyphicon glyphicon-headphones text-warning" aria-hidden="true" title="On Call"></span></h5></td>';
                    }
                    else if(data[i].status=="ACW"){
                        tabledata += '<td class="text-center bg-info"><h5 class="insideTable-icons-RTM"><span class="glyphicon glyphicon-file text-primary" aria-hidden="true" title="After Call Work"></span></h5></td>';
                    }
                    else if(data[i].status=="Other_Queue"){
                        tabledata += '<td><span class="label label-default">Other Queue</span></td>';
                    }
                    tabledata += "<td class='text-right'>"+data[i].time+"</td>";
                    tabledata += "</tr>";
                }
            }else{
                tabledata = "<tr>";
                tabledata += "<td> - </td>";
                tabledata += "<td> - </td>";
                tabledata += "<td> - </td>";
                tabledata += "</tr>";
            }
            //$("#extn_tbl_body").empty();
            
            $('#extn_tbl_body').html(tabledata);
            /*$("#extn_tbl").DataTable({
                  "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]] ,
            });*/
            
            setTimeout(display_rtm_Extnreport, refresh_rtm);
        }
    })
}

function getLiveExtensionDetails(){
    var queue = $('#select_queue_list').val();
    if(queue=="--Select--"){
        queue='';
    }
    $.ajax({
        type:'POST',
        data:{'queue':queue},
        async:false,
        url: BASEPATH + 'c_dashboard_report/getLiveExtensionDetails',
        success: function (response) {
            $('#real_time_extn_status').html(response);  
            setTimeout(getLiveExtensionDetails, 7000);
        }
    })
}


/**/
function display_pie_chart_data(){
    var queue = $('#select_queue_list').val();
    if(queue=="--Select--"){
        queue='';
    }
    /*$.ajax({
        type:'POST',
        data:{'queue':queue},
        async:false,
        url: BASEPATH + 'c_dashboard_report/display_pie_chart_data',
        success: function (response) {
            
        }
    })*/
    /**/
    
    var gaugeOptions = {

        chart: {
            type: 'solidgauge'
        },

        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {
            stops: [
                [0.1, '#DF5353'], // green#DF5353
                [0.7, '#DDDF0D'], // yellow
                [0.8, '#55BF3B'] // red#55BF3B
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            title: {
                y: -70
            },
            labels: {
                y: 16
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    // The speed gauge
    var chartSpeed = Highcharts.chart('container-speed', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Service Level'
            }
        },

        credits: {
            enabled: false
        },

        series: [{
            name: 'SL',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">%</span></div>'
            },
            tooltip: {
                valueSuffix: ' call/h'
            }
        }]

    }));

    // The RPM gauge
    var chartRpm = Highcharts.chart('container-rpm', Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Answer Level'
            }
        },

        series: [{
            name: 'AL',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">%</span></div>'
            },
            tooltip: {
                valueSuffix: ' call/min'
            }
        }]

    }));

    // Bring life to the dials
    setInterval(function () {
        // Speed
        var point,point1,
            newVal,
            inc;
            
        if (chartSpeed) {
            point1 = chartSpeed.series[0].points[0];
            /*inc = Math.round((Math.random() - 0.5) * 100);
            newVal = point.y + inc;

            if (newVal < 0 || newVal > 100) {
                newVal = point.y - inc;
            }

            point.update(newVal);*/
            $.ajax({
                type:'POST',
                data:{'queue':queue},
                url: BASEPATH + 'c_dashboard_report/get_sl_data',
                success: function (response) {
                    response = Number(response);
                    response = Math.round(response);
                    point1.update(response);
                }
            })
        }

        // RPM
        if (chartRpm) {
            point = chartRpm.series[0].points[0];
            /*inc = Math.round((Math.random() - 0.5) * 100);
            newVal = point.y + inc;

            if (newVal < 0 || newVal > 100) {
                newVal = point.y - inc;
            }

            point.update(100);*/
            $.ajax({
                type:'POST',
                data:{'queue':queue},
                url: BASEPATH + 'c_dashboard_report/get_al_data',
                success: function (response) {
                    response = Number(response);
                    response = Math.round(response);
                    point.update(response);
                }
            })
        }
    }, 8000);
    
    /**/
}

var refresh_rtm_v1=8000;
function init_rtm_v1() {
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getrefreshrate',
        async: false,
        success: function (getrefreshrate) {
            refresh_rtm_v1 = getrefreshrate;
            display_rtm_report_v1(refresh_rtm_v1);
            display_rtm_Extnreport_v1(refresh_rtm_v1);
            getForTheDayData_v1(refresh_rtm_v1);
            getCurrentIntervalData_v1(refresh_rtm_v1);
            //getLiveExtensionDetails();
        }
    });
}

function display_rtm_Extnreport_v1(getrefresh_rtm_v1){
    //$("#extn_tbl").dataTable().fnDestroy();
    var queue = $('#select_queue_list').val();
    var dropdown_sort = $('#dropdown_sort').val();
    var dropdown_order_by = $('#dropdown_order_by').val();
    if(queue=="--Select--"){
        queue='';
    }
    $.ajax({
        type:'POST',
        data:{'queue':queue,'dropdown_sort':dropdown_sort,'dropdown_order_by':dropdown_order_by},
        url: BASEPATH + 'c_dashboard_report/display_rtm_Extnreport_v1',
        success: function (response) {
            if(response!=false){
            var data = jQuery.parseJSON(response);
            var tabledata="";
            
                for(var i=0;i<data.length;i++){
                    tabledata += "<tr>";
                    tabledata += "<td>"+data[i].extension_no+"</td>";
                    /*if(data[i].status=="ACTIVE"){
                        tabledata += '<td class="text-center bg-success"><h5 class="insideTable-icons-RTM"><span class="glyphicon glyphicon-ok text-info" aria-hidden="true" title="Available"></span></h5></td>';
                    }
                    else if(data[i].status=="PAUSE"){
                        tabledata += '<td class="text-center bg-danger"><h5 class="insideTable-icons-RTM"><span class="glyphicon glyphicon-pause text-danger" aria-hidden="true" title="Pause"></span></h5></td>';
                    }
                    else if(data[i].status=="ONCALL"){
                        tabledata += '<td class="text-center bg-warning"><h5 class="insideTable-icons-RTM"><span class="glyphicon glyphicon-headphones text-warning" aria-hidden="true" title="On Call"></span></h5></td>';
                    }
                    else if(data[i].status=="ACW"){
                        tabledata += '<td class="text-center bg-info"><h5 class="insideTable-icons-RTM"><span class="glyphicon glyphicon-file text-primary" aria-hidden="true" title="After Call Work"></span></h5></td>';
                    }
                    else if(data[i].status=="Other_Queue"){
                        tabledata += '<td><span class="label label-default">Other Queue</span></td>';
                    }*/
                    tabledata += "<td class='text-right'>"+data[i].status+"</td>";
                    tabledata += "<td class='text-right'>"+data[i].time+"</td>";
                    tabledata += "<td class='text-right'>"+data[i].queue+"</td>";
                    tabledata += "</tr>";
                }
            }else{
                tabledata = "<tr>";
                tabledata += "<td> - </td>";
                tabledata += "<td> - </td>";
                tabledata += "<td> - </td>";
                tabledata += "<td> - </td>";
                tabledata += "</tr>";
            }
            //$("#extn_tbl_body").empty();
            
            $('#extn_tbl_body').html(tabledata);
            /*$("#extn_tbl").DataTable({
                  "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]] ,
            });*/
            
            setTimeout(display_rtm_Extnreport_v1, refresh_rtm_v1);
        }
    })
}

function display_rtm_report_v1(getrefresh_rtm_v1){
    var queue = $('#select_queue_list').val();
    if(queue=="--Select--"){
        queue='';
    }
    $.ajax({
        type:'POST',
        data:{'queue':queue},
        url: BASEPATH + 'c_dashboard_report/display_rtm_report_v1',
        success: function (response) {
        var tabledata="";
        if(response!=false){
            var data1 = jQuery.parseJSON(response);
            var data = data1.rtm;  
            var x= data1.queue;
            for(var i=0;i<data.length;i++){
                $("#ag_logged_in").text(data[i].extension_no); 
                $("#ag_on_available").text(data[i].active_call); 
                //$("#available").text(data[i].active_call); 
                $("#ag_on_call").text(data[i].on_call); 
                $("#other_queue").text(data[i].other_queue);
                $("#ag_on_pause").text(data[i].pause_call);
                $("#ag_on_acw").text(data[i].acw_call);
                //$("#call_in_queue").text(x);
            }
        }else{
            tabledata += "<tr>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "<td>-</td>";
            tabledata += "</tr>";
        }
            $('#rtm_tbl_body').html(tabledata);
            setTimeout(display_rtm_report_v1, refresh_rtm_v1);
        }
    })
}

function getForTheDayData_v1(getrefresh_rtm_v1) {
    var queue = $('#select_queue_list').val();
    if (queue=="--Select--" || queue == null) {
        queue = '"0"';
    }
    var d = encodeURI(queue);
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getForTheDayData_v1',
        type: 'POST',
        data: {'rpt': d},
        success: function (response) {
            var data = jQuery.parseJSON(response);
            $('#calls_off_ftd_v1').text(data[0].coftd);
            $('#calls_ans_ftd_v1').text(data[0].caftd);
            $('#calls_abn_ftd_v1').text(data[0].abftd);
            $('#att_ftd_v1').text(data[0].att);
        }
    })
    setTimeout(getForTheDayData_v1, refresh_rtm_v1);
}

function getCurrentIntervalData_v1(getrefresh_rtm_v1) {
    var queue = $('#select_queue_list').val();
    if (queue == null) {
        queue = '"0"';
    }
    var d = encodeURI(queue);
    $.ajax({
        url: BASEPATH + 'c_dashboard_report/getCurrentIntervalData_v1',
        type: 'POST',
        data: {'rpt': d},
        success: function (response) {
            var data = jQuery.parseJSON(response);
            $('#call_in_sl_int').text(data[0].call_in_sl);
            $('#call_off_int').text(data[0].coftd);
            $('#call_ans_int').text(data[0].caftd);
            $('#sl_int').text(data[0].slftd);
            $('#call_wt_int').text(data[0].ciqftd);     //call waiting
            $('#abn_int').text(data[0].abftd);
            $('#call_abnl_int').text(data[0].ablftd);
            $('#call_al_int').text(data[0].anslftd);
            //$('#asa_ci').text(data[0].asa);
            $('#att_int').text(data[0].att);
            $('#ocw_int').text(data[0].ocw);
            
            if(data[0].refreshFTD==1){
                getForTheDayData_v1(refresh_rtm_v1);
            }
        }
    })
    setTimeout(getCurrentIntervalData_v1, refresh_rtm_v1);
}

/**/