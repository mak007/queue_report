$(document).ready(function () {

    $('#datepickerStartDate').datetimepicker({
        //dateFormat: 'dd-mm-yy',
        //numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateStDate(selected);
        }
    });
    $('#datepickerEndDate').datetimepicker({
        //dateFormat: 'dd-mm-yy',
        //numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateEnDate(selected);
        }
    });

});

function updateStDate(value){
   $('#StartDate').val(value);
   /* Added by Mohan on : 03-01-17 
    * updating Start date on Call Received (Specific Extension OR DID) REOPRT*/
   $('#StartDate_callrecived').val(value);
   /* End of mohan code*/
}

function updateEnDate(value){
   $('#EndDate').val(value);
   /* Added by Mohan on : 03-01-17 
    * updating END date on Call Received (Specific Extension OR DID) REOPRT*/
   $('#EndDate_callrecived').val(value);
}

/* Added By Mohan on : 03-01-17
 * To set the Call To number value  to hidden field to pass the table 
 * for getting the records againts this destination number*/
function set_Callto_val(value){
     $('#callto').val(value) ;
}
/*====End of Mohan code===*/

jQuery(function() {
    $.extend({
        serializeJSON: function(obj) {
            var t = typeof(obj);
            if(t != "object" || obj === null) {
                // simple data type
                if(t == "string") obj = '"' + obj + '"';
                return String(obj);
            } else {
                // array or object
                var json = [], arr = (obj && obj.constructor == Array);
                $.each(obj, function(k, v) {
                    t = typeof(v);
                    if(t == "string") v = '"' + v + '"';
                    else if (t == "object" & v !== null) v = $.serializeJSON(v)
                    json.push((arr ? "" : '"' + k + '":') + String(v));
                });
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    });
});

function select_extension(){
        $.ajax({
         url: BASEPATH + 'c_outbound_report_cdr/get_extension_list',
        success: function (response) {
            
                var mySelect = $('#select_extn_list');
                mySelect.empty();

                var val, text;
                var data = jQuery.parseJSON(response);
                var x = '"' + 0 + '"';
                mySelect.append(
                        $('<option></option>').val(x).html('--All Extension--')
                        );
                for (var i = 0; i < data.length; i++)
                {
                    val = '"' + data[i].extension + '"';
                    text = data[i].extension_name;

                    mySelect.append(
                            $('<option></option>').val(val).html(text)
                            );
                }
                $("#cdr_report").hide();
                $("#each_call_report").hide();
            
        }
    })
    }
     
function get_outbound_cdr_report_store_proc(){
$("#tbody_report").empty();
var table= $('#tableOutboundReport');
table.dataTable().fnDestroy();
    var exn_id=$('#select_extn_list').val();
    var start_date=$('#datepickerStartDate').val();
    var end_date=$('#datepickerEndDate').val();
    var ph_no=$('#ph_no').val();
    
    $('#tableOutboundReport').DataTable( {
                    "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]],
                    "paging": true,
                    "processing": true,
                    "serverSide": true,                                       
                    "ajax": {
                        "url": BASEPATH + 'c_outbound_report_cdr/get_outbound_cdr_report/',
                        "type":'post',
                        "data":{exn_id:exn_id,ph_no:ph_no,start_date:start_date,end_date:end_date}
                    }
                } );
               
    }
   function get_outbound_cdr_report(){
    var exn_id=$('#select_extn_list').val();
    var ob_team=$('#oblist').val();
    var start_date=$('#datepickerStartDate').val();
    var end_date=$('#datepickerEndDate').val();
    var ph_no=$('#ph_no').val();
    var selected=$("#select_client_ticket").val();
    selected=$.trim(selected);
    if(exn_id==null && ob_team==null){
        alert('Please select either Extesion or Team');return;
    }
    
    if(start_date==''){
        alert('Please select start date');return;
    }
    if(end_date==''){
        alert('Please select end date');return;
    }
    var exn_id = encodeURI(exn_id);
   
    if(selected==2)
    {
        $.ajax({
            url: BASEPATH + 'c_outbound_report_cdr/get_outbound_cdr_report/',
            type:'post',
            data:{exn_id:exn_id,ph_no:ph_no,start_date:start_date,end_date:end_date,ob_team:ob_team},
            success:function(msg){
                    if(msg!=''){
                    var table= $('#tableOutboundReport');
                    table.dataTable().fnDestroy();
                    $('#tbody_ObReport').html(msg);     
                    $('#cdr_report').show();
                    $("#tableOutboundReport").DataTable({
            });	
                }
            }
            
            })

    }
    else
    {
        $.ajax({
            url: BASEPATH + 'c_outbound_report_cdr/get_outbound_Eachcall_report/',
            type:'post',
            data:{exn_id:exn_id,ph_no:ph_no,start_date:start_date,end_date:end_date},
            success:function(msg){
                    var table= $('#tableEachCallReport');
                    table.dataTable().fnDestroy();
                    $('#tbody_report').html(msg);     
                    $('#each_call_report').show();
                    $("#tableEachCallReport").DataTable({
            });	
                }
            
            })
    }
    
    }
    
    

function hide_extn()
{
    var selected=$("#select_client_ticket").val();
    if(selected==2)
    {
    $("#select_extn_list").show();
    $("#extn_name").show();
    $("#cdr_report").show();
    $("#each_call_report").hide();
    }else
    {
    $("#select_extn_list").hide();
    $("#extn_name").hide();
    $("#cdr_report").hide();
    $("#each_call_report").show();
    }
}

//Added by Rajnandini Lokhande on 1/12/2016
    function select_queue(){
            $.ajax({
            url: BASEPATH + 'c_outbound_report_cdr/get_queue_list',
            success: function(msg){
                $('#select_queue_list').html(msg);
            }
            })
        }
    
    function get_extension(){
        $.ajax({
        url: BASEPATH + 'c_outbound_report_cdr/get_extension_list',
        success: function(msg){
            
            $('#extn_list').html(msg);
            
        }
        })
    }
    
    function get_extension_new(){
        $.ajax({
        url: BASEPATH + 'c_outbound_report_cdr/get_extension_list_new',
        success: function(msg){
            
            $('#extn_list').html(msg);
            
        }
        })
    }
    
/*Added by Rajnandini Lokhande 0n 07/12/2016*/

function getObTeam() {
    $.ajax({
        url: BASEPATH + 'c_outbound_report_cdr/getObTeam',
        success: function (response) {
            if (response != 'false')
            {
                var mySelect = $('#oblist');
                mySelect.empty();

                var val, text;
                var data = jQuery.parseJSON(response);
                var x = '"' + 0 + '"';
                mySelect.append(
                        $('<option></option>').val(x).html('--All ObTeam--')
                        );
                for (var i = 0; i < data.length; i++)
                {
                    val = '"' + data[i].ob_team + '"';
                    text = data[i].ob_team;

                    mySelect.append(
                            $('<option></option>').val(val).html(text)
                            );
                }
            }
        }
    })
}
    
//    function generateQueueExcel(){
//        var startDate=$('#datepickerStartDate').val();
//        var endDate=$('#datepickerEndDate').val();
//        var d ={startDate:startDate,endDate:endDate};
//    $.ajax({
//        url: BASEPATH + 'c_outbound_report_cdr/GenerateQueueExcel/',
//        type: 'POST',
//        data: {'rpt': d},
//        success: function (msg) {
//            
//        }
//    })
//    }


/*
* Author : Rahul 20.12.2016 | Purpose : Outbound SLA Report | Requested By: 
*/
function get_extensionList(){
    $.ajax({
        url: BASEPATH + 'c_outbound_report_cdr/get_extensionList',
        success: function (response) {
            $('#sla_extn_list').html(response);
        }
    });
}

function ob_SLAreport(){
    var inpstdate = $('#inpFromDate1').val();
    var inpEnddate = $('#inpToDate1').val();
    var select_extn_list = $('#sla_extn_list').val();

    if (inpstdate == '') {
        alert('Please select start date.');
        return;
    }
    if (inpEnddate == '') {
        alert('Please select end date.');
        return;
    }
    if (select_extn_list == '' || select_extn_list==0 || select_extn_list=='undefined' || select_extn_list==null) {
        alert('Please select end date.');
        return;
    }
    
    $.ajax({
        url: BASEPATH + 'c_outbound_report_cdr/ob_SLAreport/',
        type: 'POST',
        data: {'inpFromDate1': inpstdate,
            'inpToDate1':inpEnddate,
            'select_extn_list':select_extn_list},
        success: function (response) {
            var text='';
            
            var data = jQuery.parseJSON(response);
            for(i=0;i<data.length;i++) {
                text+='<tr><td>'+data[i].date+'</td>';
                text+='<td>'+data[i].iqueue+'</td>';
                text+='<td>'+data[i].no_of_dial+'</td>';
                text+='<td>'+data[i].calls_connected+'</td>';
                text+='<td>'+data[i].conn_per+'%</td>';
                text+='<td>'+data[i].tot_duration+'</td>';
                text+='<td>'+data[i].avg_duration+'</td></tr>';
            }
            $("#agent_OB_slaReport").dataTable().fnDestroy();
            $('#agent_OB_slaReportBODY').html(text);
            $("#agent_OB_slaReport").DataTable({
            });
        }
    });
    
}

function update_ip_excel(){
    var stDate = $('#inpFromDate1').val();
    var eDate = $('#inpToDate1').val();
    var queueList = $('#sla_extn_list').val();
    
    $('#inpFromDate11').val(stDate);  
    $('#inpToDate11').val(eDate);  
    $('#sla_extn_list1').val(queueList);
}

//End 20.12.2016

function downloadAbandonCall()
{
    var startDate = $("#datepickerStartDate").val();
    var endDate = $("#datepickerEndDate").val();
    startDate=startDate.replace(/\//g,"-"); 
    endDate=endDate.replace(/\//g,"-");     
    //var newStartDate=$.trim(startDate.substring(0,11));
    //var starttime=$.trim(startDate.substring(11,17));
     //Here,to use replace fun. for replaced : by @ from  starttime
    //starttime=starttime.replace(':','@');
     //Here,to use substring fun. for separated time(starttime) from newEndDate
    //var newEndDate=$.trim(endDate.substring(0,11));
    //var endtime=$.trim(endDate.substring(11,17));
     //Here,to use replace fun. for replaced : by @ from  endtime
    //endtime=endtime.replace(':','@');
    //ashwini end here on 14th_dec_2016
    var url = BASEPATH + 'c_outbound_report_cdr/getCallAbandon/'+startDate+'/'+endDate;
     window.open(
             url,
      '_blank' // <- This is what makes it open in a new window.
    );
    return;
}
