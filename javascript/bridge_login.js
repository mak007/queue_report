/* Checking the all Empty field and validation OLd password Correct or not.
 * if Not then shows alert if coreect then password rest*/
function bridge_reset_pass(){
    
    var u_did_no= $('#bridge_did_numr').val();
    var u_old_pwd= $('#bridge_old_password').val();
    var u_new_pass= $('#bridge_new_password').val();
    var u_confirm_pass= $('#bridge_confirm_password').val();
    
    if(u_did_no == '')
    {
        alert('Please Enter DID Number');
        $('#bridge_did_numr').css('border-color','red');
        return;
    }
     if(u_old_pwd == '')
    {
        alert('Please Enter Old Password');
        $('#bridge_old_password').css('border-color','red');
        return;
    }
    if(u_new_pass == '')
    {
        alert('Please Enter New Password');
        $('#bridge_new_password').css('border-color','red');
        return;
    }
    if(u_confirm_pass == '')
    {
        alert('Please Enter Confirm Password');
        $('#bridge_confirm_password').css('border-color','red');
        return;
    }
    var dataString = {u_did_no: u_did_no, u_old_pwd: u_old_pwd,u_new_pass:u_new_pass,u_confirm_pass:u_confirm_pass};
    $('#bridge_did_numr').css('border-color','green');
    $('#bridge_old_password').css('border-color','green');
    $('#bridge_new_password').css('border-color','green');
    $('#bridge_confirm_password').css('border-color','green');
    $.ajax({
        url: BASEPATH + 'c_report/bridge_reset_pass',
        type: 'POST',
        data: {data: dataString},
        success: function (msg) {
            if(msg==1){
                alert("Password Reset Successfull");
                $("#frm_rest :input").css('border','1px solid #ccc');
                document.forms["frm_rest"].reset();
                $('#bridge_confirm_password').css('background-color','#e1eafc');
                $('.error_msg').empty();
            }else{
                alert("please Enter the correct Old Password");
            }
        }
    });
}

/* New Password and Confirm password validation and Error showing*/
$(document).ready(function(){
$('#bridge_confirm_password').on('keyup',function(){
    if ($(this).val() == $('#bridge_new_password').val()) 
    {
        $('#error_msg').html('Passwords Match!').css('color', '58d558');
        $('#bridge_confirm_password').css('background-color','58d558');
    } 
    else{
        $('#error_msg').html('Passwords Do Not Match!').css('color', 'f6343b');
        $('#bridge_confirm_password').css('background-color','f6343b');
    };
});
});
/* End of Confirm password validation*/