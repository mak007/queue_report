// JavaScript Document
$(document).ready(function(e) {
    $(window).resize(function() {
    var winHeight_RTM_New_UI, docHeight_RTM_New_UI;

    winHeight_RTM_New_UI = $(window).height();
    docHeight_RTM_New_UI = $(document).height();
        
    var tableHeight = winHeight_RTM_New_UI - 200;
	var minusWindowH = winHeight_RTM_New_UI - 50;
    var testDivideWindow = minusWindowH / 2;
    
	var chartWindow_height = testDivideWindow + 100;
	var intervalWindow_height = testDivideWindow - 120;
	
    $(".scrollTable-RTM-New-UI tbody").css("max-height", tableHeight +"px");
    $(".chart-window-RTM-New-UI").css("height", chartWindow_height +"px");
    $(".interval-window-RTM-New-UI").css("height", intervalWindow_height +"px");
	
	
	
//	SHOULD BE CHANGED ON COLUMN NUMS TO MAKE COLUMNS EQUAL DIVIDE COLUMN SIZE FROM 100  E.G. FOR 4 COLUMNS WILL BE 25%.
	$("table.scrollTable-RTM-New-UI tbody td, table.scrollTable-RTM-New-UI thead th").css("width","25%");
	

});
    $(window).trigger('resize');
    $(".autoResizeScreen-RTM").trigger('resize');
});