jQuery(function() {
    $.extend({
        serializeJSON: function(obj) {
            var t = typeof(obj);
            if(t != "object" || obj === null) {
                // simple data type
                if(t == "string") obj = '"' + obj + '"';
                return String(obj);
            } else {
                // array or object
                var json = [], arr = (obj && obj.constructor == Array);
                $.each(obj, function(k, v) {
                    t = typeof(v);
                    if(t == "string") v = '"' + v + '"';
                    else if (t == "object" & v !== null) v = $.serializeJSON(v)
                    json.push((arr ? "" : '"' + k + '":') + String(v));
                });
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    });
});
function addNewOutcome(accountId,userId){
	//alert(accountId);
	//alert(userId);
	//return;
	$.ajax({
        url: BASEPATH + 'c_outcome/addNewOutcome/'+accountId+'/'+userId,
        success: function(msg){
            $('#divAddOutcome').html(msg);
			
			
        }
    })
}

function existingOutcome(accountId,userId){
	
		
	$.ajax({
        url: BASEPATH + 'c_outcome/existingOutcome/'+accountId+'/'+userId,
        success: function(msg){
            $('#divDisplayOutcome').html(msg);
			 
			
        }
    })
	
	
	
	
	
}
function choose_client(){
	
	var clientId=$('#selectClient option:selected').val();
	$('#inpClient').val(clientId);
	 $.ajax({
        url: BASEPATH + 'c_outcome/show_campaigns/'+clientId,
		success: function(msg){
            $('#camplist').html(msg);
			
			
        }
        
    })
	
	
}
function choose_campaign(){
	var campaignId=$('#selectCampaign option:selected').val();
		$('#inpCampaign').val(campaignId);
		 $.ajax({
        url: BASEPATH + 'c_outcome/show_process/'+campaignId,
		success: function(msg){
            $('#processlist').html(msg);
			
			
        }
        
    })
	
}
function choose_process(){
	var processId=$('#selectProcess option:selected').val();
	$('#inpProcessId').val(processId);
}
function getEditorOutcomeData(accountId,userId){
	//alert(accountId);
	//alert(userId);
	//return;
	//var editorTextOutcome = CKEDITOR.instances.inpOutcomeRemarks.getData();
	//alert(editorTextOutcome);
	//return;
	
	var selectClient=$('#selectClient option:selected').val();
	 var selectCampaign=$('#selectCampaign option:selected').val();
	  var selectProcess=$('#selectProcess option:selected').val();
	 var OutcomeName=$('#outcomeTitle').val();
	 var inpOutcomeRemark=$('#inpOutcomeRemarks').val();
	 if(selectClient == '--Choose--')	{
		alert("Select Client");
		return;
	}
	else if(selectCampaign=='--Choose--'){
		alert("Select  Campaign");
		return;
	}
	else if(selectProcess=='--Choose--'){
		alert("Select  Process ");
		return;
	}
	else if(OutcomeName==''){
		alert("Enter Outcome Name");
		return;
	}
	else if(inpOutcomeRemark==''){
		alert("Enter Remark");
		return;
	}
	else {
 $('#btnOutcomeSave').css('display','none');
	saveOutcome(accountId,userId);
	}
	 //
	//  
	
}
function saveOutcome(accountId,userId){
	 var p=$('#inpOutcomeRemarks').val();
	  var b=encodeURIComponent(p);
	  $('#inpOutcomeRemarks').val(b);
	//
	  var x=$('#formOutcome').serializeArray();
    var d=$.serializeJSON(x);
 //alert(d);
 // return;
    $.ajax({
        url: BASEPATH + 'c_outcome/saveOutcome/'+accountId+'/'+userId,
        type: 'POST',
        data:{'rpt' : d},
        success: function(msg){
			alert("Data Saved");
			addNewOutcome(accountId,userId);
			existingOutcome(accountId,userId);
			//sortOutcomeDisplay(accountId,userId);
			
        
        },
        error: function() {
            alert('Error');
        }
    })
}
function changeOutcomeStatus(accountId,userId,outcomeId,processId){
	
	var accountId=accountId;
	var userId=userId; 
	var outcomeId=outcomeId; 
	var processId=processId;
	/* alert(accountId);
	alert(userId);
	alert(outcomeId);
	alert(processId);
	return; */
	$.ajax({
		url: BASEPATH + 'c_outcome/changeOutcomeStatus/'+outcomeId+'/'+processId,
				success: function(msg) {
			           alert('Status Changed');
					  // existingOutcome(accountId,userId);
					  sortOutcomeDisplay(accountId,userId);
        },
		error: function(msg) {
			           alert('Error');
        }
		
	})
}
function editOutcome(accountId,userId,outcomeId,processId){
	//alert(outcomeId);
	//alert(processId);
	//return;
	$.ajax({
		url:BASEPATH +'c_outcome/editOutcome/'+accountId+'/'+userId+'/'+outcomeId+'/'+processId,
		 type: 'POST',
		 success:function(msg){
			$('#divEditOutcomedata').html(msg);
			$('#divDisplayOutcome').css('display','none');//////////////////
			 
		 }
	})
	
	
}
/*
function getEditorEditOutcomeData(accountId,userId,outcomeId,processId){
		alert(accountId);
	alert(userId);
	alert(outcomeId);
	alert(processId);
	return;
	 //var editorTextOutcomeEdit = CKEDITOR.instances.editOutcomeRemarks.getData();
	//alert(editorTextOutcomeEdit);
	//return;
    //$('#bdeditOutcome').val(editorTextOutcomeEdit);
	//updateOutcome(accountId,userId,outcomeId,processId);

	
}
*/
function updateOutcome(accountId,userId,outcomeId,processId){
	var accountId=accountId;
	var userId = userId;
	var outcomeId= outcomeId;
	var processId= processId;
	var p=$('#editOutcomeRemarks').val();
	  var b=encodeURIComponent(p);
	  $('#editOutcomeRemarks').val(b);
	
	var x=$('#formEditOutcome').serializeArray();
    var d=$.serializeJSON(x);
	//alert(d);
   // return;
	 $.ajax({
        url: BASEPATH + 'c_outcome/updateOutcome/'+accountId+'/'+userId+'/'+outcomeId+'/'+processId,
        type: 'POST',
        data: {'rpt': d},
		success: function(msg) {
			           alert('data Updated');
					   
					   $('#divEditOutcomedata').css('display','none');
					    existingOutcome(accountId,userId);
						//sortOutcomeDisplay(accountId,userId);
					   window.location=BASEPATH +'c_campaigns/outcome_setup/';//changed
					 
					   
        },
		error: function(msg) {
			           alert('Error');
        }
		
        
    })
	
}
function cancelEditOutcome(){
	 window.location=BASEPATH +'c_campaigns/outcome_setup/';//changed
	
}
function choose_campaignExisting(){
	
	var clientId=$('#selectClientExisting option:selected').val();
	$('#inpClientIdExisting').val(clientId);
	//return;
	 $.ajax({
        url: BASEPATH + 'c_outcome/show_campaignsExisting/'+clientId,
		success: function(msg){
           $('#camplistExisting').html(msg);
			
			
        }
        
    })
	
}
function choose_processExisting(){
	var campaignId=$('#selectCampaignExisting option:selected').val();
	$('#inpCampaignExisting').val(campaignId);
	//return;
	 $.ajax({
        url: BASEPATH + 'c_outcome/show_processExisting/'+campaignId,
		success: function(msg){
           $('#processlistExisting').html(msg);
			
			
        }
        
    })
	
}

function InputProcessIdExisting(){
	var processId=$('#selectProcessExisting option:selected').val();
	$('#inpProcessIdExisting').val(processId);
}


function sortOutcomeDisplay(accountId,userId){
	var clientId1=$('#selectClientExisting option:selected').val();
	var campaignId1=$('#selectCampaignExisting option:selected').val();
	var processId1=$('#selectProcessExisting option:selected').val(); 
	
	if( clientId1== '--Choose--')	{
		clientId1=0;
		campaignId1=0;
		processId1=0;
	}
	else if(campaignId1=='--Choose--'){
		campaignId1=0;
		processId1=0;
	}
	else if(processId1=='--Choose--'){
		processId1=0;
	}
	
	 $.ajax({
        url: BASEPATH + 'c_outcome/sortOutcomeDisplay/'+accountId+'/'+userId+'/'+clientId1+'/'+campaignId1+'/'+processId1,
		success: function(msg){
        $('#displayexistingOutcome1').html(msg);
			
			
        }
        
    })
	
	
	}//--end function