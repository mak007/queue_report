jQuery(function ($) {
    $.extend({
        serializeJSON: function (obj) {
            var t = typeof (obj);
            if (t != "object" || obj === null) {
                // simple data type
                if (t == "string")
                    obj = '"' + obj + '"';
                return String(obj);
            } else {
                // array or object
                var json = [], arr = (obj && obj.constructor == Array);
                $.each(obj, function (k, v) {
                    t = typeof (v);
                    if (t == "string")
                        v = '"' + v + '"';
                    else if (t == "object" & v !== null)
                        v = $.serializeJSON(v)
                    json.push((arr ? "" : '"' + k + '":') + String(v));
                });
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    });
});
$(document).ready(function () {

    $('#inpFromDate').datepicker({
        dateFormat: 'dd-mm-yy',
        numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateStartDate(selected);

        }
    });
    $('#inpToDate').datepicker({
        dateFormat: 'dd-mm-yy ',
        numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateEndDate(selected);
        }
    });

    $('#inpFromDate1').datetimepicker({
        dateFormat: 'dd-mm-yy',
        numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateStartDate(selected);

        }
    });
    $('#inpToDate1').datetimepicker({
        dateFormat: 'dd-mm-yy',
        numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateEndDate(selected);
        }
    });

    $('#logsheetCalendar').datepicker({
        dateFormat: 'dd-mm-yy',
        numberOfMonths: 1,
        onSelect: function (selected, evnt) {
            updateInpDate(selected);
            updateInpDate1(selected);
            // updatePrepaidDate(selected);


        }
    });

})
function updateStartDate(value) {
    $('#inpstdate').val(value);
    $('#inpstdate').val(value);

}
function updateEndDate(value) {
    $('#inpEnddate').val(value);
}


function each_call_details_report() {
    //alert('Test');return;
    var queue = $('#select_queue_list').val();
    var exn_id=$('#sla_extn_list').val();
    var inpstdate = $('#inpstdate').val();
    var inpEnddate = $('#inpEnddate').val();
    if (inpstdate == '') {
        alert('Please select start date.');
        return;
    }
    if (inpEnddate == '') {
        alert('Please select end date.');
        return;
    }
    if (queue == null || queue==0) {
        queue = '"0"';
    }
    if (exn_id == null || exn_id==0) {
        exn_id = '"0"';
    }
    var queue = encodeURI(queue);
    var exn_id = encodeURI(exn_id);
    // alert(queue);
   // $("#eachCallDetail").dataTable().fnDestroy();
    var arr = {exn_id:exn_id,queue: queue, inpstdate: inpstdate, inpEnddate: inpEnddate};
    $.ajax({
        url: BASEPATH + 'c_report/each_call_details_report/',
        type: 'POST',
        data: {'rpt': arr},
        success: function (msg) {
            //$('#displayChangeddiv').show();
            $("#eachCallDetail").dataTable().fnDestroy();
            $('#each_call_details_report').html(msg);
            $("#eachCallDetail").DataTable({
            });

        }
    })

}
function intra_day_sl_report() {
    //alert('Test');return;
    var x = $('#formDateUnpaid2').serializeArray();
    //var y =$('#inpToDate').serializeArray();
    var d = $.serializeJSON(x);
    var date = $('#inpFromDate').val();
    var interval = $('#intervalSelect').val();
    if (date == '') {
        alert('Please select date.');
        return;
    }
    if (interval == 0) {
        alert('Please select interval time.');
        return;
    }
    $("#intraDaySl").dataTable().fnDestroy();
    $.ajax({
        url: BASEPATH + 'c_report/intra_day_sl_report/',
        type: 'POST',
        data: {'rpt': d},
        success: function (msg) {
            $("#intra_day_sl_report").empty();
            $('#intra_day_sl_report').html(msg);
            $("#intraDaySl").DataTable({
                  "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]] ,
            });

        }
    })

}



/* function day_wise_sl_report(){
 //alert('hello');return;
 //var month=getMonthCust();
 //var year=getYearCust();
 //if( month != 0 && year != 0){
 
 $.ajax({
 url: BASEPATH + 'c_report/day_wise_sl_report/',
 success: function(msg){
 $('#day_wise_sl_report').html(msg);
 }
 })
 } */

function day_wise_sl_report() {
    var queue = $('#select_queue_list').val();
    var inpstdate = $('#inpFromDate').val();
    var inpEnddate = $('#inpToDate').val();
    if (queue == null || queue==0) {
        queue = '"0"';
    }
    var queue = encodeURI(queue);
    $("#day_wise_sl_report").empty();
    $("#dayWiseSl").dataTable().fnDestroy();
    $("#day_wise_sl_report").empty();
    // alert(queue);
    var arr = {queue: queue, inpstdate: inpstdate, inpEnddate: inpEnddate};
    $.ajax({
        url: BASEPATH + 'c_report/day_wise_sl_report/',
        type: 'POST',
        data: {'rpt': arr},
        success: function (msg) {
            //$('#displayChangeddiv').show();
            $('#day_wise_sl_report').html(msg);
            $("#dayWiseSl").DataTable({
            });

        }
    })

}





/* function selectQueue(){
 
 var x=$('#formDateUnpaid').serializeArray();
 var d=$.serializeJSON(x);
 $.ajax({
 url: BASEPATH +  'c_report/selectQueue/',
 type: 'POST',
 data:{'rpt' : d},
 success: function(msg){
 $('#viewQueue').html(msg);
 }
 })
 }
 */

function set_queue() {
    $('#inpQueue').val($('#queueSelect :selected').val());
    $('#inpQueue1').val($('#queueSelect :selected').val());
    var abc = $('#queueSelect :selected').val();

}

function set_interval() {

    $('#inp_set_interval').val($('#intervalSelect :selected').val());
    // $('#inpQueue1').val($('#queueSelect :selected').val());

    $('#inpINT').val($('#intervalSelect :selected').val());
    var abc = $('#intervalSelect :selected').val();

}

function display_records() {
    $("#cdrExcel").dataTable().fnDestroy();
    var start = $('#inpstdate').val();
    var end = $('#inpEnddate').val();
    var phone = $('#inpPhone').val();
    if (start == '') {
        alert('Please select start date');
        return;
    }
    if (end == '') {
        alert('Please select end date');
        return;
    }
    if (phone == "") {
        phone = 0;
    }

    $.ajax({
        url: BASEPATH + 'c_report/display_records/' + start + '/' + end + '/' + phone,
        success: function (msg) {
            $('#display_records_div').html(msg);
            $("#cdrExcel").DataTable({
            });
        }
    })
}
function getData() {
    $('#inpQueue').val($('#queuename').val());
    //$('#inpQueue1').val($('#queuename').val());
    $('#inpQueue1').val($('#select_queue_list :selected').val());
}
function getDataforextn() {
    $('#inpQueue').val($('#extn_name').val());
    //$('#inpQueue1').val($('#queuename').val());
}
function getTransferDetails(callid) {
    //alert(callid);return;
    $.ajax({
        url: BASEPATH + 'c_report/getTransferDetails',
        type: 'POST',
        data: {'callid': callid},
        async: false,
        success: function (msg) {
            $('#transferDetailsBody').html(msg);
            $('#transferDetails').modal('show');
        }
    });
}

function agentWiseOBreport() {
    var queue = $('#queuename').val();
    var inpstdate = $('#inpstdate').val();
    var inpEnddate = $('#inpEnddate').val();
    var select_extn_list = $('#select_extn_list').val();
    var oblist = $('#oblist').val();
    if (inpstdate == '') {
        alert('Please select start date.');
        return;
    }
    if (inpEnddate == '') {
        alert('Please select end date.');
        return;
    }
    if (oblist == null) {
        oblist = 'x';
    }
    if (select_extn_list == null) {
        select_extn_list = 'x';
    }
    if (select_extn_list == 'x' && oblist == 'x') {
        alert('Please select either extension OR team !!');
        return;
    }
    var oblist = encodeURI(oblist);
    var select_extn_list = encodeURI(select_extn_list);
    
    // alert(queue);
    var arr = {oblist: oblist,select_extn_list:select_extn_list, inpstdate: inpstdate, inpEnddate: inpEnddate};
    $.ajax({
        url: BASEPATH + 'c_report/agentWiseOBreport/',
        type: 'POST',
        data: {'rpt': arr},
        success: function (response) {
            var text='';
            if (response != 'false')
            {
                var data = jQuery.parseJSON(response);
               for(i=0;i<data.length;i++) {
                  text+='<tr><td>'+data[i].date+'</td>';
                  text+='<td>'+data[i].agent_name+'</td>';
                  text+='<td>'+data[i].extn+'</td>';
                  text+='<td>'+data[i].no_of_dials+'</td>';
                  text+='<td>'+data[i].calls_connected+'</td>';
                  text+='<td>'+data[i].conn_per+'%</td>';
                  text+='<td>'+data[i].tot_duration+'</td>';
                  text+='<td>'+data[i].avg_duration+'</td></tr>';
               }
            }
            $("#agentWiseOBReportTB").dataTable().fnDestroy();
            $('#agentWiseOBReportBODY').html(text);
            $("#agentWiseOBReportTB").DataTable({
            });

        }
    })

}

function getOBteam(){
    $('#obList').val($('#oblist').val());
}
function getExtnList(){
    $('#extensionList').val($('#select_extn_list').val());
}
function get_extensionListforinbound(){
        $.ajax({
         url: BASEPATH + 'c_report/get_extensionListforinbound',
        success: function (response) {
             $('#sla_extn_list').html(response);
             
    }
        });
    }