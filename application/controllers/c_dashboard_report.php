<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class C_dashboard_report extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('utilities_model');
        $this->load->model('dashboard_report_model');
       // $this->load->library('middleware/'.CUSTOME_API_FILE);
    }

    function getrefreshrate() {
        $ans = $this->dashboard_report_model->getrefreshrate();
        echo $ans;
    }
    function getQueuename() {
        $ans = $this->dashboard_report_model->getQueuename();
        echo $ans;
    }

    function getForTheDayData() {
        $ans = $this->dashboard_report_model->getForTheDayData();
        echo $ans;
    }
    function getCurrentIntervalData() {
        $ans = $this->dashboard_report_model->getCurrentIntervalData();
        echo $ans;
    }
    function getGraphData() {
        $ans = $this->dashboard_report_model->getGraphData();
        echo $ans;
    }
    function getRealtimeQueueAPIData() {
//        $api_file = CUSTOME_API_FILE;
//        $obj = new stdClass();
//        $arr=$this->input->post('rpt');
//        $obj->queue=$arr;
//        $apiStatus = $this->$api_file->callMethod("getRealtimeQueueAPIData",$obj);
//        echo $apiStatus;
        $ans = $this->dashboard_report_model->getRealtimeQueueAPIData();
        echo $ans;
    }
    
    function display_rtm_report(){
        $ans = $this->dashboard_report_model->display_rtm_report();
        echo $ans;
    }
    function display_rtm_Extnreport(){
        $ans = $this->dashboard_report_model->display_rtm_Extnreport();
        echo $ans;
    }
    
    function rtm_report(){
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data=$this->utilities_model->init_data();
        $this->load->view('v_rtm_report', $data);
    }
    function getLiveExtensionDetails(){
        $qu=0;
        $url="";
        if(isset($_POST['queue']) && $_POST['queue']!=0){
            $queue = $_POST['queue'];
            foreach($queue as $q){
            $qu=$q.",";
            }
            $qu=rtrim($qu,",");
        }
        if($qu==0)
            $url="http://".API_SERVER."/tel_api/get_realtime_extension_list.php?queue=";
        else
            $url="http://".API_SERVER."/tel_api/get_realtime_extension_list.php?queue=".$qu;
        $curl= curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $json_response = curl_exec($curl);
        echo $json_response;
    }
    
    /**/
    function rtm_report_v1(){
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data=$this->utilities_model->init_data();
        $this->load->view('v_rtm_report_v1', $data);
    }
    
    function get_sl_data(){
        $ans = $this->dashboard_report_model->get_sl_data();
        echo $ans;
    }
    
    function get_al_data(){
        $ans = $this->dashboard_report_model->get_al_data();
        echo $ans;
    }
    function display_rtm_Extnreport_v1(){
        $ans = $this->dashboard_report_model->display_rtm_Extnreport_v1();
        echo $ans;
    }
    function display_rtm_report_v1(){
        $ans = $this->dashboard_report_model->display_rtm_report_v1();
        echo $ans;
    }
    function getForTheDayData_v1() {
        $ans = $this->dashboard_report_model->getForTheDayData_v1();
        echo $ans;
    }
    function getCurrentIntervalData_v1() {
        $ans = $this->dashboard_report_model->getCurrentIntervalData_v1();
        echo $ans;
    }
    /**/
}
?>

