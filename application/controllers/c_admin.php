<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


Class C_admin extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('utilities_model');
     //   $this->load->model('users_model');
        $this->load->model('admin_model');
      
    }
    
    function index() {
        //echo 'Production<br/><br/><i>under construction</i>' ;
        $data=$this->utilities_model->init_data();
        $qry=$this->users_model->getUserDetails($id);
        $data['user_id']=$id;
        $data['session']=$session;
        $data['formal_name']=$qry->formal_name;
        $data['user_name']=$qry->user_name;
        $data['role']=$role;
	//print_r($data['role']);
	//	exit();
        $this->load->view('v_admin_managment ', $data);
    }
	
    function approval_list($id, $session, $role) {
        //echo 'Production<br/><br/><i>under construction</i>' ;
		//echo"hello";
	//	exit;
         $data=$this->utilities_model->init_data();
        $qry=$this->users_model->getUserDetails($id);
        $data['user_id']=$id;
        $data['session']=$session;
        $data['formal_name']=$qry->formal_name;
        $data['user_name']=$qry->user_name;
        $data['role']=$role;
		//print_r($data['role']);
	//	exit();
        $this->load->view('v_approval_list', $data);
    }
	function payment_pending_list($id, $session, $role) {
        //echo 'Production<br/><br/><i>under construction</i>' ;
		//echo"hello";
	//	exit;
         $data=$this->utilities_model->init_data();
        $qry=$this->users_model->getUserDetails($id);
        $data['user_id']=$id;
        $data['session']=$session;
        $data['formal_name']=$qry->formal_name;
        $data['user_name']=$qry->user_name;
        $data['role']=$role;
		//print_r($data['role']);
	//	exit();
        $this->load->view('v_payment_pending_list', $data);
    }
	function archivedPayment_pending_list(){
	    $ans=$this->admin_model->archivedPayment_pending_list();
            echo $ans;
	}
	function archivedApproval(){
            $ans=$this->admin_model->archivedApproval();
            echo $ans;
    }
    
   function apprvoveOrder($order_id){
    $ans=$this->admin_model->apprvoveOrder($order_id);
    echo $ans;
   }
   function apprvoveUnpaid_list($order_id){
    $ans=$this->admin_model->apprvoveUnpaid_list($order_id);
    echo $ans;
   }
    
   function saveApprovedOrder($order_id){
        $table_data = json_decode($_POST['rpt'], true);
        foreach ($table_data as $value) {
            $$value["name"] = $value["value"];
            echo $value["name"] . "=" . $$value["name"];
            echo "<br/>";
        }
        $status=$this->security->xss_clean($table_data[0]['value']);
        $mode=$this->security->xss_clean($table_data[1]['value']);
        $amount=$this->security->xss_clean($table_data[2]['value']);
        $orderStatus=$this->security->xss_clean($table_data[3]['value']);
        $arr=array(
            'payment_status'=>$status,
            'payment_mode'=>$mode,
            'amount_received'=>$amount,
            'order_status'=>$orderStatus
        );
        $a=$this->admin_model->saveApprovedOrder($order_id, $arr);
        echo '<x>' . $a . '</x>';
        
        
        
   }
    function saveApprvoveUnpaid_list($order_id){
        $table_data = json_decode($_POST['rpt'], true);
		foreach ($table_data as $value) {
            $$value["name"] = $value["value"];
            echo $value["name"] . "=" . $$value["name"];
            echo "<br/>";
        }
        $recDate = date('Y-m-d', strtotime($table_data[0]['value']));
        $status = $this->security->xss_clean($table_data[1]['value']);
		$mode = $this->security->xss_clean($table_data[2]['value']);
        $remarks = $this->security->xss_clean($table_data[3]['value']);
        $amount = $this->security->xss_clean($table_data[4]['value']);
        if ($status == 'Unpaid') {
            $order_status = "O";
        } else {
            $order_status = "A";
        }

        $arr = array(
            'order_status' => $order_status,
            'payment_receipt_dt' => $recDate,
            'payment_status' => $status,
			'payment_mode' => $mode,
            'remarks'=>$remarks,
            'amount_received'=>$amount,
        );
	//	print_r($arr);
//		exit;
        $a=$this->admin_model->saveApprvoveUnpaid_list($order_id, $arr);
        echo '<x>' . $a . '</x>';
    }
   function getApprovalOrdersForDate() {
        $table_data = json_decode($_POST['rpt'], true);
        foreach ($table_data as $value) {
            $$value["name"] = $value["value"];
            echo $value["name"] . "=" . $$value["name"];
            echo "<br/>";
        }
        $dt1=$this->security->xss_clean($table_data[0]['value']);
		$dt2=$this->security->xss_clean($table_data[1]['value']);
	//	echo $dt1.$dt2;exit;
        $ans=$this->admin_model->getApprovalOrdersForDate($dt1,$dt2);
        echo $ans;
    }
  //------------------------------------------------------------------------------------- 
   function weborders($id, $session, $role) {
       $data=$this->utilities_model->init_data();
       $data['customers']=$this->weborders_model->fetchCustomers();
      // $data['orders']=$this->weborders_model->fetchLast28DaysOrders(); substituted with checkFreshOrders() function below.
       $data['user_id']=$id;
       $qry=$this->users_model->getUserDetails($id);
       $data['formal_name']=$qry->formal_name;
       $data['role']=$role;
       $data['session']=$session;
       $this->load->view('v_weboders_temp', $data);
   }
   
   function customerDetails($weborder_id){
       $ans=$this->weborders_model->customerDetails($weborder_id);
       echo $ans;
   }
   
   function checkcustomerInEnh($mobile){
       $ans=$this->admin_model->checkcustomerInEnh($mobile);
       echo $ans;
   }
   
   function searchCustID($cust_id){
        $ans=$this->admin_model->checkcustomerInEnh($cust_id);
        echo $ans;           
    }
    
   function updateEnhCustomer($cust_id){
        $table_data = json_decode($_POST['rpt'], true);
        foreach ($table_data as $value) {
            $$value["name"] = $value["value"];
            echo $value["name"] . "=" . $$value["name"];
            echo "<br/>";
        }
       $arr=array(
           'customer_title'=>$this->security->xss_clean($table_data[0]['value']),
           'gender'=>$this->security->xss_clean($table_data[1]['value']),
           'first_name'=>$this->security->xss_clean($table_data[2]['value']),
           'last_name'=>$this->security->xss_clean($table_data[3]['value']),
           'dob'=>date('Y-m-d', strtotime($this->security->xss_clean($table_data[4]['value']))),
           'mobile'=>$this->security->xss_clean($table_data[5]['value']),
           'email'=>$this->security->xss_clean($table_data[6]['value']),
           'rating'=>$this->security->xss_clean($table_data[7]['value'])
       ); 
       $ans=$this->admin_model->updateEnhCustomer($cust_id, $arr);
       echo $ans;
   }
   
   function createNewCustomer() {
       $ans=$this->admin_model->createNewCustomer();
       echo $ans;
   }
   
   function insertEnhCustomer(){
        $table_data = json_decode($_POST['rpt'], true);
        foreach ($table_data as $value) {
            $$value["name"] = $value["value"];
            echo $value["name"] . "=" . $$value["name"];
            echo "<br/>";
        }
       
       $arr=array(
           'customer_title'=>$this->security->xss_clean($table_data[0]['value']),
           'gender'=>$this->security->xss_clean($table_data[1]['value']),
           'first_name'=>  ucfirst($this->security->xss_clean($table_data[2]['value'])),
           'last_name'=>ucfirst($this->security->xss_clean($table_data[3]['value'])),
           'dob'=>date('Y-m-d', strtotime($this->security->xss_clean($table_data[4]['value']))),
           'mobile'=>$this->security->xss_clean($table_data[5]['value']),
           'email' => $this->security->xss_clean($table_data[6]['value']),
           'created_on' => date('Y-m-d H:i:s'),
            'rating'=>$this->security->xss_clean($table_data[7]['value'])
       ); 
       $ans=$this->admin_model->insertEnhCustomer($arr);
       echo $ans;
   }
   
   function checkCustomerInOurDB(){
        $table_data = json_decode($_POST['rpt'], true);
        foreach ($table_data as $value) {
            $value["name"] = $value["value"];
            echo $value["name"] . "=" . $$value["name"];
            echo "<br/>";
        }
       $arr=array(
           'first_name'=>$this->security->xss_clean($table_data[0]['value']),
           'last_name'=>$this->security->xss_clean($table_data[1]['value']),
           'mobile'=>$this->security->xss_clean($table_data[2]['value']),
           'email'=>$this->security->xss_clean($table_data[3]['value'])
       ); 
       $ans=$this->admin_model->checkCustomerInOurDB($arr);
       echo $ans;
       
   }
   
   function getSpecificWebOrder($weborder_id){
       $ans=$this->weborders_model->getSpecificWebOrder($weborder_id);
       echo $ans;
   }
   
   function importOrders() {
       $ans=$this->weborders_model->importAllWebOrders();
       echo $ans;
   }
   
   function checkWebOrderInEnhDB($weborder_id){
       $ans=$this->admin_model->checkWebOrderInEnhDB($weborder_id);
       echo $ans;
   }
   
   function getCustomerAddresses($cust_id){
       $ans=$this->admin_model->getCustomerAddresses($cust_id);
       echo $ans;
   }
   
   function getCity1($city_id) {
       $ans=$this->weborders_model->setArea1($city_id);
       echo $ans;
   }
   
   function getCity2($city_id) {
       $ans=$this->weborders_model->setArea2($city_id);
       echo $ans;
   }
   
   function getArea($area_id){
       $ans=$this->weborders_model->getArea($area_id);
       echo $ans;
   }
   
   function setArea1($area_id){
       $ans=$this->weborders_model->setArea1($area_id);
       echo $ans;
   }
   
   function setArea2($area_id){
       $ans=$this->weborders_model->setArea2($area_id);
       echo $ans;
   }
   
   function addAddressAsBillingAddress($cust_id){
        $table_data = json_decode($_POST['rpt'], true);
        foreach ($table_data as $value) {
            $$value["name"] = $value["value"];
            echo $value["name"] . "=" . $$value["name"];
            echo "<br/>";
        }
       if($this->security->xss_clean($table_data[5]['value'])==253) {
           $city='Mumbai';
       }
       if($this->security->xss_clean($table_data[5]['value'])==254) {
           $city='Pune';
       }
       
        
        
        
        
        $arr=array(
           'customer_id'=>$cust_id,
           'address'=>$this->security->xss_clean($table_data[0]['value']),
           'road'=>$this->security->xss_clean($table_data[1]['value']),
           'landmark'=>$this->security->xss_clean($table_data[2]['value']),
           'channel'=>$this->security->xss_clean($table_data[3]['value']),
           'pin'=>$this->security->xss_clean($table_data[4]['value']),
           'city'=>$city,
           'zone'=>$this->security->xss_clean($table_data[6]['value']),
           'area'=>$this->security->xss_clean($table_data[7]['value'])
            );   
       $ans=$this->admin_model->insertAddress($arr);
       echo '<x>' . $ans .'</x>';
   }
   
   function addAddressAsShippingAddress($cust_id){
            $table_data = json_decode($_POST['rpt'], true);
        foreach ($table_data as $value) {
            $$value["name"] = $value["value"];
            echo $value["name"] . "=" . $$value["name"];
            echo "<br/>";
        }
       if($this->security->xss_clean($table_data[5]['value'])==253) {
           $city='Mumbai';
       }
       if($this->security->xss_clean($table_data[5]['value'])==254) {
           $city='Pune';
       }
       
       $arr=array(
           'customer_id'=>$cust_id,
           'address'=>$this->security->xss_clean($table_data[0]['value']),
           'road'=>$this->security->xss_clean($table_data[1]['value']),
           'landmark'=>$this->security->xss_clean($table_data[2]['value']),
           'channel'=>$this->security->xss_clean($table_data[3]['value']),
           'pin'=>$this->security->xss_clean($table_data[4]['value']),
           'city'=>$city,
           'zone'=>$this->security->xss_clean($table_data[6]['value']),
           'area'=>$this->security->xss_clean($table_data[7]['value'])
            );   
       $ans=$this->admin_model->insertAddress($arr);
       echo '<x>' . $ans .'</x>';   
   }
   
   function checkModifiedRC(){
       $ans=$this->weborders_model->checkModifiedRC();
       echo $ans;
   }
   
   function checkFreshOrders(){
       //09-03-2015
       $ans=$this->weborders_model->fetchLast28DaysOrders();
       echo $ans;
   }
   
   function checkPendingOrders(){
       $ans=$this->weborders_model->checkPendingOrders();
       echo $ans;
   }   
   
   function btnCreateOrder($cust_id, $billing_id, $delivery_id, $weborder_id, $rm_id){
       $ans=$this->weborders_model->btnCreateOrder($cust_id, $billing_id, $delivery_id, $weborder_id, $rm_id);
       echo $ans;
   }
   
   function showNewlyCreatedOrder($order_id){
       $ans=$this->weborders_model->showNewlyCreatedOrder($order_id);
       echo $ans;
   }
   
   function selectRowForUpdate($weborder_id, $websuborder_id){
       $ans=$this->weborders_model->selectRowForUpdate($weborder_id, $websuborder_id);
       echo $ans;
   }

   function rejectOrder($weborder_id) {
        $ans = $this->weborders_model->rejectOrder($weborder_id);
        echo $ans;
    }
	function firstTimeCalledReportExcel(){
		$dt1 = $this->input->post('inpLogdate');
		$dt2 = $this->input->post('inpEnddate');
		$startDate=date('Y-m-d', strtotime($dt1));
		$endDate=date('Y-m-d', strtotime($dt2));
	//echo $dt1.$dt2;exit;
		$ans= $this->admin_model->firstTimeCalledReportExcel($startDate,$endDate);
	//	print_r($ans);
		
		//exit;
		 $qryy=$this->db->query("SELECT * FROM `vicidial_log` WHERE `call_date` >= '$startDate' AND `call_date`<= '$endDate' AND `called_count` = '1'");
		$data=$qryy->num_rows();
		//print_r($data);
	//	exit;
 $this->load->library('excel');
            //activate worksheet number 1
            $exc=new PHPExcel();
			$exc->setActiveSheetIndex(0);
            //name the worksheet
            $exc->getActiveSheet()->setTitle('FirstTimeCalledReport');
		   $count=count($ans);
		           $exc->getActiveSheet()->setCellValue('A1' ,'LeadId' );
					$exc->getActiveSheet()->setCellValue('B1' ,'LeadId' );
					$exc->getActiveSheet()->setCellValue('C1' ,'Phone Number' );
					$exc->getActiveSheet()->setCellValue('D1' ,'Campaign' );
					$exc->getActiveSheet()->setCellValue('E1' ,'Called Date' );
					$exc->getActiveSheet()->setCellValue('F1' ,'Disposition' );
					$exc->getActiveSheet()->setCellValue('G1' ,'Called Count' );
					
					//$exc->getActiveSheet()->setCellValue('B1','Customer Name');
					$i=1;
			for ($x = 0; $x < $data; $x++ ){
				if (isset($ans['lead_id'][$x])) {
				//$ans['cust_id'][$x]='no record';
			
				if (!isset($ans['lead_id'][$x])) {
				$ans['lead_id'][$x]='no record';
			}
			if (!isset($ans['phone_number'][$x])) {
				$ans['phone_number'][$x]='no record';
			}
			if (!isset($ans['campaign_id'][$x])) {
				$ans['campaign_id'][$x]='no record';;
			}
			if (!isset($ans['call_date'][$x])) {
				$ans['call_date'][$x]='no record';
			}
			if (!isset($ans['status'][$x])) {
				$ans['status'][$x]='no record';
			}
			if (!isset($ans['called_count'][$x])) {
				$ans['called_count'][$x]='no record';
			}
			
			
					//$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$i) ->setAutoSize(true);;
					$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$i);
					$exc->getActiveSheet()->setCellValue('B' . (string)($i + 1),$ans['lead_id'][$x] );
					$exc->getActiveSheet()->setCellValue('C' . (string)($i + 1),$ans['phone_number'][$x] );
					$exc->getActiveSheet()->setCellValue('D' . (string)($i + 1),$ans['campaign_id'][$x] );
					$exc->getActiveSheet()->setCellValue('E' . (string)($i + 1),$ans['call_date'][$x] );
					$exc->getActiveSheet()->setCellValue('F' . (string)($i + 1),$ans['status'][$x] );
					$exc->getActiveSheet()->setCellValue('G' . (string)($i + 1),$ans['called_count'][$x] );
					
					
					$exc->getActiveSheet()->getStyle('A' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('B' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('C' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('D' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('E' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('F' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('G' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					
					
					$exc->getActiveSheet()->getStyle('H' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('I' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('J' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('K' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('L' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('M' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('N' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					$i++;
				}
	}
				// Setting width
					$exc->getActiveSheet()->getColumnDimension('B')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('E')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('F')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('G')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('H')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('I')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('J')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('k')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('L')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('M')->setWidth(100);
					$exc->getActiveSheet()->getColumnDimension('N')->setWidth(15);
					
					
					//Alignment Centre  
					$exc->getActiveSheet()->getStyle('A' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('B' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('C' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('D' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('E' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('F' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('G' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('H' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('I' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('J' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('K' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('L' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('M' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('N' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					//change the font size
            $exc->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
            //make the font become bold
			$exc->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $exc->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
			
			
			
			 $exc->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		//	$dte=date('d-m-Y',strtotime($dt));
			$fname='';
			$fname='FirstTimeCalledReport'.$startDate.'to'.$endDate.'.xls';
		  


header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$fname.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
             
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
       $objWriter->save($fname);
        $objWriter->save('php://output');
		
	}

	
	
	
	function agentReportExcel(){
		$dt1 = $this->input->post('inpReportStartDate');
		$dt2 = $this->input->post('inpReportEndDate');
		$startDate=date('Y-m-d', strtotime($dt1));
		$endDate=date('Y-m-d', strtotime($dt2));
	//echo $dt1.$dt2;exit;
		$ans= $this->admin_model->agentReportExcel($startDate,$endDate);
//print_r($ans);
		
	//exit;
		 $qryy=$this->db->query("SELECT DISTINCT user FROM `vicidial_agent_log` WHERE `event_time` >= '$startDate' AND `event_time`<= '$endDate'");
		$data=$qryy->num_rows();
		//print_r($data);
	//	exit;
 $this->load->library('excel');
            //activate worksheet number 1
            $exc=new PHPExcel();
			$exc->setActiveSheetIndex(0);
            //name the worksheet
            $exc->getActiveSheet()->setTitle('AgentReport');
		   $count=count($ans);
		           $exc->getActiveSheet()->setCellValue('A1' ,'AgentName' );
					//$exc->getActiveSheet()->setCellValue('B1' ,'Calls' );
					$exc->getActiveSheet()->setCellValue('B1' ,'Calls' );
					$exc->getActiveSheet()->setCellValue('C1' ,'Pause Time' );
					$exc->getActiveSheet()->setCellValue('D1' ,'Wait Time' );
					$exc->getActiveSheet()->setCellValue('E1' ,'Talk Time' );
					$exc->getActiveSheet()->setCellValue('F1' ,'Dispo TIme' );
					$exc->getActiveSheet()->setCellValue('G1' ,'Dead TIme' );
					$exc->getActiveSheet()->setCellValue('H1' ,'Total Time' );
					
					//$exc->getActiveSheet()->setCellValue('B1','Customer Name');
					$i=1;
			for ($x = 0; $x < $data; $x++ ){
				if (isset($ans['username'][$x])) {
				//$ans['cust_id'][$x]='no record';
			
				if (!isset($ans['username'][$x])) {
				$ans['username'][$x]='no record';
			}
			if (!isset($ans['leadId'][$x])) {
				$ans['leadId'][$x]='no record';
			}
			if (!isset($ans['pauseSec'][$x])) {
				$ans['pauseSec'][$x]='no record';;
			}
			if (!isset($ans['waitSec'][$x])) {
				$ans['waitSec'][$x]='no record';
			}
			if (!isset($ans['talkSec'][$x])) {
				$ans['talkSec'][$x]='no record';
			}
			if (!isset($ans['dispoSec'][$x])) {
				$ans['dispoSec'][$x]='no record';
			}
			if (!isset($ans['deadTime'][$x])) {
				$ans['deadTime'][$x]='no record';
			}
			if (!isset($ans['toal_time'][$x])) {
				$ans['toal_time'][$x]='no record';
			}
			
					//$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$i) ->setAutoSize(true);;
				//	$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$i);
					$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$ans['username'][$x] );
					$exc->getActiveSheet()->setCellValue('B' . (string)($i + 1),$ans['leadId'][$x] );
					$exc->getActiveSheet()->setCellValue('C' . (string)($i + 1),$ans['pauseSec'][$x] );
					$exc->getActiveSheet()->setCellValue('D' . (string)($i + 1),$ans['waitSec'][$x] );
					$exc->getActiveSheet()->setCellValue('E' . (string)($i + 1),$ans['talkSec'][$x] );
					$exc->getActiveSheet()->setCellValue('F' . (string)($i + 1),$ans['dispoSec'][$x] );
					$exc->getActiveSheet()->setCellValue('G' . (string)($i + 1),$ans['deadTime'][$x] );
					$exc->getActiveSheet()->setCellValue('H' . (string)($i + 1),$ans['toal_time'][$x] );
					
					
					$exc->getActiveSheet()->getStyle('A' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('B' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('C' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('D' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('E' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('F' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('G' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					
					
					$exc->getActiveSheet()->getStyle('H' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('I' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('J' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('K' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('L' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('M' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('N' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					$i++;
				}
	}
				// Setting width
					$exc->getActiveSheet()->getColumnDimension('B')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('E')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('F')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('G')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('H')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('I')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('J')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('k')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('L')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('M')->setWidth(100);
					$exc->getActiveSheet()->getColumnDimension('N')->setWidth(15);
					
					
					//Alignment Centre  
					$exc->getActiveSheet()->getStyle('A' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('B' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('C' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('D' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('E' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('F' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('G' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('H' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('I' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('J' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('K' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('L' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('M' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('N' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					//change the font size
            $exc->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
            //make the font become bold
			$exc->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $exc->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
			
			
			
			 $exc->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		//	$dte=date('d-m-Y',strtotime($dt));
			$fname='';
			$fname='AgentReport'.$startDate.'to'.$endDate.'.xls';
		  


header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$fname.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
             
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
       $objWriter->save($fname);
        $objWriter->save('php://output');
		
	}

	
	
	function userLogExcel(){
		$dt1 = $this->input->post('inpUserLogStartDate');
		$dt2 = $this->input->post('inpUsersLogEndDate');
		$startDate=date('Y-m-d', strtotime($dt1));
		$endDate=date('Y-m-d', strtotime($dt2));
	echo $dt1.$dt2;exit;
		$ans= $this->admin_model->userLogExcel($startDate,$endDate);
			print_r($ans);
			exit;
		 $qryy=$this->db->query("SELECT DISTINCT user FROM `vicidial_agent_log` WHERE `event_time` >= '$startDate' AND `event_time`<= '$endDate'");
		$data=$qryy->num_rows();
		//print_r($data);
	//	exit;
 $this->load->library('excel');
            //activate worksheet number 1
            $exc=new PHPExcel();
			$exc->setActiveSheetIndex(0);
            //name the worksheet
            $exc->getActiveSheet()->setTitle('AgentReport');
		   $count=count($ans);
		           $exc->getActiveSheet()->setCellValue('A1' ,'AgentName' );
					//$exc->getActiveSheet()->setCellValue('B1' ,'Calls' );
					$exc->getActiveSheet()->setCellValue('B1' ,'Total Time(In Min)' );
					$exc->getActiveSheet()->setCellValue('C1' ,'Pause Time' );
					$exc->getActiveSheet()->setCellValue('D1' ,'Wait Time' );
					$exc->getActiveSheet()->setCellValue('E1' ,'Talk Time' );
					$exc->getActiveSheet()->setCellValue('F1' ,'Dispo TIme' );
					$exc->getActiveSheet()->setCellValue('G1' ,'Dead TIme' );
					
					//$exc->getActiveSheet()->setCellValue('B1','Customer Name');
					$i=1;
			for ($x = 0; $x < $data; $x++ ){
				if (isset($ans['username'][$x])) {
				//$ans['cust_id'][$x]='no record';
			
				if (!isset($ans['username'][$x])) {
				$ans['username'][$x]='no record';
			}
			if (!isset($ans['leadId'][$x])) {
				$ans['leadId'][$x]='no record';
			}
			if (!isset($ans['pauseSec'][$x])) {
				$ans['pauseSec'][$x]='no record';;
			}
			if (!isset($ans['waitSec'][$x])) {
				$ans['waitSec'][$x]='no record';
			}
			if (!isset($ans['talkSec'][$x])) {
				$ans['talkSec'][$x]='no record';
			}
			if (!isset($ans['dispoSec'][$x])) {
				$ans['dispoSec'][$x]='no record';
			}
			if (!isset($ans['deadTime'][$x])) {
				$ans['deadTime'][$x]='no record';
			}
			
					//$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$i) ->setAutoSize(true);;
				//	$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$i);
					$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$ans['username'][$x] );
					$exc->getActiveSheet()->setCellValue('B' . (string)($i + 1),$ans['leadId'][$x] );
					$exc->getActiveSheet()->setCellValue('C' . (string)($i + 1),$ans['pauseSec'][$x] );
					$exc->getActiveSheet()->setCellValue('D' . (string)($i + 1),$ans['waitSec'][$x] );
					$exc->getActiveSheet()->setCellValue('E' . (string)($i + 1),$ans['talkSec'][$x] );
					$exc->getActiveSheet()->setCellValue('F' . (string)($i + 1),$ans['dispoSec'][$x] );
					$exc->getActiveSheet()->setCellValue('G' . (string)($i + 1),$ans['deadTime'][$x] );
					
					
					$exc->getActiveSheet()->getStyle('A' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('B' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('C' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('D' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('E' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('F' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('G' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					
					
					$exc->getActiveSheet()->getStyle('H' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('I' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('J' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('K' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('L' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('M' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('N' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					$i++;
				}
	}
				// Setting width
					$exc->getActiveSheet()->getColumnDimension('B')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('E')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('F')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('G')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('H')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('I')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('J')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('k')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('L')->setWidth(15);
					$exc->getActiveSheet()->getColumnDimension('M')->setWidth(100);
					$exc->getActiveSheet()->getColumnDimension('N')->setWidth(15);
					
					
					//Alignment Centre  
					$exc->getActiveSheet()->getStyle('A' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('B' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('C' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('D' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('E' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('F' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('G' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('H' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('I' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('J' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('K' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('L' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('M' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$exc->getActiveSheet()->getStyle('N' . (string)($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
					
					//change the font size
            $exc->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
            //make the font become bold
			$exc->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            $exc->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('K1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('L1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('M1')->getFont()->setBold(true);
			$exc->getActiveSheet()->getStyle('N1')->getFont()->setBold(true);
			
			
			
			 $exc->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('K1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$exc->getActiveSheet()->getStyle('N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
		//	$dte=date('d-m-Y',strtotime($dt));
			$fname='';
			$fname='AgentReport'.$startDate.'to'.$endDate.'.xls';
		  


header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename="'.$fname.'"'); //tell browser what's the file name
header('Cache-Control: max-age=0'); //no cache
             
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
$objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');  
//force user to download the Excel file without writing it to server's HD
       $objWriter->save($fname);
        $objWriter->save('php://output');
		
	}
} /* ---------------------Controller ends here------------------------------- */