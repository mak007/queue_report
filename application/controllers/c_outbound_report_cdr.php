<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


Class C_outbound_report_cdr extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('utilities_model');
        $this->load->model('admin_model');
        $this->load->model('outbound_report_cdr_model');
      
    }
    
    function index() {
        //echo 'Production<br/><br/><i>under construction</i>' ;
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data=$this->utilities_model->init_data();
        $this->load->view('v_outbound_report_cdr', $data);
    }
    
    function get_extension_list(){
	    $ans=$this->outbound_report_cdr_model->get_extension_list();
            echo $ans;
	}
        function get_extension_list_new(){
	    $ans=$this->outbound_report_cdr_model->get_extension_list_new();
            echo $ans;
	}
    function get_outbound_cdr_report(){
            //$data = $this->input->post("data");
	    $ans=$this->outbound_report_cdr_model->get_outbound_cdr_report();
            echo $ans;
	}
    function get_outbound_Eachcall_report(){
            $ans=$this->outbound_report_cdr_model->get_outbound_Eachcall_report();
            echo $ans;
	}  
        
    /*Added by Rajnandini Lokhande on 1/12/2016*/
    
    function extra_report() {
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_extra_report', $data);
    }
    
    function get_queue_list(){
	    $ans=$this->outbound_report_cdr_model->get_queue_list();
            echo $ans;
	}
        
    function get_queue_list1(){
	    $ans=$this->outbound_report_cdr_model->get_queue_list1();
            echo $ans;
	}
        
    /*Added by Rajnandini Lokhande on 07/12/2016*/
    function getObTeam() 
    {
        $ans = $this->outbound_report_cdr_model->getObTeam();
        echo $ans;
    }

    function GenerateQueueExcel() {
        $startDate = $this->input->post('datepickerStartDate');
        $endDate = $this->input->post('datepickerEndDate');
        $dt1 = date('Y-m-d', strtotime($startDate));
        $dt2 = date('Y-m-d', strtotime($endDate));
        //$queueName = $this->input->post('select_queue_list');
        $ans = $this->outbound_report_cdr_model->generateQueueExcel($startDate, $endDate);
        //print_r($ans);exit;
        $data = count($ans);
        //echo $data;exit;
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Daywise Service Level');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'SR NO');
        $exc->getActiveSheet()->setCellValue('B1', 'Call Date');
        $exc->getActiveSheet()->setCellValue('C1', 'Source ');
        $exc->getActiveSheet()->setCellValue('D1', 'Destination');
        $exc->getActiveSheet()->setCellValue('E1', 'Duration');
        $exc->getActiveSheet()->setCellValue('F1', 'Bill Sec');
        $exc->getActiveSheet()->setCellValue('G1', 'Disposition');
        $exc->getActiveSheet()->setCellValue('H1', 'Recording file');
        $exc->getActiveSheet()->setCellValue('I1', 'Ivr level1');
        $exc->getActiveSheet()->setCellValue('J1', 'Ivr level2');
        $exc->getActiveSheet()->setCellValue('K1', 'Ivr level3');
        $exc->getActiveSheet()->setCellValue('L1', 'Ivr level4');

       /* $i = 1;
        for ($x = 0; $x < $data; $x++) {
            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $i);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans[$x]['calldate']);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans[$x]['src']);
            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans[$x]['dst']);
            $exc->getActiveSheet()->setCellValue('E' . (string) ($i + 1), $ans[$x]['duration']);
            $exc->getActiveSheet()->setCellValue('F' . (string) ($i + 1), $ans[$x]['billsec']);
            $exc->getActiveSheet()->setCellValue('G' . (string) ($i + 1), $ans[$x]['disposition']);
            $exc->getActiveSheet()->setCellValue('H' . (string) ($i + 1), $ans[$x]['recordingfile']);
            $exc->getActiveSheet()->setCellValue('I' . (string) ($i + 1), $ans[$x]['ivrlevel1']);
            $exc->getActiveSheet()->setCellValue('J' . (string) ($i + 1), $ans[$x]['ivrlevel2']);
            $exc->getActiveSheet()->setCellValue('K' . (string) ($i + 1), $ans[$x]['ivrlevel3']);
            $exc->getActiveSheet()->setCellValue('L' . (string) ($i + 1), $ans[$x]['ivrlevel4']);
            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('F' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('G' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('H' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('I' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('J' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('K' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('L' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('I')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $i++;
        }*/
        $fname = '';
        $fname = 'After Call hours Report ' . $startDate . ' to ' . $endDate . '.xls';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
        $objWriter->save($fname);
        $objWriter->save('php://output');
    }
    
    /*
    * Author : Rahul 20.12.2016 | Purpose : Outbound SLA Report | Requested By: 
    */
    function get_extensionList() 
    {
        $ans = $this->outbound_report_cdr_model->get_extensionList();
        echo $ans;
    }
    function ob_SLAreport(){
        $ans = $this->outbound_report_cdr_model->ob_SLAreport();
        echo $ans;
    }
    // End 20.12.2016
    function getCallAbandon($newStartDate, $newEndDate)
    {
        #pass variable $starttime in date formate and  then pass to url
        $newStartDate = urldecode($newStartDate);
         #pass variable $endtime in date formate and  then pass to url
        $newEndDate = urldecode($newEndDate);
         #ashwini end here on 14th_dec_2016
        $columns = array('Date','Phone Number','Queue','Queue waiting time');

        $order_str = null;
        $search = null;
        $this->load->model('outbound_report_cdr_model');
        $result = $this->outbound_report_cdr_model->getCallAbandon($newStartDate,$newEndDate);
        $newStartDate=str_replace("-","/",$newStartDate);
        $newEndDate=str_replace("-","/",$newEndDate);
        $newStartDate1 = date("Y-m-d", strtotime($newStartDate));/*Converting Start Date formate form Y-m-d H:i:s to Y-m-d*/
        $startDate1 = str_replace("-","_",$newStartDate1);/* Replacing the - by _ in Date*/

        $newEndDate1 = date("Y-m-d", strtotime($newEndDate));/*Converting End Date formate form Y-m-d H:i:s to Y-m-d*/
        //change datetime variable
        $newEndDate1 = str_replace("-","_",$newEndDate1);/* Replacing the - by _ in Date*/

        $filename = "All_Abandon_Report_".$startDate1."_To_".$newEndDate1.".csv";/* Here is File name genrating including Start and End Date*/
        /*----End Of Mohan Code-----*/
        //$filename = "uloaded_".strtotime("now").'.csv';
        $this->create_csv_file_abandon($filename,$columns, $result);
    }
    function create_csv_file_abandon($file_name,$columns, $result)
    {            
        try{
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename='.$file_name);
            $output = fopen("php://output", "w");
            fputcsv($output,$columns); 
            $arr=array();
            $i=0;
            foreach ($result->result_array() as $aRow)
            {   
                $arr[$i++]=$time=$aRow['time'];
                $arr[$i++]=$callid=$aRow['cli'];
                $arr[$i++]=$callid=$aRow['qname'];
                $arr[$i++]=$callid=$aRow['time_queue_abandon'];
                fputcsv($output, $arr);
                $arr=null;
                $i=0;
            }
            fclose($output);
        } catch (Exception $ex){

            }        
    }
} 
?>

