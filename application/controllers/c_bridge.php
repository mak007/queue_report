<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class C_bridge extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('utilities_model');
        $this->load->model('bridge_model');
    }

    function index() {
        $data = $this->utilities_model->init_data();
        $this->load->view('v_bridge_index',$data);
    }
    
    /* Validation the entered password and username is correct or not for Login into bridge*/
    function bridge_login_check(){
        $arr = $_POST['data'];
        $ans = $this->bridge_model->bridge_login_check($arr);
        echo $ans;
    }
    /* Load the bridge Control for password reset*/
    /* Getting the DID number bridgereset View*/
    function get_did_number(){
        $this->load->model('bridge_model');
        $ans = $this->bridge_model->get_did_number();
        echo $ans;
    }
    
    /* Getting the DID number bridgereset View*/
    function get_did_number_forReset(){
        $this->load->model('bridge_model');
        $ans = $this->bridge_model->get_did_number_forReset();
        echo $ans;
    }
    
    /* Resetting the password for Bridge*/
    function bridge_reset_pass(){
        $arr = $_POST['data'];
        $u_old_pwd = $arr['u_old_pwd'];
        $qry = "SELECT * FROM conference WHERE Password='$u_old_pwd'";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0){
            $this->load->model('bridge_model');
            $ans = $this->bridge_model->bridge_reset_pass($arr);
            echo $ans;
        } else {
            return 0;
        }
    }
    
    function save_addDid() {
        $ans=$this->bridge_model->save_addDid();
        echo $ans;
    }
    
    function save_addContact() {
        $ans=$this->bridge_model->save_addContact();
        echo $ans;
    }
    
    function get_didcontact_List($conf_id) 
    {
        $ans = $this->bridge_model->get_didcontact_List($conf_id);
        echo $ans;
    }
    
    function start_conference($didno) 
    {
        $res=json_decode($_POST['rpt']);
        if(!empty($res)){
            foreach($res as $val){
               $arr[]='91'.$val->value;
           }
           $contact=implode('|',$arr);  
           $ans = $this->bridge_model->getDidNumber($didno);
           $did_no=json_decode($ans);
           $did=$did_no[0]->did_no;
           echo $url='http://'.CONF_SERVER.'/click2call/CreateNJoinConf.php?ParticipantsNumbers='.$contact.'&ConferenceNumber='.$did.'&ConferenceCallerID='.$did.'&SecKey=2c41c5e95e052e9d03f423e9f578b9b3';
           $curl = curl_init();
           curl_setopt($curl, CURLOPT_URL, $url);
           curl_setopt($curl, CURLOPT_HEADER, false);
           curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
           $json_response = curl_exec($curl);
           echo 1;
        }
        echo 0;
    }
}

//------------------------------Controller ends here---------------------------
