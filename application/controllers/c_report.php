<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class C_report extends CI_Controller {

    public $campaign;

    function __construct() {
        parent::__construct();
        $this->load->model('utilities_model');
        $this->load->model('imports_model');
        $this->load->model('reports_model');
        $this->load->model('outbound_report_cdr_model');
    }

    function index() {

        $data = $this->utilities_model->init_data();
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $this->load->view('v_landing_page', $data);
    }

    function index23() {

        $data = $this->utilities_model->init_data();
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $this->load->view('v_landing_page23', $data);
    }

    function day_wise_sl() {
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_day_wise_sl', $data);
    }

    function each_call_details() {
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_each_call_details', $data);
    }
    /* Added by :Mangesh Kamble 14-12-2016
    Google Docs : C-Square Outbound Report
     *      */
    function ob_agent_wise() {
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_ob_agent_wise', $data);
    }

    function intra_day_sl() {
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_intra_day_sl', $data);
    }

    function cdr_excel() {
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_cdr_excel', $data);
    }

    /* function selectQueue(){

      $table_data = json_decode($_POST['rpt'], true);
      foreach ($table_data as $value) {
      $$value["name"] = $value["value"];
      //echo $value["name"] . "=" . $$value["name"];
      echo "<br/>";
      }
      $dt1=$this->security->xss_clean($table_data[0]['value']);
      $dt2=$this->security->xss_clean($table_data[1]['value']);

      $startDate=date('Y-m-d', strtotime($dt1));
      $endDate=date('Y-m-d', strtotime($dt2));


      //echo $startDate;
      $startTime=strtotime($startDate."00:00:00");
      $endTime=strtotime($endDate."23:59:59");

      //$startTime=strtotime($startDate);
      //	$endTime=strtotime($endDate);

      $newStartTime=$startTime+5.5*60;
      $newendTime=$endTime+5.5*60;

      //echo $startTime;exit;
      $ans=$this->reports_model->selectQueue($newStartTime,$newendTime);
      echo $ans;


      } */

//rahul
    function selecttimeinterval() {
        $ans = $this->reports_model->selecttimeinterval();
        echo $ans;
    }

//rahul
    function selectQueue() {

        $ans = $this->reports_model->selectQueue();
        echo $ans;
    }

    function day_wise_sl_report() {
        $ans2 = $this->reports_model->day_wise_sl_report();
        echo $ans2;
    }

    function each_call_details_report() {
        $arr=$this->input->post('rpt');
        //print_r($arr);exit;
        $ans2 = $this->reports_model->each_call_details_report($arr);
        echo $ans2;
    }
    function agentWiseOBreport() {
        $arr=$this->input->post('rpt');
        //print_r($arr);exit;
        $ans2 = $this->reports_model->agentWiseOBreport($arr);
        echo $ans2;
    }

    function intra_day_sl_report() {
        $table_data = json_decode($_POST['rpt'], true);
        foreach ($table_data as $value) {
            $$value["name"] = $value["value"];
        }
        $dt1 = $this->security->xss_clean($table_data[0]['value']);
        $interval = $this->security->xss_clean($table_data[1]['value']);
        //echo $dt1.$dt2;exit;
        $ans2 = $this->reports_model->intra_day_sl_report($dt1, $interval);
        echo $ans2;
    }

    function GenerateExcel() {
        $dt1 = $this->input->post('inpstdate');
        $dt2 = $this->input->post('inpEnddate');
        //echo $queueName;exit;
        $startDate = date('Y-m-d', strtotime($dt1));
        $endDate = date('Y-m-d', strtotime($dt2));
        //echo $dt1.$dt2;
        $ans = $this->reports_model->GenerateExcel($startDate, $endDate);
//print_r($ans);exit;
        //echo $ans;
        //$data=$this->db->query("select Count(Distinct customer_id), agent_id from interactions where dt between '$startDate' and '$endDate'");
        //$qry=$this->db->query("Select dt,count(distinct customer_id) as customer_id,count(customer_id) as num from interactions where date(dt)= '$now' ");

        $data = count($ans);
        //echo $data;exit;
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Daywise Service Level');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'Number');
        $exc->getActiveSheet()->setCellValue('B1', 'Date');

        $exc->getActiveSheet()->setCellValue('C1', 'Calls offered');
        $exc->getActiveSheet()->setCellValue('D1', 'Calls answered');
        $exc->getActiveSheet()->setCellValue('E1', 'ATT');

        $exc->getActiveSheet()->setCellValue('F1', 'Calls Abandoned');
        $exc->getActiveSheet()->setCellValue('G1', 'Short Abandoned');
        $exc->getActiveSheet()->setCellValue('H1', 'Answered Level(all)');

        $exc->getActiveSheet()->setCellValue('I1', 'Answered within SL');
        $exc->getActiveSheet()->setCellValue('J1', 'SL(ALl)');
        $exc->getActiveSheet()->setCellValue('K1', 'SL(short Abandoned)');
        $i = 1;
        for ($x = 0; $x < $data; $x++) {

            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $i);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans[$x]['now']);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans[$x]['did_count']);

            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans[$x]['complete_count']);
            $exc->getActiveSheet()->setCellValue('E' . (string) ($i + 1), $ans[$x]['ATT']);

            $exc->getActiveSheet()->setCellValue('F' . (string) ($i + 1), $ans[$x]['abandon_count']);
            $exc->getActiveSheet()->setCellValue('G' . (string) ($i + 1), $ans[$x]['abandon_count_qtime']);

            $exc->getActiveSheet()->setCellValue('H' . (string) ($i + 1), $ans[$x]['answered_level']);

            $exc->getActiveSheet()->setCellValue('I' . (string) ($i + 1), $ans[$x]['complete_count_qtime']);
            $exc->getActiveSheet()->setCellValue('J' . (string) ($i + 1), $ans[$x]['answered_Service_level']);

            $exc->getActiveSheet()->setCellValue('K' . (string) ($i + 1), $ans[$x]['short_abandoned']);




            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('F' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('G' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('H' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('I' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('J' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('K' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


            $exc->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('I')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('K')->setWidth(20);




            $i++;
        }
        // Setting width
        //Alignment Centre  
        //	$dte=date('d-m-Y',strtotime($dt));
        $fname = '';
        $fname = 'Daywise Service Level ' . $startDate . ' to ' . $endDate . '.xls';



        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
//force user to download the Excel file without writing it to server's HD
        $objWriter->save($fname);
        $objWriter->save('php://output');
    }

    function GenerateExcelEachCall() {
        $dt1 = $this->input->post('inpstdate');
        $dt2 = $this->input->post('inpEnddate');
        if($dt1==''){
            echo 'Please select start date.';exit;
        }
        if($dt2==''){
            echo 'Please select end date.';exit;
        }
        $startDate = date('Y-m-d', strtotime($dt1));
        $endDate = date('Y-m-d', strtotime($dt2));
        //echo $dt1.$dt2;
        $ans = $this->reports_model->GenerateExcelEachCall($startDate, $endDate);
//print_r($ans);exit;
        //echo $ans;
        //$data=$this->db->query("select Count(Distinct customer_id), agent_id from interactions where dt between '$startDate' and '$endDate'");
        //$qry=$this->db->query("Select dt,count(distinct customer_id) as customer_id,count(customer_id) as num from interactions where date(dt)= '$now' ");

        $data = count($ans);
        //echo $data;exit;
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Daywise Service Level');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'Number');
        $exc->getActiveSheet()->setCellValue('B1', 'Phone Number');

        $exc->getActiveSheet()->setCellValue('C1', 'Date');
        $exc->getActiveSheet()->setCellValue('D1', 'Hours');
        $exc->getActiveSheet()->setCellValue('E1', 'Minutes');

        $exc->getActiveSheet()->setCellValue('F1', 'Seconds');
        $exc->getActiveSheet()->setCellValue('G1', 'ANS/ABN');
        $exc->getActiveSheet()->setCellValue('H1', 'Wait Time');

        $exc->getActiveSheet()->setCellValue('I1', 'Talktime');
        $exc->getActiveSheet()->setCellValue('J1', 'Agent');

        $i = 1;
        for ($x = 0; $x < $data; $x++) {

            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $i);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans[$x]['cli']);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans[$x]['date']);

            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans[$x]['hour']);
            $exc->getActiveSheet()->setCellValue('E' . (string) ($i + 1), $ans[$x]['minutes']);

            $exc->getActiveSheet()->setCellValue('F' . (string) ($i + 1), $ans[$x]['sec']);
            $exc->getActiveSheet()->setCellValue('G' . (string) ($i + 1), $ans[$x]['Ab_Ans']);

            $exc->getActiveSheet()->setCellValue('H' . (string) ($i + 1), $ans[$x]['time_queue']);

            $exc->getActiveSheet()->setCellValue('I' . (string) ($i + 1), $ans[$x]['talk_time']);
            $exc->getActiveSheet()->setCellValue('J' . (string) ($i + 1), $ans[$x]['agent']);




            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('F' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('G' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('H' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('I' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('J' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



            $exc->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('I')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('J')->setWidth(20);





            $i++;
        }
        // Setting width
        //Alignment Centre  
        //	$dte=date('d-m-Y',strtotime($dt));
        $fname = '';
        $fname = 'Each Day Call Report ' . $startDate . ' to ' . $endDate . '.xls';



        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
//force user to download the Excel file without writing it to server's HD
        $objWriter->save($fname);
        $objWriter->save('php://output');
    }

    function excelintra_day_sl_report() {
        $dt1 = $this->input->post('inpstdate');
        $interaval = $this->input->post('inp_set_interval');
        if($dt1==''){
            echo 'Please select date.';exit;
        }
        if($interaval==''){
            echo 'Please select interval time';exit;
        }
        
        //echo $interaval;exit;
        $startDate = date('Y-m-d', strtotime($dt1));
        //echo $startDate;exit;
        //echo $dt1.$dt2;
        $ans = $this->reports_model->excelintra_day_sl_report($startDate, $interaval);



        //print_r($ans);exit;
        //$data=$this->db->query("select Count(Distinct customer_id), agent_id from interactions where dt between '$startDate' and '$endDate'");
        //$qry=$this->db->query("Select dt,count(distinct customer_id) as customer_id,count(customer_id) as num from interactions where date(dt)= '$now' ");

        $data = count($ans);
        //echo $data;exit;
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Daywise Service Level');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'Number');
        $exc->getActiveSheet()->setCellValue('B1', 'Time');

        $exc->getActiveSheet()->setCellValue('C1', 'Calls offered');
        $exc->getActiveSheet()->setCellValue('D1', 'Calls answered');
        $exc->getActiveSheet()->setCellValue('E1', 'ATT');


        $exc->getActiveSheet()->setCellValue('F1', 'Answered Level(all)');

        $exc->getActiveSheet()->setCellValue('G1', 'Answered within SL');
        $exc->getActiveSheet()->setCellValue('H1', 'SL(ALl)');
        $exc->getActiveSheet()->setCellValue('I1', 'SL(short Abandoned)');
        $i = 1;
        for ($x = 0; $x < $data; $x++) {

            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $i);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans[$x]['time_interval']);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans[$x]['did_count']);

            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans[$x]['complete_count']);
            $exc->getActiveSheet()->setCellValue('E' . (string) ($i + 1), $ans[$x]['ATT']);



            $exc->getActiveSheet()->setCellValue('F' . (string) ($i + 1), $ans[$x]['answered_level']);

            $exc->getActiveSheet()->setCellValue('G' . (string) ($i + 1), $ans[$x]['complete_count_qtime']);
            $exc->getActiveSheet()->setCellValue('H' . (string) ($i + 1), $ans[$x]['answered_Service_level']);

            $exc->getActiveSheet()->setCellValue('I' . (string) ($i + 1), $ans[$x]['short_abandoned']);




            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('F' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('G' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('H' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('I' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


            $exc->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('I')->setWidth(30);






            $i++;
        }
        // Setting width
        //Alignment Centre  
        //	$dte=date('d-m-Y',strtotime($dt));
        $fname = '';
        $fname = 'Intra Day SL ' . $startDate . '.xls';



        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
//force user to download the Excel file without writing it to server's HD
        $objWriter->save($fname);
        $objWriter->save('php://output');
    }

    function genrate_cdr_excel() {

        $CI = &get_instance();
        $this->db2 = $CI->load->database('db2', TRUE);

        $dt1 = $this->input->post('inpstdate');
        $dt2 = $this->input->post('inpEnddate');
        if($dt1==''){
            echo 'Please select start date';exit;
        }
        if($dt2==''){
           echo 'Please select end date';exit; 
        }
        $phone = $this->input->post('inpPhone');
        //	$queueName=$this->input->post('inpQueue1');
        //echo $queueName;exit;
        $startDate = date('Y-m-d', strtotime($dt1));
        $startDate = $startDate . " 00:00:01";
        $endDate = date('Y-m-d', strtotime($dt2));
        $endDate = $endDate . " 23:59:59";
        //echo $startDate;exit;
        $ans = $this->reports_model->genrate_cdr_excel($startDate, $endDate, $phone);
        //print_r($ans);exit;
//$ans= $this->reports_model->customerbase_excel();
        //print_r($ans);exit;
        /* if($phone==0){
          $qryy=$this->db2->query("SELECT * FROM cdr where(`dst` = 's' or `dst`=200) and calldate>='$startDate' and calldate<='$endDate'");
          }
          if($phone!=''){
          $qryy=$this->db2->query("SELECT * FROM cdr where(`dst` = 's' or `dst`=200) and calldate>='$startDate' and calldate<='$endDate' and src='$phone'");
          } */
        $data = count($ans['date_time']);
        //print_r($data);exit;
        //   $data=$qryy->num_rows();
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Worksheet');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'Date Time');
        $exc->getActiveSheet()->setCellValue('B1', 'Phone No.');
        $exc->getActiveSheet()->setCellValue('C1', 'Queue');
        $exc->getActiveSheet()->setCellValue('D1', 'Bill Time');
        $exc->getActiveSheet()->setCellValue('E1', 'Status');

        //$exc->getActiveSheet()->setCellValue('B1','Customer Name');
        $i = 1;
        for ($x = 0; $x < $data; $x++) {

            if (!isset($ans['date_time'][$x])) {
                $ans['date_time'][$x] = 'NO RECORD';
                ;
            }
            if (!isset($ans['phoneNo'][$x])) {
                $ans['phoneNo'][$x] = 'NO RECORD';
                ;
            }
            if (!isset($ans['queue'][$x])) {
                $ans['queue'][$x] = 'NO RECORD';
                ;
            }
            if (!isset($ans['bill_time'][$x])) {
                $ans['bill_time'][$x] = 'NO RECORD';
                ;
            }
            if (!isset($ans['status'][$x])) {
                $ans['status'][$x] = 'NO RECORD';
                ;
            }


            //$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$i) ->setAutoSize(true);;
            //$exc->getActiveSheet()->setCellValue('A' . (string)($i + 1),$i);
            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $ans['date_time'][$x]);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans['phoneNo'][$x]);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans['queue'][$x]);
            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans['bill_time'][$x]);
            $exc->getActiveSheet()->setCellValue('E' . (string) ($i + 1), $ans['status'][$x]);
            $exc->getActiveSheet()->setCellValue('F' . (string) ($i + 1), '');


            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



            $i++;
        }
        // Setting width
        $exc->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $exc->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $exc->getActiveSheet()->getColumnDimension('E')->setWidth(20);



        //Alignment Centre  
        $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);



        //change the font size
        // $exc->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
        //make the font become bold

        $exc->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $exc->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
        $exc->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
        $exc->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
        $exc->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
        $exc->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);



        $exc->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $exc->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


        //$dte=date('d-m-Y',strtotime($dt));
        $fname = '';
        $fname = 'CDRExcel.xls';



        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
//if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
//force user to download the Excel file without writing it to server's HD
        $objWriter->save($fname);
        $objWriter->save('php://output');

        //print_r($ans);exit;
        //$qryy = $this->db->query("SELECT * FROM `temp_orders` WHERE `date` >= '$startDate' AND `date`<= '$endDate'" );
    }

    function display_records($start, $end, $phone) {
        $ans = $this->reports_model->display_records($start, $end, $phone);
        echo $ans;
    }
    
    #---------Date: 1/12/2016  --- Mangesh kamble ---------------------------
    function report_config() {
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_reports_config', $data);
    }
    function saveReportConfig() {
        $arr=$this->input->post('rpt');
        //print_r($arr);exit;
        $ans=$this->reports_model->saveReportConfig($arr);
        echo $ans;
    }
    
    function save_addUser() {
        $ans=$this->reports_model->save_addUser();
        echo $ans;
    }
    
    function save_addDid() {
        $ans=$this->reports_model->save_addDid();
        echo $ans;
    }
    
    function save_addContact() {
        $ans=$this->reports_model->save_addContact();
        echo $ans;
    }
    
    function getReportsConfig() {
        $ans=$this->reports_model->getReportsConfig();
        echo $ans;
    }
    function getUserList() {
        $ans=$this->reports_model->getUserList();
        echo $ans;
    }
    
    function save_user() {
        $arr=$_POST['rpt'];
        $ans=$this->reports_model->save_user($arr);
        echo $ans;
    }

     function GenerateQueueExcel() {
         $startDate = $this->input->post('datepickerStartDate');
         $endDate = $this->input->post('datepickerEndDate');
        if($startDate==''){
            echo 'Please select start date';exit;
        }
        
        if($endDate==''){
            echo 'Please select end date';exit;
        }
        $dt1 = date('Y-m-d', strtotime($startDate));
        $dt2 = date('Y-m-d', strtotime($endDate));
        //$queueName = $this->input->post('select_queue_list');
        $ans = $this->outbound_report_cdr_model->generateQueueExcel($startDate, $endDate);
        //print_r($ans);exit;
        $data = count($ans);
        //echo $data;exit;
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Daywise Service Level');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'SR NO');
        $exc->getActiveSheet()->setCellValue('B1', 'Call Date');
        $exc->getActiveSheet()->setCellValue('C1', 'Source ');
        $exc->getActiveSheet()->setCellValue('D1', 'Destination');
        $exc->getActiveSheet()->setCellValue('E1', 'Duration');
        $exc->getActiveSheet()->setCellValue('F1', 'Bill Sec');
        $exc->getActiveSheet()->setCellValue('G1', 'Disposition');
        $exc->getActiveSheet()->setCellValue('H1', 'Recording file');
        $exc->getActiveSheet()->setCellValue('I1', 'Ivr level1');
        $exc->getActiveSheet()->setCellValue('J1', 'Ivr level2');
        $exc->getActiveSheet()->setCellValue('K1', 'Ivr level3');
        $exc->getActiveSheet()->setCellValue('L1', 'Ivr level4');

        $i = 1;
        for ($x = 0; $x < $data; $x++) {
            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $i);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans[$x]['calldate']);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans[$x]['src']);
            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans[$x]['dst']);
            $exc->getActiveSheet()->setCellValue('E' . (string) ($i + 1), $ans[$x]['duration']);
            $exc->getActiveSheet()->setCellValue('F' . (string) ($i + 1), $ans[$x]['billsec']);
            $exc->getActiveSheet()->setCellValue('G' . (string) ($i + 1), $ans[$x]['disposition']);
            $exc->getActiveSheet()->setCellValue('H' . (string) ($i + 1), $ans[$x]['recordingfile']);
            $exc->getActiveSheet()->setCellValue('I' . (string) ($i + 1), $ans[$x]['ivrlevel1']);
            $exc->getActiveSheet()->setCellValue('J' . (string) ($i + 1), $ans[$x]['ivrlevel2']);
            $exc->getActiveSheet()->setCellValue('K' . (string) ($i + 1), $ans[$x]['ivrlevel3']);
            $exc->getActiveSheet()->setCellValue('L' . (string) ($i + 1), $ans[$x]['ivrlevel4']);
            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('F' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('G' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('H' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('I' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('J' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('K' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('L' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('I')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $i++;
        }
        $fname = '';
        $fname = 'After Call hours Report ' . $startDate . ' to ' . $endDate . '.xls';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
        $objWriter->save($fname);
        $objWriter->save('php://output');
    }
    
    function generateCall_recivedExcel() {
        $startDate = $this->input->post('datepickerStartDate');
        $endDate = $this->input->post('datepickerEndDate');
        $call_To = $this->input->post('callto');
        if($startDate==''){
            echo 'Please select start date';
            exit;
        }
        if($endDate==''){
            echo 'Please select end date';
            exit;
        }
        if($call_To==''){
            echo 'Please Enter the Call To Number';
            exit;
        }
        $dt1 = date('Y-m-d', strtotime($startDate));
        $dt2 = date('Y-m-d', strtotime($endDate));
        //echo "s_ate-:".$startDate."e_date-:".$endDate;exit;
        $ans = $this->outbound_report_cdr_model->generateCall_recivedExcel($startDate, $endDate,$call_To);
        $data = count($ans);
        //echo $data;exit;
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Call Received');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'SR NO');
        $exc->getActiveSheet()->setCellValue('B1', 'Call Date');
        $exc->getActiveSheet()->setCellValue('C1', 'Source ');
        $exc->getActiveSheet()->setCellValue('D1', 'Destination');

        $i = 1;
        for ($x = 0; $x < $data; $x++) {
            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $i);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans[$x]['calldate']);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans[$x]['src']);
            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans[$x]['dst']);
            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $i++;
        }
        $fname = '';
        $fname = 'Call_Received_Specific_Extension_OR_DID) ' . $startDate . ' to ' . $endDate . '.xls';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
        $objWriter->save($fname);
        $objWriter->save('php://output');
    }
    
     function generateExcelAgentWiseOB() {
        $inpstdatetime= $this->input->post('inpstdate');
        $inpstdate= date('Y-m-d',  strtotime($inpstdatetime));
        $inpEnddatetime = $this->input->post('inpEnddate');
        $inpEnddate= date('Y-m-d',  strtotime($inpEnddatetime));
        $ans = $this->outbound_report_cdr_model->generateExcelAgentWiseOB();
        //print_r($ans);exit;
        $data = count($ans);
        //echo $data;exit;
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Agent wise outbound report');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'SR NO');
        $exc->getActiveSheet()->setCellValue('B1', 'Call Date');
        $exc->getActiveSheet()->setCellValue('C1', 'Agent Name ');
        $exc->getActiveSheet()->setCellValue('D1', 'Extn');
        $exc->getActiveSheet()->setCellValue('E1', 'No of calls');
        $exc->getActiveSheet()->setCellValue('F1', 'Calls connected');
        $exc->getActiveSheet()->setCellValue('G1', 'Total Talktime');
        $exc->getActiveSheet()->setCellValue('H1', 'Avg Talktime');
        $i = 1;
        for ($x = 0; $x < $data; $x++) {
            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $i);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans[$x]['date']);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans[$x]['agent_name']);
            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans[$x]['extn']);
            $exc->getActiveSheet()->setCellValue('E' . (string) ($i + 1), $ans[$x]['no_of_dials']);
            $exc->getActiveSheet()->setCellValue('F' . (string) ($i + 1), $ans[$x]['calls_connected']);
            $exc->getActiveSheet()->setCellValue('G' . (string) ($i + 1), $ans[$x]['tot_duration']);
            $exc->getActiveSheet()->setCellValue('H' . (string) ($i + 1), $ans[$x]['avg_duration']);
            
            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('F' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('G' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('H' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
            $exc->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('H')->setWidth(30);
   
            $i++;
        }
        $fname = '';
        $fname = 'Agent wise outbound report ' . $inpstdate . ' to ' . $inpEnddate . '.xls';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
        $objWriter->save($fname);
        $objWriter->save('php://output');
    }
    
    function getTransferDetails(){
        $ans=$this->reports_model->getTransferDetails();
        echo $ans;
    }
    
    /*
     * Author : Rahul 20.12.2016 | Purpose : Outbound SLA Report | Requested By: 
     */
    function ob_sla_report(){
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_ob_sla_report', $data);
    }
    
    function generateExcelOB_slaReport() {
        $inpstdatetime= $this->input->post('inpFromDate11');
        $inpstdate= date('Y-m-d',  strtotime($inpstdatetime));
        $inpEnddatetime = $this->input->post('inpToDate11');
        $inpEnddate= date('Y-m-d',  strtotime($inpEnddatetime));
        $queueName = $this->input->post('sla_extn_list1');
        //echo "$inpstdatetime------$inpEnddatetime-------$queueName";exit;
        $ans = $this->outbound_report_cdr_model->ob_SLAreport_excelData($inpstdatetime, $inpEnddatetime, $queueName);
        //print_r($ans);exit;
        $data = count($ans);
        //echo $data;exit;
        $this->load->library('excel');
        //activate worksheet number 1
        $exc = new PHPExcel();
        $exc->setActiveSheetIndex(0);
        //name the worksheet
        $exc->getActiveSheet()->setTitle('Outbound SLA Report');
        $count = count($ans);
        $exc->getActiveSheet()->setCellValue('A1', 'SR NO');
        $exc->getActiveSheet()->setCellValue('B1', 'Call Date');
        $exc->getActiveSheet()->setCellValue('C1', 'Queue Name ');
        $exc->getActiveSheet()->setCellValue('D1', 'No of calls');
        $exc->getActiveSheet()->setCellValue('E1', 'Calls connected');
        $exc->getActiveSheet()->setCellValue('F1', 'Connect%');
        $exc->getActiveSheet()->setCellValue('G1', 'Total Talktime');
        $exc->getActiveSheet()->setCellValue('H1', 'Avg Talktime');
        $i = 1;
        for ($x = 0; $x < $data; $x++) {
            $exc->getActiveSheet()->setCellValue('A' . (string) ($i + 1), $i);
            $exc->getActiveSheet()->setCellValue('B' . (string) ($i + 1), $ans[$x]->date);
            $exc->getActiveSheet()->setCellValue('C' . (string) ($i + 1), $ans[$x]->iqueue);
            $exc->getActiveSheet()->setCellValue('D' . (string) ($i + 1), $ans[$x]->no_of_dial);
            $exc->getActiveSheet()->setCellValue('E' . (string) ($i + 1), $ans[$x]->calls_connected);
            $exc->getActiveSheet()->setCellValue('F' . (string) ($i + 1), $ans[$x]->conn_per);
            $exc->getActiveSheet()->setCellValue('G' . (string) ($i + 1), $ans[$x]->tot_duration);
            $exc->getActiveSheet()->setCellValue('H' . (string) ($i + 1), $ans[$x]->avg_duration);
            
            $exc->getActiveSheet()->getStyle('A' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('B' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('C' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('D' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('E' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('F' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('G' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $exc->getActiveSheet()->getStyle('H' . (string) ($i + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
            $exc->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $exc->getActiveSheet()->getColumnDimension('F')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            $exc->getActiveSheet()->getColumnDimension('H')->setWidth(30);
   
            $i++;
        }
        $fname = '';
        $fname = 'Outbound sla report ' . $inpstdate . ' to ' . $inpEnddate . '.xls';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $fname . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($exc, 'Excel5');
        $objWriter->save($fname);
        $objWriter->save('php://output');
    }
    //End 20.12.2016
    
    //=========== Code Sdded By Mohan on 24-01-17 ================
    /* Loding the Login View*/
    function bridge_login(){
        $this->load->view('file/header');
        $this->load->view('file/nav');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_bridge_login', $data);
    }
    /* Validation the entered password and username is correct or not for Login into bridge*/
    function bridge_login_check(){
        $data = array('u_name' => $this->input->post('bridge_username'),'u_pass' => $this->input->post('bridge_password'));
        $ans = $this->reports_model->bridge_login_check($data);
        if ($ans == 'false') {
            $data = array(
                'error_message' => 'Invalid Username or Password !!!!'
            );
            $this->load->view('file/header');
            $this->load->view('file/nav');
            $this->load->view('v_bridge_login', $data);
        }else{
            redirect('c_report/bridge_menu');
        }
    }
    /* Load the bridge Control for password reset*/
    function bridge_reset_view(){
        $this->load->view('file/header');
        $this->load->view('file/nav2');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_bridge_control', $data);
    }
    /* Getting the DID number o0n bridgereset View*/
    function get_did_number(){
        $this->load->model('reports_model');
        $ans = $this->reports_model->get_did_number();
        echo $ans;
    }
    /* Resetting the password for Bridge*/
    function bridge_reset_pass(){
        $arr = $_POST['data'];
        $u_old_pwd = $arr['u_old_pwd'];
        $qry = "SELECT * FROM conference WHERE Password='$u_old_pwd'";
        $query = $this->db->query($qry);
        if ($query->num_rows() > 0){
            $this->load->model('reports_model');
            $ans = $this->reports_model->bridge_reset_pass($arr);
            echo $ans;
        } else {
            return 0;
        }
        
    }
      function get_extensionListforinbound(){
        $this->load->model('reports_model');
        $ans = $this->reports_model->get_extensionListforinbound();
        echo $ans;
    }
    
    
    // ======= End of Mohan Code ==============
    
    /*Code Added by Rajnandini*/
    
    function bridge_menu(){
        $this->load->view('file/header');
        $this->load->view('file/nav2');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_bridge_menu', $data);
    }
    function display_did_form(){
        $this->load->view('file/header');
        $this->load->view('file/nav2');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_add_bridge_did', $data);
    }
    function display_contact_form(){
        $this->load->view('file/header');
        $this->load->view('file/nav2');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_add_bridge_contact', $data);
    }
    function display_bridge_controlPanel(){
        $this->load->view('file/header');
        $this->load->view('file/nav2');
        $data = $this->utilities_model->init_data();
        $this->load->view('v_bridge_control_panel', $data);
    }
    function get_didno_list() 
    {
        $ans = $this->reports_model->get_didno_list();
        echo $ans;
    }
    function get_didcontact_List($conf_id) 
    {
        $ans = $this->reports_model->get_didcontact_List($conf_id);
        echo $ans;
    }
    function deleteContact($id) 
    {
        $ans = $this->reports_model->deleteContact($id);
        echo $ans;
    }
    function start_conference($didno) 
    {
        $res=json_decode($_POST['rpt']);
        if(!empty($res)){
            foreach($res as $val){
               $arr[]='91'.$val->value;
           }
           $contact=implode('|',$arr);  
           $ans = $this->reports_model->getDidNumber($didno);
           $did_no=json_decode($ans);
           $did=$did_no[0]->did_no;
           echo $url='http://'.CONF_SERVER.'/click2call/CreateNJoinConf.php?ParticipantsNumbers='.$contact.'&ConferenceNumber='.$did.'&ConferenceCallerID='.$did.'&SecKey=2c41c5e95e052e9d03f423e9f578b9b3';
           $curl = curl_init();
           curl_setopt($curl, CURLOPT_URL, $url);
           curl_setopt($curl, CURLOPT_HEADER, false);
           curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
           $json_response = curl_exec($curl);
           echo 1;
        }
        echo 0;
        
    }
    
    /*end here*/
}

//------------------------------Controller ends here---------------------------
