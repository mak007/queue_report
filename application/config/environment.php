<?php

defined('BASEPATH') OR exit('No direct script access allowed.');

/*
 * ---------------------------------------------------------------
 * PHP ini settings
 * ---------------------------------------------------------------
 */

//ini_set('date.timezone', 'GMT');
// or
@date_default_timezone_set('Asia/Kolkata');

ini_set('memory_limit', '512M');
ini_set('post_max_size', '16M');

ini_set('upload_max_filesize', '16M');
ini_set('max_file_uploads', 20);

ini_set('max_execution_time', 30);
ini_set('max_input_time', 60);
