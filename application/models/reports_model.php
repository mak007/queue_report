<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Reports_model extends CI_Model {

    function __construct() {
        parent::__construct();
        //    $this->db = $this->load->database('default', TRUE);
        //ini_set('max_input_time', 10000);
        //	ini_set('max_execution_time', 10000);
        //ini_set('MAX_EXECUTION_TIME', -1);
        date_default_timezone_set('Asia/Kolkata');
    }

    function day_wise_sl_report() {
        $arr = $this->input->post('rpt');
        $startDate = $arr['inpstdate'];
        $endDate = $arr['inpEnddate'];
        $queueName = $arr['queue'];
        $x = '';
        $queueName = urldecode($queueName);
        if ($queueName == '"0"') {

            $qry = $this->db->select('queue')->get('qname');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->queue . '",';
                }
            }
            $x = rtrim($x, ",");
            $queueName = $x;
        }
        //print_r($queueName);exit;
        $service_level_threshold = 0;
        $short_abandon_threshould = 0;
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $hms_for_day = '00:00:00';
        $hms_for_night = '23:59:59';
        $iDateFrom = $startDate . ' ' . $hms_for_day;
        $iDateTo = $endDate . ' ' . $hms_for_night;
        $msg1 = '';
        $i = 1;
        $count = 0;
        $aryRange = array();
        $aryRange = $this->createDateRangeArray($startDate, $endDate);

        $datecount = count($aryRange);
        //echo $datecount;exit;

        $query1 = $this->db->query("Select * from report_configuration");
        //$result=$conn->query($query1);
        //GMT=$qry1->row()->GMT_offset;

        foreach ($query1->result() as $query11) {
            //$GMT=$query11->GMT_offset;
            $service_level_threshold = $query11->service_level_threshold;
            $short_abandon_threshould = $query11->short_abandon_threshould;
        }

        for ($j = 0; $j < $datecount; $j++) {

            $now = $aryRange[$j];
            //echo $now;exit;

            $msg1.= '<tr>';
            $msg1.='<td>' . $now . '</td>';
            $iDateFrom = $now . " 00:00:00";
            $iDateTo = date('Y-m-d', strtotime($now));
            $iDateTo = $iDateTo . " 23:59:59";
            $qry = "select count(callid) as did_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and  qname IN (" . $queueName . ")";
            $qry1 = $this->db->query($qry);
            if ($qry1->row()->did_count > 0) {
                if ($qry1->num_rows() > 0) {

                    $msg1.='<td>' . $qry1->row()->did_count . '</td>';

                    $qry = "select count(callid) as complete_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action in ('COMPLETECALLER','COMPLETEAGENT') and qname IN (" . $queueName . ")";
                    $qry2 = $this->db->query($qry);
                    if ($qry2->num_rows > 0) {
                        $msg1.='<td>' . $qry2->row()->complete_count . '</td>';
                    }
                    $qry6 = $this->db->query("select avg(talk_time) as ATT from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action in ('COMPLETECALLER','COMPLETEAGENT') and qname IN (" . $queueName . ")");
                    if ($qry6->num_rows > 0) {
                        $msg1.='<td>' . round($qry6->row()->ATT, 2) . '</td>';
                    }

                    $qry3 = $this->db->query("select count(callid) as abandon_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action='ABANDON' and qname IN (" . $queueName . ")");
                    if ($qry3->num_rows > 0) {
                        $msg1.='<td>' . $qry3->row()->abandon_count . '</td>';
                    }
                    $qry4 = $this->db->query("select count(callid) as abandon_count_qtime from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action='ABANDON' and time_queue_abandon <='$short_abandon_threshould' and qname IN (" . $queueName . ")");

                    if ($qry4->num_rows > 0) {
                        $msg1.='<td>' . $qry4->row()->abandon_count_qtime . '</td>';
                    }

                    $answered_level = $qry2->row()->complete_count / $qry1->row()->did_count;
                    if ($answered_level > 0) {
                        $answered_level = round($answered_level * 100, 2);

                        $msg1.='<td>' . $answered_level . '%</td>';
                    }

                    $qry5 = $this->db->query("select count(callid) as complete_count_qtime from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action<> 'ABANDON' and  time_queue_ans <= '$service_level_threshold' and qname IN (" . $queueName . ")");
                    if ($qry5->num_rows > 0) {
                        $msg1.='<td>' . $qry5->row()->complete_count_qtime . '</td>';
                    }

                    $answered_Service_level = $qry5->row()->complete_count_qtime / $qry1->row()->did_count;
                    if ($answered_Service_level > 0) {
                        $answered_Service_level = round($answered_Service_level * 100, 2);

                        $msg1.='<td>' . $answered_Service_level . '%</td>';
                    } else {
                        $msg1.='<td> - </td>';
                    }
                    $diff1 = ($qry1->row()->did_count - $qry4->row()->abandon_count_qtime);
                    if ($diff1 <= 0) {
                        /* Bug :- showing data table error.
                           if  abandon_count_qtime is 0 don't go in next condition directly show blank*/
                         $msg1.='<td> - </td>';
                         $short_abandoned = 0;
                    } else {
                        $short_abandoned = $qry5->row()->complete_count_qtime / $diff1;
                    }
                    if ($short_abandoned > 0) {

                        $short_abandoned = round($short_abandoned * 100, 2);
                        $msg1.='<td>' . $short_abandoned . '%</td>';
                    } else {
                        $msg1.='<td> - </td>';
                    }
                }
            } else {
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
            }
            $msg1.= '</tr>';
        }


        $msg1.='</table>';
        return $msg1;
    }

//rahul
    function selecttimeinterval() {

        //echo $startDate;
        $msg = '<select id="intervalSelect" style="border: 1px solid #c1c1c1; width: 100px;" onchange="set_interval();"><option value="0">Select Interval</option>';
        $msg.='<option value="15">15</option>';
        $msg.='<option value="30">30</option>';
        $msg.='<option value="60">60</option>';

        $msg.='</select>';
        // $msg.='<input type="text" style="display: non;" id="inp_set_interval" name="inp_set_interval" value=""/>'; 

        return $msg;
    }

//rahul

    function selectQueue() {
        $service_level_threshold = 0;
        $short_abandon_threshould = 0;

        //echo $startDate;
        $msg = '<select multiple id="queueSelect" style="border: 1px solid #c1c1c1; width: 100px;" onchange="set_queue();"><option value="0">All</option>';

        $qry = $this->db->query("Select distinct qname from each_call_details");

        foreach ($qry->result() as $row) {
            $msg.='<option value=' . $row->qname . '>' . $row->qname . '</option>';
        }





        $msg.='</select>';
        //$msg.='<input type="text" style="display: non;" id="inpQueue" name="inpQueue" value=""/>'; 

        return $msg;
    }

    function each_call_details_report($arr) {
        //$startDate,$endDate
        $startDate = $arr['inpstdate'];
        $endDate = $arr['inpEnddate'];
        $queue = $arr['queue'];
        $exn_id = $arr['exn_id'];
        //print_r($exn_id);exit;
        $x = '';
        $y='';
        $queue = urldecode($queue);
          $exn_id = urldecode($exn_id);
        if ($queue == '"0"') {

            $qry = $this->db->select('queue')->get('qname');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->queue . '",';
                }
            }
            $x = rtrim($x, ",");
            $queue = $x;
        } else {
            $queue;
        }
//         if($exn_id!='')
//        {
//         
//            if ($exn_id == '"0"') {
//                $qry = $this->db->select('extension_name')->get('extension_list');
//                if ($qry->num_rows() > 0) {
//                    foreach ($qry->result() as $row) {
//                        $y.='"' . $row->extension_name . '",';
//                    }
//                }
//                $y = rtrim($y, ",");
//                $exn_id = $y;
//            } else {
//                $exn_id;
//            }
//        }
        //print_r($exn_id);exit;
        $service_level_threshold = 0;
        $short_abandon_threshould = 0;
        //echo $startDate;
        //echo $endDate;

        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $msg1 = '';
        $i = 1;
        $count = 0;

        $query1 = $this->db->query("Select * from report_configuration");
        //$result=$conn->query($query1);
        //GMT=$qry1->row()->GMT_offset;

        foreach ($query1->result() as $query11) {
            //$GMT=$query11->GMT_offset;
            $service_level_threshold = $query11->service_level_threshold;
            $short_abandon_threshould = $query11->short_abandon_threshould;
        }

        //echo $datecount;
        //echo $now;
        $msg1.= '<table>';

        $newStartTime = $startDate . " 00:00:00";
        $newendTime = $endDate . " 23:59:59";
        //echo $newStartTime;
        //echo $newendTime;//exit;
        //$newStartTime=$startTime+$GMT*60*60;
        //$newendTime=$endTime+$GMT*60*60;
        //echo $newStartTime.'<br>';
//        echo "SELECT 
//    ecd.time,
//    ecd.cli,
//    ecd.abandon_flag,
//    ecd.time_queue_abandon,
//    ecd.time_queue_ans,
//    ecd.talk_time,
//    ecd.callid AS ecid,
//    ecd.agent,
//  
//    (SELECT 
//            COUNT(callid)
//        FROM
//            each_call_details
//        WHERE
//            callid = ecid) AS trans,
//    el.agent_name,
//    ecd.record_path
//FROM
//    each_call_details ecd
//        LEFT JOIN
//    extension_list el ON (ecd.agent LIKE CONCAT('%', el.extension_name))
//WHERE
//    time >= '$newStartTime'
//        AND time <= '$newendTime'
//        AND el.extension_name IN (".$exn_id.")
//        AND qname IN (" . $queue . ");";exit;

//        $qry1 = $this->db->query("SELECT
//                                                ecd.time,ecd.cli,ecd.abandon_flag,ecd.time_queue_abandon,ecd.time_queue_ans,ecd.talk_time,
//                                                ecd.callid as ecid,ecd.agent,
//                                                (SELECT count(callid) FROM each_call_details where callid= ecid) as trans,
//                                                el.agent_name,ecd.record_path
//                                                FROM
//                                                each_call_details ecd
//                                                LEFT JOIN extension_list el ON (ecd.agent like CONCAT('%',el.extension_name))
//                                                WHERE
//                                                time >= '$newStartTime'
//                                                AND time <= '$newendTime'
//                                                AND el.extension_name IN (".$exn_id.")
//                                                  AND qname IN (" . $queue . ");");
        if ($exn_id == '"0"'){
            $qry1 = $this->db->query("SELECT 
    ecd.time,
    ecd.cli,
    ecd.abandon_flag,
    ecd.time_queue_abandon,
    ecd.time_queue_ans,
    ecd.talk_time,
    ecd.callid AS ecid,
    ecd.agent,
    ecd.qname,
    (SELECT 
            COUNT(callid)
        FROM
            each_call_details
        WHERE
            callid = ecid) AS trans,
    el.agent_name,
    ecd.record_path
FROM
    each_call_details ecd
        LEFT JOIN
    extension_list el ON (ecd.agent LIKE CONCAT('%', el.extension_name))
WHERE
    time >= '$newStartTime'
        AND time <= '$newendTime'
        AND qname IN (" . $queue . ")");
        }else{
            $qry1 = $this->db->query("SELECT 
    ecd.time,
    ecd.cli,
    ecd.abandon_flag,
    ecd.time_queue_abandon,
    ecd.time_queue_ans,
    ecd.talk_time,
    ecd.callid AS ecid,
    ecd.agent,
    ecd.qname,
    (SELECT 
            COUNT(callid)
        FROM
            each_call_details
        WHERE
            callid = ecid) AS trans,
    el.agent_name,
    ecd.record_path
FROM
    each_call_details ecd
        LEFT JOIN
    extension_list el ON (ecd.agent LIKE CONCAT('%', el.extension_name))
WHERE
    time >= '$newStartTime'
        AND time <= '$newendTime'
        AND el.extension_name IN (".$exn_id.")
        AND qname IN (" . $queue . ")");
        }
        if ($qry1->num_rows > 0) {

            foreach ($qry1->result() as $row) {
                $date = date('Y-m-d H:i:s', strtotime($row->time));
                $hour = date('h', strtotime($row->time));
                $minutes = date('i', strtotime($row->time));
                $sec = date('s', strtotime($row->time));

                //echo $minutes.'<br>';
                //echo $sec;exit;


                $transfer = (empty($row->trans) ? 1 : $row->trans);
                $msg1.='<tr><td>' . $i . '</td>';
                $msg1.='<td>' . $row->cli . '</td>';
                if ($transfer > 1) {
                    $msg1.='<td><button onclick="getTransferDetails(' . urlencode($row->ecid) . ')"> YES </button></td>';
                } else {
                    $msg1.='<td> NO </td>';
                }
                $msg1.='<td>' . $date . '</td>';
                //$msg1.='<td>'.$hour.'</td>';
                //$msg1.='<td>'.$minutes.'</td>';
                
                if ($row->abandon_flag == 1) {
                    $msg1.='<td>ABANDON</td>';
                    $msg1.='<td>' . $row->time_queue_abandon . '</td>';
                    $msg1.='<td>-</td>';
                } else {
                    $msg1.='<td>ANSWERED</td>';
                    $msg1.='<td>' . $row->time_queue_ans . '</td>';
                    $msg1.='<td>' . $row->talk_time . '</td>';
                }
                if ($row->agent_name == '') {

                    $msg1.='<td>-</td>';
                } else {
                    $msg1.='<td>' . $row->agent_name . '</td>';
                }
                $msg1.='<td>'.$row->qname.'</td>';
                $msg1.= '<td><a target="_blank" class="btn btn-success btn-xs" href="' . RECORD_PATH . "/" . $row->record_path . '"><i class="fa fa-play"></i> Play</a></td>';
                $msg1.= '</tr>';
                $i++;
            }
        }


        
          else {
          $msg1.='<td> - </td>';
          $msg1.='<td> - </td>';
          $msg1.='<td> - </td>';
          $msg1.='<td> - </td>';
          $msg1.='<td> - </td>';
          $msg1.='<td> - </td>';
          $msg1.='<td> - </td>';
          $msg1.='<td> - </td>';
          $msg1.='<td> - </td>';
         



          } 



        $msg1.='</table>';
        return $msg1;
    }

    function GenerateExcelEachCall($startDate, $endDate) {
        $queueName = $this->input->post('inpQueue');
        if ($queueName == '') {
            echo 'Please select queue.';
            exit;
        }
        // print_r($queueName);die;
        $service_level_threshold = 0;
        $short_abandon_threshould = 0;
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $msg1 = '';
        $i = 1;
        $count = 0;


        $k = 0;
        $arr = array();



        $query1 = $this->db->query("Select * from report_configuration");
        foreach ($query1->result() as $query11) {
            $GMT = $query11->GMT_offset;
            $service_level_threshold = $query11->service_level_threshold;
            $short_abandon_threshould = $query11->short_abandon_threshould;
        }


        $newStartTime = date('Y-m-d H:i:s', strtotime($startDate . " 00:00:00"));
        $newendTime = date('Y-m-d H:i:s', strtotime($endDate . " 23:59:59"));

        //$newStartTime=date('Y-m-d H:i:s',strtotime($startTime+$GMT*60*60));
        //$newendTime=date('Y-m-d H:i:s',strtotime($endTime+$GMT*60*60));
        //echo $newStartTime.'<br>';
        //echo $newendTime;exit;
        // print_r($queueName);die;
        if ($queueName == '"0"') {
            $qry = "select * from each_call_details where time >='$newStartTime' and time <='$newendTime' and qname!=''";
        } else {
            $qry = "select * from each_call_details where time >='$newStartTime' and time <='$newendTime' and qname IN (" . $queueName . ")";
        }
        $qry1 = $this->db->query($qry);

        if ($qry1->num_rows() > 0) {

            //$arr['did_count']=$qry1->row()->did_count;

            foreach ($qry1->result() as $row) {

                $date = date('Y-m-d H:i:s', strtotime($row->time));
                $hour = date('h', strtotime($row->time));
                $minutes = date('i', strtotime($row->time));
                $sec = date('s', strtotime($row->time));

                //echo $minutes.'<br>';
                //echo $sec;exit;


                $arr['i'] = $i;

                $arr['cli'] = $row->cli;

                $arr['date'] = $date;
                $arr['hour'] = $hour;
                $arr['minutes'] = $minutes;
                $arr['sec'] = $sec;


                if ($row->abandon_flag == 1) {
                    $arr['Ab_Ans'] = 'ABN';
                    $arr['time_queue'] = $row->time_queue_abandon;
                    $arr['talk_time'] = '-';
                } else {
                    $arr['Ab_Ans'] = 'ANS';
                    $arr['time_queue'] = $row->time_queue_ans;
                    $arr['talk_time'] = $row->talk_time;
                }
                $arr['agent'] = $row->agent;

                $i++;
                $msg1[$k] = $arr;
                $k++;
            }
        } else {
            $arr['i'] = 0;
            $arr['cli'] = '-';
            $arr['date'] = '-';
            $arr['hour'] = '-';
            $arr['minutes'] = '-';
            $arr['sec'] = '-';
            $arr['Ab_Ans'] = '-';
            $arr['time_queue'] = '-';
            $arr['talk_time'] = '-';
            $arr['agent'] = '-';
        }
        return $msg1;
    }

    function intra_day_sl_report($startDate, $interval) {
        $service_level_threshold = 0;
        $short_abandon_threshould = 0;
//	echo $startDate;
        //echo $endDate;
//echo $startDate.$interval;
        date_default_timezone_set('Asia/Kolkata');
        date_default_timezone_set('Asia/Kolkata');
        $startDate = date('Y-m-d', strtotime($startDate));
        $Date1 = $startDate;
        $time_intervals = array();
        $time_intervals = $this->getTimeInInterval($startDate, $interval);
//print_r($time_intervals);exit;
        $number_of_interaval = count($time_intervals);
        $msg1 = '';
        $i = 1;
        $count = 0;




        $query1 = $this->db->query("Select * from report_configuration");
        //$result=$conn->query($query1);
        //GMT=$qry1->row()->GMT_offset;

        foreach ($query1->result() as $query11) {
            $GMT = $query11->GMT_offset;
            $service_level_threshold = $query11->service_level_threshold;
            $short_abandon_threshould = $query11->short_abandon_threshould;
        }

        //echo $datecount;
        //echo $now;
        //echo $now;


        /* 			$startTime=strtotime($startDate."00:00:00");
          $endTime=strtotime($startDate."23:59:59");

          $newStartTime=$startTime;
          $newendTime=$endTime;
          $number_of_interaval=(24*60)/$interval;
          $interval_slot=$interval*60;
          $slot_start_time=$newStartTime; */
        $test = 0;
        $j = 1;
        for ($i = 0; $i < ($number_of_interaval - 1); $i++) {

            $slot_start_time = $Date1 . ' ' . $time_intervals[$i];
            $slot_end_time = $Date1 . ' ' . $time_intervals[$i + 1];
            $msg1.= '<tr>';
            $time_interval = date('H:i', strtotime($slot_start_time));
            $msg1.='<td>' . $j . '</td>';
            $msg1.='<td>' . $time_interval . '</td>';

            //$slot_end_time=$slot_start_time+$interval_slot+$test;
            //echo $slot_end_time.'<br>';
            //exit;  
            //$qry1=$this->db->query("select count(callid) as did_count from each_call_details where time >='$startTime' and time <='$slot_end_time'");
            $qry1 = $this->db->query("select count(callid) as did_count from each_call_details where time >='$slot_start_time' and time <='$slot_end_time'");
            //$result1 = $conn->query($qry1);
            //$row1 = $result1->fetch_assoc();
            if ($qry1->row()->did_count > 0) {
                if ($qry1->num_rows > 0) {

                    $msg1.='<td>' . $qry1->row()->did_count . '</td>';


                    $qry2 = $this->db->query("select count(callid) as complete_count from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action in ('COMPLETECALLER','COMPLETEAGENT') ");

                    //$result2 = $conn->query($qry2);
                    //$row2 = $result2->fetch_assoc();

                    if ($qry2->num_rows > 0) {
                        $msg1.='<td>' . $qry2->row()->complete_count . '</td>';
                    }

                    $qry6 = $this->db->query("select avg(talk_time) as ATT from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action in ('COMPLETECALLER','COMPLETEAGENT') ");

                    //$result6 = $conn->query($qry6);
                    //$row6 = $result6->fetch_assoc();

                    if ($qry6->num_rows > 0) {
                        $msg1.='<td>' . round($qry6->row()->ATT, 2) . '</td>';
                    }






                    $qry3 = $this->db->query("select count(callid) as abandon_count from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action='ABANDON' ");
                    //$result3 = $conn->query($qry3);
                    //$row3 = $result3->fetch_assoc();
                    if ($qry3->num_rows > 0) {
                        //$msg1.='<td>'.$qry3->row()->abandon_count.'</td>';
                    }

                    $qry4 = $this->db->query("select count(callid) as abandon_count_qtime from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action='ABANDON' and time_queue_abandon <='$short_abandon_threshould'");
                    //$result4 = $conn->query($qry4);
                    //$row4 = $result4->fetch_assoc();
                    if ($qry4->num_rows > 0) {
                        //$msg1.='<td>'.$qry4->row()->abandon_count_qtime.'</td>';
                    }

                    $answered_level = $qry2->row()->complete_count / $qry1->row()->did_count;
                    if ($answered_level > 0) {
                        $answered_level = round($answered_level * 100, 2);

                        $msg1.='<td>' . $answered_level . '%</td>';
                    }


                    $qry5 = $this->db->query("select count(callid) as complete_count_qtime from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action<> 'ABANDON' and  time_queue_ans <= '$service_level_threshold'");
                    //$result5 = $conn->query($qry5);
                    //$row5 = $result5->fetch_assoc();
                    if ($qry5->num_rows > 0) {
                        $msg1.='<td>' . $qry5->row()->complete_count_qtime . '</td>';
                    }

                    $answered_Service_level = $qry5->row()->complete_count_qtime / $qry1->row()->did_count;
                    if ($answered_Service_level > 0) {
                        $answered_Service_level = round($answered_Service_level * 100, 2);

                        $msg1.='<td>' . $answered_Service_level . '%</td>';
                    }
                    $diff1 = $qry1->row()->did_count - $qry4->row()->abandon_count_qtime;
                    if ($diff1 <= 0) {
                        $short_abandoned = 0;
                    } else {
                        $short_abandoned = $qry5->row()->complete_count_qtime / ($diff1);
                    }

                    if ($short_abandoned > 0) {

                        $short_abandoned = round($short_abandoned * 100, 2);
                        $msg1.='<td>' . $short_abandoned . '%</td>';
                    }
                }
            } else {
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
                $msg1.='<td> - </td>';
            }
            //	$slot_end_time
            $slot_start_time = $slot_end_time;
            $test = 1;
            $msg1.= '</tr>';
            $j++;
        }
        return $msg1;
    }

    function genrate_cdr_excel($startDate, $endDate, $phone) {
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db2', TRUE);

        if ($phone == '') {
            $query = $this->db2->query("SELECT * FROM cdr where(`dst` = 's' or `dst`=200) and calldate>='$startDate' and calldate<='$endDate'");
        }
        if ($phone != '') {
            $query = $this->db2->query("SELECT * FROM cdr where(`dst` = 's' or `dst`=200) and calldate>='$startDate' and calldate<='$endDate' and src='$phone'");
        }
        //$query=$this->db2->query("SELECT * FROM cdr where(`dst` = 's' or `dst`=200) and calldate>='$startDate' and calldate<='$endDate' and phone='$phone'");
        $j = 0;
        $arr = array();
        foreach ($query->result() as $row) {

            $arr['date_time'][$j] = $row->calldate;
            if ($row->dst == 's') {
                $arr['queue'][$j] = 'Off Time Queue';
            }
            if ($row->dst == '200') {
                $arr['queue'][$j] = 'Office Time Queue';
            }
            $arr['phoneNo'][$j] = $row->src;
            $arr['bill_time'][$j] = $row->billsec;
            $arr['status'][$j] = $row->disposition;
            $j++;
        }
        return $arr;
    }

    function display_records($start, $end, $phone) {
        $service_level_threshold = 0;
        $short_abandon_threshould = 0;
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db2', TRUE);

        $msg = '';

        $sdate = date('Y-m-d', strtotime($start));
        $edate = date('Y-m-d', strtotime($end));

        if ($phone == 0) {
            $query = $this->db2->query("SELECT * FROM cdr where(`dst` = 's' or `dst`=200) and calldate>='$sdate' and calldate<='$edate' ");
        }
        if ($phone != 0) {
            $query = $this->db2->query("SELECT * FROM cdr where(`dst` = 's' or `dst`=200) and calldate>='$sdate' and calldate<='$edate' and src='$phone'");
        }

        $msg.='<table class="table-bordered">';


        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                if ($row->dst == 's') {
                    $queue = 'Off Time Queue';
                }
                if ($row->dst == '200') {
                    $queues = 'Office Time Queue';
                }
                $msg.='<tr><td>' . $row->calldate . '</td><td>' . $row->src . '</td><td>' . $queue . '</td><td>' . $row->billsec . '</td><td>' . $row->disposition . '</td></tr>';
            }
        }//IF
        else {
            $msg.='No Data Found';
        }
        $msg.='</table>';

        return $msg;
    }

    function GenerateExcel($startDate, $endDate) {

        $x = '';
        $queueName = $this->input->post('inpQueue1');
          if ($queueName == '') {
            echo 'Please select queue.';
            exit;
        }
        //print_r($queueName);die;
        $service_level_threshold = 0;
        $short_abandon_threshould = 0;
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $hms_for_day = '00:00:00';
        $hms_for_night = '23:59:59';
        $iDateFrom = $startDate . ' ' . $hms_for_day;
        $iDateTo = $endDate . ' ' . $hms_for_night;
        $msg1 = array();
        $k = 0;
        $i = 1;
        $count = 0;
        if ($queueName == '"0"' || $queueName == '') {
            $qry = $this->db->select('queue')->get('qname');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->queue . '",';
                }
            }
            $x = rtrim($x, ",");
            $queueName = $x;
        } else {
            $queueName;
        }
        //$queueName = '"'.implode('","', $queueName).'"';
        $aryRange = array();
        $aryRange = $this->createDateRangeArray($startDate, $endDate);
        $datecount = count($aryRange);
        //echo $datecount;exit;

        $query1 = $this->db->query("Select * from report_configuration");

        foreach ($query1->result() as $query11) {
            $service_level_threshold = $query11->service_level_threshold;
            $short_abandon_threshould = $query11->short_abandon_threshould;
        }

        for ($j = 0; $j < $datecount; $j++) {

            $now = $aryRange[$j];
//			$msg1.=  '<tr>';
            $arr['now'] = $now;

            $iDateFrom = $now . " 00:00:00";
            $iDateTo = date('Y-m-d', strtotime($now));
            $iDateTo = $iDateTo . " 23:59:59";
//            if ($queueName[0] == 0) {
//                $qry = "select count(callid) as did_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and qname!=''";
//            } else {
            $qry = "select count(callid) as did_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and qname IN (" . $queueName . ")";
            //}
            $qry1 = $this->db->query($qry);
            if ($qry1->row()->did_count > 0) {
                if ($qry1->num_rows() > 0) {

                    $arr['did_count'] = $qry1->row()->did_count;
//                    if ($queueName[0] == 0) {
//                        $qry = "select count(callid) as complete_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action in ('COMPLETECALLER','COMPLETEAGENT') and qname!=''";
//                    } else {
                    $qry = "select count(callid) as complete_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action in ('COMPLETECALLER','COMPLETEAGENT') and qname IN (" . $queueName . ")";
                    // }
                    $qry2 = $this->db->query($qry);

                    if ($qry2->num_rows > 0) {
                        $arr['complete_count'] = $qry2->row()->complete_count;
                    }
//                    if ($queueName[0] == 0) {
//                        $qry6 = $this->db->query("select avg(talk_time) as ATT from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action in ('COMPLETECALLER','COMPLETEAGENT') and qname!=''");
//                    } else {
                    $qry6 = $this->db->query("select avg(talk_time) as ATT from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action in ('COMPLETECALLER','COMPLETEAGENT') and qname IN (" . $queueName . ")");
//                    }
                    if ($qry6->num_rows > 0) {
                        $arr['ATT'] = round($qry6->row()->ATT, 2);
                    }
//                    if ($queueName[0] == 0) {
//                        $qry3 = $this->db->query("select count(callid) as abandon_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action='ABANDON' and qname!=''");
//                    } else {
                    $qry3 = $this->db->query("select count(callid) as abandon_count from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action='ABANDON' and qname IN (" . $queueName . ")");
//                    }

                    if ($qry3->num_rows > 0) {
                        $arr['abandon_count'] = $qry3->row()->abandon_count;
                    }
//                    if ($queueName[0] == 0) {
//                        $qry4 = $this->db->query("select count(callid) as abandon_count_qtime from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action='ABANDON' and time_queue_abandon <='$short_abandon_threshould' and qname!=''");
//                    } else {
                    $qry4 = $this->db->query("select count(callid) as abandon_count_qtime from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action='ABANDON' and time_queue_abandon <='$short_abandon_threshould' and qname IN (" . $queueName . ")");
//                    }
                    if ($qry4->num_rows > 0) {
                        $arr['abandon_count_qtime'] = $qry4->row()->abandon_count_qtime;
                    }

                    $answered_level = $qry2->row()->complete_count / $qry1->row()->did_count;
                    if ($answered_level > 0) {
                        $answered_level = round($answered_level * 100, 2);

                        $arr['answered_level'] = $answered_level;
                    } else {
                        $arr['answered_level'] = '-';
                    }

//                    if ($queueName[0] == 0) {
//                        $qry5 = $this->db->query("select count(callid) as complete_count_qtime from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action<> 'ABANDON' and  time_queue_ans <= '$service_level_threshold' and qname!=''");
//                    } else {
                    $qry5 = $this->db->query("select count(callid) as complete_count_qtime from each_call_details where time >='$iDateFrom' and time <='$iDateTo' and action<> 'ABANDON' and  time_queue_ans <= '$service_level_threshold' and qname IN (" . $queueName . ")");
//                    }

                    if ($qry5->num_rows > 0) {
                        $arr['complete_count_qtime'] = $qry5->row()->complete_count_qtime;
                    }

                    $answered_Service_level = $qry5->row()->complete_count_qtime / $qry1->row()->did_count;
                    if ($answered_Service_level > 0) {
                        $answered_Service_level = round($answered_Service_level * 100, 2);

                        $arr['answered_Service_level'] = $answered_Service_level;
                    } else {
                        $arr['answered_Service_level'] = '-';
                    }
                    $diff1 = ($qry1->row()->did_count - $qry4->row()->abandon_count_qtime);
                    if ($diff1 <= 0) {
                        $short_abandoned = 0;
                    } else {
                        $short_abandoned = $qry5->row()->complete_count_qtime / $diff1;
                    }
                    //$short_abandoned= $qry5->row()->complete_count_qtime/($qry1->row()->did_count-$qry4->row()->abandon_count_qtime);

                    if ($short_abandoned > 0) {

                        $short_abandoned = round($short_abandoned * 100, 2);
                        $arr['short_abandoned'] = $short_abandoned;
                    } else {
                        $arr['short_abandoned'] = '-';
                    }
                }
            } else {
                $arr['now'] = $now;
                $arr['did_count'] = '-';
                $arr['complete_count'] = '-';
                $arr['ATT'] = '-';
                $arr['abandon_count'] = '-';
                $arr['abandon_count_qtime'] = '-';
                $arr['answered_level'] = '-';
                $arr['complete_count_qtime'] = '-';
                $arr['answered_Service_level'] = '-';
                $arr['short_abandoned'] = '-';
            }
            $msg1[$k] = $arr;
            $k++;
        }
        return $msg1;
    }

    function excelintra_day_sl_report($startDate, $interval) {
        $service_level_threshold = 0;
        $short_abandon_threshould = 0;
//	echo $startDate;
        //echo $endDate;
//echo $startDate.$interval;
        date_default_timezone_set('Asia/Kolkata');
        date_default_timezone_set('Asia/Kolkata');
        $startDate = date('Y-m-d', strtotime($startDate));
        $Date1 = $startDate;
        $time_intervals = array();
        $time_intervals = $this->getTimeInInterval($startDate, $interval);
//print_r($time_intervals);exit;
        $number_of_interaval = count($time_intervals);
        $msg = array();
        $i = 1;
        $count = 0;
        $k = 0;

        $query1 = $this->db->query("Select * from report_configuration");
        foreach ($query1->result() as $query11) {
            $GMT = $query11->GMT_offset;
            $service_level_threshold = $query11->service_level_threshold;
            $short_abandon_threshould = $query11->short_abandon_threshould;
        }
        $test = 0;
        for ($i = 0; $i < ($number_of_interaval - 1); $i++) {
            $slot_start_time = $Date1 . ' ' . $time_intervals[$i];
            $slot_end_time = $Date1 . ' ' . $time_intervals[$i + 1];
            $time_interval = date('H:i', strtotime($slot_start_time));

            $arr['time_interval'] = $time_interval;
            $qry = "select count(callid) as did_count from each_call_details where time >='$slot_start_time' and time <='$slot_end_time'";
            $qry1 = $this->db->query($qry);
            if ($qry1->row()->did_count > 0) {
                if ($qry1->num_rows > 0) {

                    $arr['did_count'] = $qry1->row()->did_count;


                    $qry2 = $this->db->query("select count(callid) as complete_count from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action in ('COMPLETECALLER','COMPLETEAGENT') ");

                    if ($qry2->num_rows > 0) {
                        $arr['complete_count'] = $qry2->row()->complete_count;
                    }

                    $qry6 = $this->db->query("select avg(talk_time) as ATT from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action in ('COMPLETECALLER','COMPLETEAGENT') ");

                    if ($qry6->num_rows > 0) {
                        $arr['ATT'] = round($qry6->row()->ATT, 2);
                    }
                    $qry3 = $this->db->query("select count(callid) as abandon_count from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action='ABANDON' ");
                    if ($qry3->num_rows > 0) {
                        //$msg1.='<td>'.$qry3->row()->abandon_count.'</td>';
                    }

                    $qry4 = $this->db->query("select count(callid) as abandon_count_qtime from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action='ABANDON' and time_queue_abandon <='$short_abandon_threshould'");
                    //$result4 = $conn->query($qry4);
                    //$row4 = $result4->fetch_assoc();
                    if ($qry4->num_rows > 0) {
                        //$msg1.='<td>'.$qry4->row()->abandon_count_qtime.'</td>';
                    }

                    $answered_level = $qry2->row()->complete_count / $qry1->row()->did_count;
                    if ($answered_level > 0) {
                        $answered_level = round($answered_level * 100, 2);

                        $arr['answered_level'] = $answered_level;
                    }


                    $qry5 = $this->db->query("select count(callid) as complete_count_qtime from each_call_details where time >='$slot_start_time' and time <='$slot_end_time' and action<> 'ABANDON' and  time_queue_ans <= '$service_level_threshold'");
                    //$result5 = $conn->query($qry5);
                    //$row5 = $result5->fetch_assoc();
                    if ($qry5->num_rows > 0) {
                        $arr['complete_count_qtime'] = $qry5->row()->complete_count_qtime;
                    }

                    $answered_Service_level = $qry5->row()->complete_count_qtime / $qry1->row()->did_count;
                    if ($answered_Service_level > 0) {
                        $answered_Service_level = round($answered_Service_level * 100, 2);

                        $arr['answered_Service_level'] = $answered_Service_level;
                    }
                    $diff1 = $qry1->row()->did_count - $qry4->row()->abandon_count_qtime;
                    if ($diff1 <= 0) {
                        $short_abandoned = 0;
                    } else {
                        $short_abandoned = $qry5->row()->complete_count_qtime / ($diff1);
                    }

                    if ($short_abandoned > 0) {

                        $short_abandoned = round($short_abandoned * 100, 2);
                        $arr['short_abandoned'] = $short_abandoned;
                    }
                }
            } else {
                $arr['time_interval'] = '-';
                $arr['did_count'] = '-';
                $arr['complete_count'] = '-';
                $arr['ATT'] = '-';
                $arr['answered_level'] = '-';
                $arr['complete_count_qtime'] = '-';
                $arr['answered_Service_level'] = '-';
                $arr['short_abandoned'] = '-';
            }
            $msg[$k] = $arr;
            $k++;
        }
        return $msg;
    }

//----------------------Additional functions---------------------------------------
    function createDateRangeArray($strDateFrom, $strDateTo) {

        $aryRange = array();

        $iDateFrom = mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    function getTimeInInterval($selDate, $interval) {
        $arrInterval = array();
        date_default_timezone_set('Asia/Kolkata');
        $selDate = date('Y-m-d', strtotime($selDate));
        $startTime = strtotime($selDate . "00:00:00");
        $endTime = strtotime($selDate . "23:59:59");

        $newStartTime = $startTime;
        $newendTime = $endTime;
        $number_of_interaval = (24 * 60) / $interval;
        $interval_slot = $interval * 60;
        $slot_start_time = $newStartTime;
        for ($i = 1; $i <= $number_of_interaval; $i++) {
            $time_interval = date('H:i', $slot_start_time);
            array_push($arrInterval, $time_interval . ':00');
            $slot_end_time = $slot_start_time + $interval_slot;
            $slot_start_time = $slot_end_time;
        }
        return $arrInterval;
    }

//function ends    
    #---------Date: 1/12/2016  --- Mangesh kamble ---------------------------

    function saveReportConfig($arr) {
        $this->db->update('report_configuration', $arr);
        return 1;
    }

    function getReportsConfig() {
        $result = array();
        $qry = $this->db->select('*')->get('report_configuration');
        $obj = new stdClass();
        $obj->gmt_offset = (empty($qry->row()->GMT_offset) ? 0 : $qry->row()->GMT_offset);
        $obj->slt = (empty($qry->row()->service_level_threshold) ? 0 : $qry->row()->service_level_threshold);
        $obj->sat = (empty($qry->row()->short_abandon_threshould) ? 0 : $qry->row()->short_abandon_threshould);
        $obj->rr = (empty($qry->row()->refresh_rate) ? 0 : $qry->row()->refresh_rate);
        $obj->did_no = (empty($qry->row()->did_no) ? 0 : $qry->row()->did_no);
        $obj->off_time_start = (empty($qry->row()->off_time_start) ? '18:00:00' : $qry->row()->off_time_start);
        $obj->off_time_end = (empty($qry->row()->off_time_end) ? '09:30:00' : $qry->row()->off_time_end);
        array_push($result, $obj);
        $obj = null;
        return json_encode($result);
    }

    function save_addUser() {
        $arr = $this->input->post('rpt');
        //print_r($arr);exit;
        $queue = str_replace('"', "", $arr['queue']);
        $obteam = str_replace('"', "", $arr['obteam']);
        //echo $queue;exit;
        $arr_new = array(
            'agent_name' => $arr['name'],
            'extension_name' => $arr['extn'],
            'iqueue_name' => $queue,
            'ob_team' => $obteam
        );
        $this->db->insert('extension_list', $arr_new);
        $arr = $arr_new = null;
        return 1;
    }
    

    function getUserList() {
        $msg = '';
        $qry = $this->db->query('SELECT IFNULL(agent_name,"Unspecified") AS agent_name,IFNULL(iqueue_name,"Unallocated") AS iqueue_name,IFNULL(extension_name,"No Extension") AS extension_name,IFNULL(ob_team,"Unallocated") AS ob_team FROM extension_list');
        if ($qry->num_rows() > 0) {
            $i = 1;
            foreach ($qry->result() as $row) {
                $msg.='<tr>';
                $msg.='<td><input type="text" id="agent_name' . $i . '" name="agent_name' . $i . '" value="' . $row->agent_name . '" readonly></td>';
                $msg.='<td><input type="text" id="iqueue_name' . $i . '" name="iqueue_name' . $i . '" value="' . $row->iqueue_name . '" readonly></td>';
                $msg.='<td><input type="text" id="extension_name' . $i . '" name="extension_name' . $i . '" value="' . $row->extension_name . '" readonly></td>';
                $msg.='<td><input type="text" id="ob_team' . $i . '" name="ob_team' . $i . '" value="' . $row->ob_team . '" readonly></td>';
                $msg.='<td><i id="edit' . $i . '" class="fa fa-edit" onclick="edit_user(' . $i . ');"></i><i id="save' . $i . '" style="display:none;" class="fa fa-save" onclick="save_user(' . $i . ');"></i></td></tr>';
                $i++;
            }
            return $msg;
        }
    }

    function save_user($arr) {
        $this->db->set('iqueue_name', $arr['iqueue_name']);
        $this->db->set('agent_name', $arr['agent']);
        $this->db->set('ob_team', $arr['ob_team']);
        $this->db->where('extension_name', $arr['extension_name']);
        $this->db->where('extension_name', $arr['extension_name'])->update('extension_list');
        return 1;
    }

    function getTransferDetails() {
        $msg='';
        $callid = $this->input->post('callid');
        $qry = $this->db->query("SELECT ecd.time,ecd.cli,ecd.qname,ecd.talk_time,CONCAT(ecd.agent,'(',el.agent_name,')') as agent FROM each_call_details ecd
        LEFT JOIN extension_list el ON (ecd.agent like CONCAT('%',el.extension_name))
         where callid='" . $callid . "' order by time asc");
        if ($qry->num_rows() > 0) {
            $msg.='<table class="table table-bordered">
                    <thead>
                        <th>Time</th>
                        <th>Phone Number</th>
                        <th>Queue</th>
                        <th>Talk Time</th>
                        <th>Agent</th>
                    </thead><tbody>';
            foreach ($qry->result() as $row) {
            $msg.='<tr>';
            $msg.='<td>'.$row->time.'</td>';
            $msg.='<td>'.$row->cli.'</td>';
            $msg.='<td>'.$row->qname.'</td>';
            $msg.='<td>'.$row->talk_time.'</td>';
            $msg.='<td>'.$row->agent.'</td>';
            $msg.='</tr>';
            }
            $msg.='</table>';
            return $msg;
        }
    }
    function agentWiseOBreport($arr){
        $x='';
        $count=0;
        $result1=array();
        $tot_billsec=0;
        $oblist=  urldecode($arr['oblist']);
        $select_extn_list= urldecode($arr['select_extn_list']);
        $inpstdatetime=  urldecode($arr['inpstdate']);
        $inpstdate= date('Y-m-d',  strtotime($inpstdatetime));
        $startTime=date("H:i:s",strtotime($inpstdatetime));
        $inpEnddatetime =  urldecode($arr['inpEnddate']);
        $inpEnddate= date('Y-m-d',  strtotime($inpEnddatetime));
        $endTime=date("H:i:s",strtotime($inpEnddatetime));
        $oblist=urldecode($arr['oblist']);
                
        if($select_extn_list!='x'){
            //echo 'extn selected';
            if ($select_extn_list == '"0"') {
                $qry = $this->db->query('select DISTINCT(extension_name) as extension_name from extension_list where extension_name IS NOT NULL');
                if ($qry->num_rows() > 0) {
                    foreach ($qry->result() as $row) {
                        $x.='"' . $row->extension_name . '",';
                    }
                }
                $x = rtrim($x, ",");
                $select_extn_list = $x;
            } 
        }elseif($oblist!='x'){
            //echo 'OB team selected';
            if ($oblist == '"0"') {
                $qry = $this->db->query('select DISTINCT(ob_team) as ob_team from extension_list where ob_team IS NOT NULL');
                if ($qry->num_rows() > 0) {
                    foreach ($qry->result() as $row) {
                        $x.='"' . $row->ob_team . '",';
                    }
                }
                $x = rtrim($x, ",");
                $oblist = $x;
            }   
        }
        $dateArray=$this->outbound_report_cdr_model->dateRange($inpstdate,$inpEnddate);
        if(count($dateArray)==1){
        $count=count($dateArray)+1;
        }else{
        $count=count($dateArray);  
        }
         for($i=0;$i<($count-1);$i++){
             if(count($dateArray)==1){
                $sd=$dateArray[$i].' '.$startTime;
                $ed=$dateArray[$i].' '.$endTime;
             }else{
                $sd=$dateArray[$i].' '.$startTime;
                $ed=$dateArray[$i+1].' '.$endTime;
             }
            // echo '<br>'.$sd.'-->'.$ed;
            if($select_extn_list!='x'){
            $query="SELECT DISTINCT(c.src) src,el.agent_name FROM cdr c 
                    INNER JOIN extension_list el ON (el.extension_name=c.src)
                    where c.calldate>='$sd' 
                    AND c.calldate<='$ed' 
                    AND c.lastapp='Dial' AND c.src IN ($select_extn_list)";
            }else{
                $query="SELECT DISTINCT(c.src) src,el.agent_name FROM cdr c 
                    INNER JOIN extension_list el ON (el.extension_name=c.src)
                    where c.calldate>='$sd' 
                    AND c.calldate<='$ed' 
                    AND c.lastapp='Dial' AND el.ob_team IN ($oblist)";
            }
            $qry1=$this->db->query($query);
            if($qry1->num_rows()>0){
            foreach($qry1->result() as $row){

                    $obj=new StdClass(); 
                    $obj->date=$dateArray[$i];
                    $obj->agent_name=$row->agent_name;
                    $obj->extn=$row->src;
                $query2="SELECT 
                    SUM(case when src IN ('$row->src') then 1 else 0 end) as no_of_dials,
                    SUM(case when src=('$row->src') AND disposition='ANSWERED' then 1 else 0 end) as calls_connected,
                    SUM(case when src IN ('$row->src') then billsec else 0 end) as tot_billsec 
                     FROM cdr c 
                    where c.calldate>='$sd' 
                    AND c.calldate<='$ed' 
                    AND c.lastapp='Dial'"; 
                
                $qry2=$this->db->query($query2);
                    $no_of_dials=(empty($qry2->row()->no_of_dials)?0:$qry2->row()->no_of_dials);
                    $obj->no_of_dials=$no_of_dials;
                    $calls_connected=(empty($qry2->row()->calls_connected)?0:$qry2->row()->calls_connected);
                    $obj->calls_connected=$calls_connected;
                    $conn=(($calls_connected/$no_of_dials)*100);
                    $obj->conn_per=round($conn,2);
                    $tot_billsec=(empty($qry2->row()->tot_billsec)?0:$qry2->row()->tot_billsec);
                    $obj->tot_duration= gmdate("H:i:s", $tot_billsec);
                    if($calls_connected==0){
                     $obj-> avg_duration=0;  
                    }else{
                     $obj-> avg_duration= gmdate("H:i:s", round($tot_billsec/$calls_connected,0));
                    }
                    array_push($result1, $obj);
            }
        }
    }   
        return json_encode($result1);
   }

   function bridge_login_check($arr) {
    $u_name = $arr['u_name']; 
    $u_pass = $arr['u_pass'];
    $qry = "SELECT * FROM users WHERE user_name = '$u_name' AND password='$u_pass' AND status='1'";
    $query = $this->db->query($qry);
    if($query->num_rows()>0)      
    {
        return 'true';
    }
    else{
        return 'false';
    }
    }
    
   function get_did_number(){
    $qry = $this->db->query("Select * from conference");
    $msg = '';
    $msg.='<option value=""> Select DID Number </option>';
    foreach ($qry->result() as $row) {
        $msg.='<option value=' . $row->ConfNum . '>' . $row->ConfNum . '</option>';
    }   
    return $msg;
    }    
    
   function bridge_reset_pass($arr){
    $u_did_no = $arr['u_did_no'];
    $u_new_pass = $arr['u_new_pass'];
    $qry = "UPDATE conference SET Password='$u_new_pass' WHERE ConfNum='$u_did_no'";
    if ($query = $this->db->query($qry)){
        return 1;
    } else {
        return 0;
    }
    }
     function get_extensionListforinbound() 
    {
        //This function fetches distinct queue name from extensio_list table
        //Displays the result in dropdown
        
        $msg='';
        
        $sql = "SELECT extension_name,agent_name FROM extension_list WHERE extension_name IS NOT NULL";
        $query = $this->db->query($sql);
        
        $msg.='<option value="0">All</option>';
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $msg.='<option value="'.$row->extension_name.'">'.$row->extension_name.'('.$row->agent_name.')</option>';
            }
            $msg.='<option value="all">All</option>';
        }
        
        return $msg;
    }
    
    /*Added by Rajnandini*/
    
    function save_addDid() {
        $arr = $this->input->post('rpt');
        $arr_new = array(
            'conf_name' => $arr['conf_name'],
            'did_no' => $arr['did']
            
        );
        $this->db->insert('conf_did', $arr_new);
        $arr = $arr_new = null;
        return 1;
    }
    
    function save_addContact() {
        $arr = $this->input->post('rpt');
        print_r($arr);
        $arr_new = array(
            'confid' => $arr['didno_list'],
            'contact_no' => $arr['contact_no']
            
        );
        $this->db->insert('conf_members', $arr_new);
        $arr = $arr_new = null;
        return 1;
    }
    
    function get_didno_list() {
        $qlist = array();
        $sql = "SELECT * FROM conf_did";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $obj = new stdClass();
                $obj->confid=$row->confid;
                $obj->conf_name=$row->conf_name;
                array_push($qlist, $obj);
            }
            return json_encode($qlist);
        }
    }
    
    function get_didcontact_List($conf_id) {
        $qlist = array();
        $sql = "SELECT * FROM conf_members where confid=$conf_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $obj = new stdClass();
                $obj->id=$row->id;
                $obj->contact_no=$row->contact_no;
                array_push($qlist, $obj);
            }
            return json_encode($qlist);
        }
    }
    
    function getDidNumber($did_id) {
        $qlist = array();
        $sql = "SELECT * FROM conf_did where confid=$did_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $obj = new stdClass();
                $obj->did_no=$row->did_no;
                array_push($qlist, $obj);
            }
            return json_encode($qlist);
        }
    }
    
    function deleteContact($id)
    {
        $this->db->where('id', $id)->delete('conf_members'); 
        return;
    }
    
    /*end here*/
   
} // end of class
