<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

Class Bridge_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function bridge_login_check($arr) {
        $u_name = $arr['u_name']; 
        $u_pass = $arr['u_pass'];
        $qry = "SELECT * FROM users WHERE user_name = '$u_name' AND password='$u_pass' AND status='1'";
        $query = $this->db->query($qry);
            if($query->num_rows()>0)      
            {
                return true;
            }
            else{
                return false;
            }
    }

    function get_did_number(){
        $qry = $this->db->query("Select * from conf_did");
        $msg = '';
        $msg.='<option value=""> -- Choose -- </option>';
        foreach ($qry->result() as $row) {
            $msg.='<option value=' . $row->confid . '>' . $row->conf_name . '</option>';
        }   
        return $msg;
    }
    
    function get_did_number_forReset(){
        $qry = $this->db->query("Select * from conference");
        $msg = '';
        $msg.='<option value=""> -- Choose -- </option>';
        foreach ($qry->result() as $row) {
            $msg.='<option value=' . $row->ConfNum . '>' . $row->ConfNum . '</option>';
        }   
        return $msg;
    }
    
    function bridge_reset_pass($arr){
        $u_did_no = $arr['u_did_no'];
        $u_new_pass = $arr['u_new_pass'];
        $qry = "UPDATE conference SET Password='$u_new_pass' WHERE ConfNum='$u_did_no'";
        if ($query = $this->db->query($qry)){
            return true;
        } else {
            return false;
        }
    }
    
    function save_addDid() {
        $arr = $this->input->post('rpt');
        $arr_new = array(
            'conf_name' => $arr['conf_name'],
            'did_no' => $arr['did']
        );
        $this->db->insert('conf_did', $arr_new);
        $arr = $arr_new = null;
        return 1;
    }
    
    function save_addContact() {
        $arr = $this->input->post('rpt');
        $arr_new = array(
            'confid' => $arr['didno_list'],
            'contact_no' => $arr['contact_no']
        );
        $this->db->insert('conf_members', $arr_new);
        $arr = $arr_new = null;
        return 1;
    }
    
    function get_didcontact_List($conf_id) {
        $res = array();
        $sql = "SELECT * FROM conf_members where confid=$conf_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $obj = new stdClass();
                $obj->id=$row->id;
                $obj->contact_no=$row->contact_no;
                array_push($res, $obj);
            }
        }
        return json_encode($res);
    }
    
    function getDidNumber($did_id) {
        $qlist = array();
        $sql = "SELECT * FROM conf_did where confid=$did_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $obj = new stdClass();
                $obj->did_no=$row->did_no;
                array_push($qlist, $obj);
            }
            return json_encode($qlist);
        }
    }
} 