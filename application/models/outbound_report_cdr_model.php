<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Outbound_report_cdr_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_extension_list_new() {
        $qry = $this->db->select('extension_name')->get('extension_list');
        $msg = '<select class="form-control" id="select_extn_list" name="select_extn_list">';
        $msg.='<option value="0">All</option>';
        foreach ($qry->result() as $row) {
            $msg.='<option value="' . $row->extension_name . '">' . $row->extension_name . '</option>';
        }
        $msg.='</select>';
        return $msg;
    }

    function get_extension_list() {
        $qlist = array();
        $qry = $this->db->query("SELECT 
                        extension_name, IFNULL(agent_name,'unspecified') as agent_name
                    FROM
                        extension_list
                    WHERE
                        extension_name IS NOT NULL");
        if ($qry->num_rows() > 0) {
            foreach ($qry->result() as $row) {
                $obj = new stdClass();
                $obj->extension=$row->extension_name;
                $obj->extension_name = $row->extension_name.' ('.$row->agent_name.')';
                array_push($qlist, $obj);
            }
            return json_encode($qlist);
        }
    }
    
    function get_outbound_cdr_report(){
        $x = '';
        $y = '';
        $exn_id = $this->input->post('exn_id');
        $ob_team = $this->input->post('ob_team');
        
        if($exn_id=='null')
        {
            if($ob_team[0]!='')
            {
               if ($ob_team[0] != '"0"') {
                    $ob_team = ''.implode(',', $ob_team).'';
                    $Qry = "SELECT extension_name FROM extension_list WHERE ob_team IN ($ob_team)"; 
                }else{
                    $Qry = "SELECT extension_name FROM extension_list WHERE ob_team != 'NULL' and ob_team != ''"; 
                }
                $res=$this->db->query($Qry);
                if ($res->num_rows() > 0) {
                    foreach ($res->result() as $row) {
                        $y.='"' . $row->extension_name . '",';
                    }
                }
                $y = rtrim($y, ",");
                $exn_id = $y;
            } 
        }
        
        
        if($exn_id!='')
        {
            $exn_id = urldecode($exn_id);
            if ($exn_id == '"0"') {
                $qry = $this->db->select('extension_name')->get('extension_list');
                if ($qry->num_rows() > 0) {
                    foreach ($qry->result() as $row) {
                        $x.='"' . $row->extension_name . '",';
                    }
                }
                $x = rtrim($x, ",");
                $exn_id = $x;
            } else {
                $exn_id;
            }
        }
        $startDate = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $ph_no = $this->input->post("ph_no");
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = date("Y-m-d", strtotime($end_date . ' + 1 day'));
        $where = "calldate >='$startDate' AND calldate <='$endDate' ";
        if ($ph_no != '' AND ! is_null($ph_no)) {
            $where.=" AND dst='$ph_no'";
        }
        $where.=" AND src IN (" . $exn_id . ")";
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        $msg1 = '';
        $i = 1;
        $count = 0;
        $sQuery = "SELECT c.calldate, c.src AS extension, c.dst AS phone_no, c.duration,c.billsec,c.disposition,c.recordingfile FROM cdr c
        WHERE $where ";
        $qry1 = $this->db->query($sQuery);
        if ($qry1->num_rows > 0) {
                        $msg1.= '<tr>';
            foreach ($qry1->result() as $row) {

                $msg1.='<td>' . $i . '</td>';
                $msg1.='<td>' . $row->calldate . '</td>';
                $msg1.='<td>' . $row->extension . '</td>';
                $msg1.='<td>' . $row->phone_no . '</td>';
                $msg1.='<td>' . gmdate('H:i:s', $row->duration) . '</td>';
                $msg1.='<td>' . $row->billsec . '</td>';
                $msg1.='<td>' . $row->disposition . '</td>';

                $msg1.= '<td><a target="_blank" class="btn btn-success btn-xs" href="' . RECORD_PATH . "/" . $row->recordingfile . '"><i class="fa fa-play"></i> Play</a></td>';
                $msg1.= '</tr>';
                $i++;
            }
                    $msg1.='</table>';
        }
        return $msg1;
    }

    function get_outbound_cdr_report_old() {

        $x = '';
        $exn_id = $this->input->post('exn_id');
        $exn_id = urldecode($exn_id);
        if ($exn_id == '"0"') {
            $qry = $this->db->select('extension_name')->get('extension_list');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->extension_name . '",';
                }
            }
            $x = rtrim($x, ",");
            $exn_id = $x;
        } else {
            $exn_id;
        }
        $where = '';

        $columns = array('calldate', 'src', 'dst', 'duration', 'billsec', 'disposition', 'recordingfile');
        $columns = array(
            // datatable column index  => database column name
            0 => 'calldate',
            1 => 'src',
            2 => 'dst',
            3 => 'duration',
            4 => 'billsec',
            5 => 'disposition',
            6 => 'recordingfile'
        );
        /*
         * Ordering
         */
        $orderArr = $this->input->get_post('order', true);
        $col_name = $orderArr[0]["column"];
        $col_dir = $orderArr[0]["dir"];

        $startDate = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $exn_id = $this->input->post("exn_id");
        $ph_no = $this->input->post("ph_no");
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = date("Y-m-d", strtotime($end_date . ' + 1 day'));

        //No change -----------------
        $sEcho = $this->input->get_post('draw', true);
        //===========================

        $order_str = null;
        if (count($orderArr) > 0) {
            $order_str.=" ORDER BY " . $columns[$col_name] . "   " . $col_dir;
        }

        //Search by query append
        //Search ===================
        $searchArr = $this->input->get_post('search', true);
        $searchFlag = $searchArr['value'];
        $search = null;
        if ($searchFlag != '' AND ! is_null($searchFlag)) {
            $search = " AND (calldate LIKE '" . $searchArr["value"] . "%' "
                    . " OR src LIKE '" . $searchArr["value"] . "%' "
                    . " OR dst LIKE '" . $searchArr["value"] . "%' "
                    . " OR disposition LIKE '" . $searchArr["value"] . "%')";
        }

        /* Total data set length */
        $where = "calldate >='$startDate' AND calldate <='$endDate' ";
        if ($ph_no != '' AND ! is_null($ph_no)) {
            $where.=" AND dst='$ph_no'";
        }
        if ($exn_id != '' AND ! is_null($exn_id) AND $exn_id != 0 AND $exn_id != '0') {
            $where.=" AND src IN (" . $exn_id . ")";
        }
        $sQuery = "SELECT COUNT(c.calldate) AS rowCount FROM cdr c
        WHERE $where $search $order_str";


        // Order by query append 
        //No change -----------------
        $q2 = $sQuery;
        $rResultTotal = $this->db->query($sQuery)or die(mysqli_error());


        $aResultTotal = $rResultTotal->row();
        $iTotal = $aResultTotal->rowCount;

        /*
         * Paging
         */
        $sLimit = "";
        $iDisplayStart = $this->input->get_post('start', true);
        $iDisplayLength = $this->input->get_post('length', true);

        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart) . ", " . intval($iDisplayLength);
        }

        //===========================
        /* Data set length after filtering */

        $sQuery = "SELECT c.calldate, c.src AS extension, c.dst AS phone_no, c.duration,c.billsec,c.disposition,c.recordingfile FROM cdr c
        WHERE $where $search $order_str $sLimit ";

        $rResult = $this->db->query($sQuery)or die("error" . mysqli_error());
        //no change -----------------------------
        $iFilteredTotal = $rResult->num_rows();

        $output = array(
            "draw" => intval($sEcho),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iTotal,
            "data" => array(),
            "resp" => $q2
        );
        //========================================
        //$output['data'][]=$rResult->result_array();
        foreach ($rResult->result_array() as $Row) {
            $row = array();
            $row[] = date("d/m/Y", strtotime($Row['calldate']));
            $row[] = $Row['extension'];
            $row[] = $Row['phone_no'];
            //$row[]=$Row['duration'];
            $row[] = gmdate('H:i:s', $Row['duration']);
            $row[] = $Row['billsec'];
            $row[] = $Row['disposition'];
            if (!is_null($Row["recordingfile"])AND $Row["recordingfile"] != '') {
                $row[] = '<a target="_blank" class="btn btn-success btn-xs" href="' . RECORD_PATH . "/" . $Row["recordingfile"] . '"><i class="fa fa-play"></i> Play</a>';
            } else {
                $row[] = '-';
            }
            $output['data'][] = $row;
        }

        echo json_encode($output);
        //return $output;
    }

    function get_outbound_cdr_report_proce() {
        $where = '';

        $columns = array('calldate', 'src', 'dst', 'duration', 'billsec', 'disposition', 'recordingfile');
        $columns = array(
            // datatable column index  => database column name
            0 => 'calldate',
            1 => 'src',
            2 => 'dst',
            3 => 'duration',
            4 => 'billsec',
            5 => 'disposition',
            6 => 'recordingfile'
        );
        /*
         * Ordering
         */
        $orderArr = $this->input->get_post('order', true);
        $col_name = $orderArr[0]["column"];
        $col_dir = $orderArr[0]["dir"];

        $startDate = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $exn_id = $this->input->post("exn_id");
        $ph_no = $this->input->post("ph_no");
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = date("Y-m-d", strtotime($end_date . ' + 1 day'));

        //No change -----------------
        $sEcho = $this->input->get_post('draw', true);
        //===========================
        //$startDate = date("Y-m-d", strtotime($startDate));
        //$endDate = date("Y-m-d", strtotime($endDate . ' + 1 day'));			


        /* Total data set length */
        $where = "calldate >='$startDate' AND calldate <='$endDate' ";
        if ($ph_no != '' AND ! is_null($ph_no)) {
            $where.=" AND dst='$ph_no'";
        }
        if ($exn_id != '' AND ! is_null($exn_id) AND $exn_id != 0 AND $exn_id != '0') {
            $where.=" AND src='$exn_id'";
        }
        $sQuery = 'CALL proc_cdr_report("' . $where . '","COUNT");';

        // Order by query append 
        $order_str = null;
        if (count($orderArr) > 0) {
            $order_str.=" ORDER BY " . $columns[$col_name] . "   " . $col_dir;
        }

        //Search by query append
        //Search ===================
        $searchArr = $this->input->get_post('search', true);
        $search = null;
        if (count($searchArr) > 0) {
            $search = " AND (ec.calldate LIKE '" . $searchArr["value"] . "%' "
                    . " OR ec.src LIKE '" . $searchArr["value"] . "%' "
                    . " OR us1.dst LIKE '" . $searchArr["value"] . "%' "
                    . " OR us2.disposition LIKE '" . $searchArr["value"] . "%')";
        }

        //No change -----------------
        $q2 = $sQuery;
        $rResultTotal = $this->db->query($sQuery)or die(mysqli_error());

        while (mysqli_next_result($this->db->conn_id)) {
            if ($result = mysqli_store_result($this->db->conn_id)) {
                mysqli_free_result($result);
            }
        }

        $aResultTotal = $rResultTotal->row();
        $iTotal = $aResultTotal->rowCount;

        /*
         * Paging
         */
        $sLimit = "";
        $iDisplayStart = $this->input->get_post('start', true);
        $iDisplayLength = $this->input->get_post('length', true);

        if (isset($iDisplayStart) && $iDisplayLength != '-1') {
            $sLimit = "LIMIT " . intval($iDisplayStart) . ", " . intval($iDisplayLength);
        }
        $where .= $sLimit;
        //===========================
        /* Data set length after filtering */

        $sQuery = 'CALL proc_cdr_report("' . $where . '","LIMIT")';

        $rResult = $this->db->query($sQuery)or die("error" . mysqli_error());
        while (mysqli_next_result($this->db->conn_id)) {
            if ($result = mysqli_store_result($this->db->conn_id)) {
                mysqli_free_result($result);
            }
        }
        //no change -----------------------------
        $iFilteredTotal = $rResult->num_rows();

        $output = array(
            "draw" => intval($sEcho),
            "recordsTotal" => $iTotal,
            "recordsFiltered" => $iTotal,
            "data" => array(),
            "resp" => $q2
        );
        //========================================
        //$output['data'][]=$rResult->result_array();
        foreach ($rResult->result_array() as $Row) {
            $row = array();
            $row[] = date("d/m/Y", strtotime($Row['calldate']));
            $row[] = $Row['extension'];
            $row[] = $Row['phone_no'];
            $row[] = $Row['duration'];
            $row[] = $Row['billsec'];
            $row[] = $Row['disposition'];
            if (!is_null($Row["recordingfile"])AND $Row["recordingfile"] != '') {
                $row[] = '<a target="_blank" class="btn btn-success btn-xs" href="' . RECORD_PATH . "/" . $Row["recordingfile"] . '">Play</a>';
            } else {
                $row[] = '-';
            }
            $output['data'][] = $row;
        }

        echo json_encode($output);
        //return $output;
    }

    function get_outbound_Eachcall_report() {
        $startDate = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $ph_no = $this->input->post("ph_no");
        $startDate = date("Y-m-d", strtotime($startDate));
        $endDate = date("Y-m-d", strtotime($end_date . ' + 1 day'));
        $where = "calldate >='$startDate' AND calldate <='$endDate'";
        if ($ph_no != '' AND ! is_null($ph_no)) {
            $where.=" AND dst='$ph_no'";
        }
        $msg1 = '';
        $i = 1;
        $sQuery = "SELECT c.calldate, c.dst AS phone_no, c.duration,c.billsec,c.disposition,c.uniqueid,ql.agent FROM cdr c
                LEFT JOIN queue_log ql ON c.uniqueid=ql.callid
                WHERE ql.event!='CONFIGRELOAD' AND $where ";
        $qry1 = $this->db->query($sQuery);
        $msg1.= '<tr>';
        if ($qry1->num_rows > 0) {

            foreach ($qry1->result() as $row) {

                $msg1.='<td>' . $i . '</td>';
                $msg1.='<td>' . $row->phone_no . '</td>';
                $msg1.='<td>' . date("d/m/Y", strtotime($row->calldate)) . '</td>';
                $msg1.='<td>' . gmdate('H:i:s', $row->duration) . '</td>';
                $msg1.='<td>' . $row->disposition . '</td>';
                $msg1.='<td>' . '-' . '</td>';
                $msg1.='<td>' . '-' . '</td>';
                $msg1.='<td>' . $row->agent . '</td>';
                $msg1.= '</tr>';
                $i++;
            }
        }
        $msg1.='</table>';
        return $msg1;
    }

    /* Added by Rajnandini Lokhande on 1/12/2016 */

    function get_queue_list() {
        $qry = $this->db->select('queue')->get('qname');
        $msg = '<select  multiple id="select_queue_list" name="select_queue_list" class="form-control" onclick="getData();" style="padding:0px;width:170px; height: 65px;">';
        $msg.='<option value="0">All</option>';
        foreach ($qry->result() as $row) {
            $msg.='<option value=\'"' . $row->queue . '"\'>' . $row->queue . '</option>';
        }
        $msg.='</select>';
        return $msg;
    }
    
    function get_queue_list1() {
        $qry = $this->db->select('queue')->get('qname');
        $msg = '<select  multiple id="select_queue_list" name="select_queue_list" class="form-control" onclick="getData();" style="padding:0px;width:170px; height: 65px;">';
        $msg.='<option value="--Select--">--Select--</option>';
        foreach ($qry->result() as $row) {
            $msg.='<option value=\'"' . $row->queue . '"\'>' . $row->queue . '</option>';
        }
        $msg.='</select>';
        return $msg;
    }

    function generateQueueExcel($startDate, $endDate) {
        $offStartTime = '';
        $offEndTime = '';
        $did_no=0;
        $i=0;$k=1;$l=0;
        $msg1='';
        $dates=array();
        $sql = $this->db->select('off_time_start,off_time_end,did_no')->get('report_configuration');
        if ($sql->num_rows() > 0) {
            $offStartTime = (empty($sql->row()->off_time_start) ? '18:00:00' : $sql->row()->off_time_start);
            $offEndTime = (empty($sql->row()->off_time_end) ? '18:00:00' : $sql->row()->off_time_end);
            $did_no =(empty($sql->row()->did_no) ? '18:00:00' : $sql->row()->did_no);
        } else {
            echo 'You need to configure your After office hour start time and end time first.';
            exit;
        }
        if($did_no==0){
            echo 'You need to configure your DID number to download.';
            exit;
        }
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        //echo 'st:' . $startDate . '  | en:' . $endDate;
        $dates= $this->dateRange( $startDate,$endDate);  
        //echo count($dates);exit;
        for($i=0;$i<(count($dates)-1);$i++){
            $sd=$dates[$i].' '.$offStartTime;
            $ed=$dates[$i+1].' '.$offEndTime;
            $arr = array();
        $qry = "select calldate,src,dst,duration,billsec,disposition,recordingfile,ivrlevel1,ivrlevel2,ivrlevel3,ivrlevel4 from cdr where calldate >='$sd' and calldate <='$ed'and dst='$did_no'";
        $qry1 = $this->db->query($qry);
        if ($qry1->num_rows() > 0) {
            foreach ($qry1->result() as $row) {
                $arr['i'] = $k;
                $arr['calldate'] = (empty($row->calldate) ? '-' : $row->calldate);
                $arr['src'] = (empty($row->src) ? '-' : $row->src);
                $arr['dst'] = (empty($row->dst) ? '-' : $row->dst);
                $arr['duration'] = (empty($row->duration) ? '-' : $row->duration);
                $arr['billsec'] = (empty($row->billsec) ? '-' : $row->billsec);
                $arr['disposition'] = (empty($row->disposition) ? '-' : $row->disposition);
                $arr['recordingfile'] = (empty($row->recordingfile) ? '-' : $row->recordingfile);
                $arr['ivrlevel1'] = (empty($row->ivrlevel1) ? '-' : $row->ivrlevel1);
                $arr['ivrlevel2'] = (empty($row->ivrlevel2) ? '-' : $row->ivrlevel2);
                $arr['ivrlevel3'] = (empty($row->ivrlevel3) ? '-' : $row->ivrlevel3);
                $arr['ivrlevel4'] = (empty($row->ivrlevel4) ? '-' : $row->ivrlevel4);
                $k++;
                $msg1[$l] = $arr;
                $l++;
            }
        }else{
                $arr['i'] = 0;  
                $arr['calldate'] = '-';
                $arr['src'] ='-';
                $arr['dst'] = '-';
                $arr['duration'] = '-';
                $arr['billsec'] = '-';
                $arr['disposition'] = '-';
                $arr['recordingfile'] = '-';
                $arr['ivrlevel1'] = '-';
                $arr['ivrlevel2'] = '-';
                $arr['ivrlevel3'] = '-';
                $arr['ivrlevel4'] = '-';
                $msg1[0] = $arr;
        } 
     
        }// foreach Dates
        //exit;
        return $msg1;
    }
    
    /* Added By Mohan on : 03-01-17
    * To get the records for Excel genrate in Call Received (Specific Extension OR DID)
    * againts the destination number and Start,End date*/
    function generateCall_recivedExcel($startDate, $endDate,$call_To) {
        $did_no=0;
        $i=0;$k=1;$l=0;
        $msg1='';
        $dates1=array();
        $startDate = date('Y-m-d', strtotime($startDate));
        $endDate = date('Y-m-d', strtotime($endDate));
        //echo 'st:' . $startDate . '  | en:' . $endDate.' | '.$call_To;exit;
        $dates= $this->dateRange($startDate,$endDate);  
        //echo"hh". count($dates);exit;
        for($i=0;$i<(count($dates)-1);$i++)
        {
        $arr = array();
        $qry ="SELECT calldate,src,dst FROM queue_stat.cdr where dst='$call_To' AND calldate >'$startDate' AND calldate < '$endDate'";
        //echo $qry;exit;
        $qry1 = $this->db->query($qry);
        if ($qry1->num_rows() > 0) {
            foreach ($qry1->result() as $row){
                $arr['i'] = $k;
                $arr['calldate'] = (empty($row->calldate) ? '-' : $row->calldate);
                $arr['src'] = (empty($row->src) ? '-' : $row->src);
                $arr['dst'] = (empty($row->dst) ? '-' : $row->dst);
                $k++;
                $msg1[$l] = $arr;
                $l++;
            }
        }else{
                $arr['i'] = 0;  
                $arr['calldate'] = '-';
                $arr['src'] ='-';
                $arr['dst'] = '-';
                $msg1[0] = $arr;
        } 
     
        }
        return $msg1;
    }
    /*====End of Mohan code===*/
    
    /*Added By Rajnandini Lokhande on 07/12/2016*/
    
    function getObTeam() {
        $oblist = array();
        $query="select DISTINCT(ob_team) from extension_list where ob_team != 'NULL' AND ob_team!=''";
        $qry=$this->db->query($query);
        if ($qry->num_rows() > 0) {
            foreach ($qry->result() as $row) {
                $obj = new stdClass();
                $obj->ob_team = $row->ob_team;
                array_push($oblist, $obj);
            }
            return json_encode($oblist);
        }
    }
    
    

    function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d') {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
        while ($current <= $last) {
            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }
        return $dates;
    }
    
    function generateExcelAgentWiseOB(){
        $x='';
        $result1=array();
        $tot_billsec=0;
        $count=0;
        $msg='';
        $oblist= $this->input->post('obList');
        $select_extn_list= $this->input->post('extensionList');
        $inpstdatetime= $this->input->post('inpstdate');
        $inpstdate= date('Y-m-d',  strtotime($inpstdatetime));
        $startTime=date("H:i:s",strtotime($inpstdatetime));
        $inpEnddatetime = $this->input->post('inpEnddate');
        $inpEnddate= date('Y-m-d',  strtotime($inpEnddatetime));
        $endTime=date("H:i:s",strtotime($inpEnddatetime));
                
        if($select_extn_list!=''&& $select_extn_list == '"0"') {
            //echo 'extn all selected';
                $qry = $this->db->query('select DISTINCT(extension_name) as extension_name from extension_list where extension_name IS NOT NULL');
                if ($qry->num_rows() > 0) {
                    foreach ($qry->result() as $row) {
                        $x.='"' . $row->extension_name . '",';
                    }
                }
                $x = rtrim($x, ",");
                $select_extn_list = $x;
        }elseif($oblist!='' && $oblist == '"0"') {
            echo 'ob team all selected';
                $qry = $this->db->query('select DISTINCT(ob_team) as ob_team from extension_list where ob_team IS NOT NULL');
                if ($qry->num_rows() > 0) {
                    foreach ($qry->result() as $row) {
                        $x.='"' . $row->ob_team . '",';
                    }
                }
                $x = rtrim($x, ",");
                $oblist = $x;   
        }
        //print_r($select_extn_list);exit;
       $dateArray=$this->outbound_report_cdr_model->dateRange($inpstdate,$inpEnddate);
        if(count($dateArray)==1){
        $count=count($dateArray)+1;
        }else{
        $count=count($dateArray);  
        }
         for($i=0;$i<($count-1);$i++){
             if(count($dateArray)==1){
                $sd=$dateArray[$i].' '.$startTime;
                $ed=$dateArray[$i].' '.$endTime;
             }else{
                $sd=$dateArray[$i].' '.$startTime;
                $ed=$dateArray[$i+1].' '.$endTime;
             }
            if($select_extn_list!=''){
            $query="SELECT DISTINCT(c.src) src,el.agent_name FROM cdr c 
                    INNER JOIN extension_list el ON (el.extension_name=c.src)
                    where c.calldate>='$sd' 
                    AND c.calldate<='$ed' 
                    AND c.lastapp='Dial' AND c.src IN ($select_extn_list)";
            }else{
                $query="SELECT DISTINCT(c.src) src,el.agent_name FROM cdr c 
                    INNER JOIN extension_list el ON (el.extension_name=c.src)
                    where c.calldate>='$sd' 
                    AND c.calldate<='$ed' 
                    AND c.lastapp='Dial' AND el.ob_team IN ($oblist)";
            }
            $qry1=$this->db->query($query);
            if($qry1->num_rows()>0){
            $k=0;   
            foreach($qry1->result() as $row){

                    $result1['i']=$k;
                    $result1['date']=$dateArray[$i];
                    $result1['agent_name']=$row->agent_name;
                    $result1['extn']=$row->src;
                $query2="SELECT 
                    SUM(case when src IN ('$row->src') then 1 else 0 end) as no_of_dials,
                    SUM(case when src=('$row->src') AND disposition='ANSWERED' then 1 else 0 end) as calls_connected,
                    SUM(case when src IN ('$row->src') then billsec else 0 end) as tot_billsec 
                     FROM cdr c 
                    where c.calldate>='$sd' 
                    AND c.calldate<='$ed' 
                    AND c.lastapp='Dial'"; 
                
                $qry2=$this->db->query($query2);
                    $no_of_dials=(empty($qry2->row()->no_of_dials)?0:$qry2->row()->no_of_dials);
                    $result1['no_of_dials']=$no_of_dials;
                    $calls_connected=(empty($qry2->row()->calls_connected)?0:$qry2->row()->calls_connected);
                    $result1['calls_connected']=$calls_connected;
                    $tot_billsec=(empty($qry2->row()->tot_billsec)?0:$qry2->row()->tot_billsec);
                    $result1['tot_duration']= gmdate("H:i:s", $tot_billsec);
                    if($calls_connected==0){
                     $result1['avg_duration']=0;  
                    }else{
                     $result1['avg_duration']= gmdate("H:i:s", round($tot_billsec/$calls_connected,0));
                    }
                    $msg[$k]=$result1;
                    $k++;
            }
            return $msg;
         }else{
                    $result1['i']=0;
                    $result1['date']='-';
                    $result1['agent_name']='-';
                    $result1['extn']='-';
                    $result1['no_of_dials']=0;
                    $result1['calls_connected']=0;
                    $result1['tot_duration']=0;
                    $result1['avg_duration']=0;
                    $msg[0]=$result1;
            return $msg;
         }
    }   
    
    
    
    
    
    }
    
    /*
    * Author : Rahul 20.12.2016 | Purpose : Outbound SLA Report | Requested By: 
    */
    function get_extensionList() 
    {
        //This function fetches distinct queue name from extensio_list table
        //Displays the result in dropdown
        
        $msg='';
        
        $sql = "SELECT DISTINCT(iqueue_name) FROM extension_list WHERE extension_name IS NOT NULL";
        $query = $this->db->query($sql);
        
        $msg.='<option value="0">--Choose--</option>';
        if($query->num_rows() > 0){
            foreach($query->result() as $row){
                $msg.='<option value="'.$row->iqueue_name.'">'.$row->iqueue_name.'</option>';
            }
            $msg.='<option value="all">All</option>';
        }
        
        return $msg;
    }
    function ob_SLAreport(){
        $startDate = '';
        $endDate = '';
        $queueName = array();
        $result_arr = array();
        $arr = array();
        $k=0;
        
        if(isset($_POST['inpFromDate1'])){
            $startDate = $_POST['inpFromDate1'];
        }
        if(isset($_POST['inpToDate1'])){
            $endDate = $_POST['inpToDate1'];
        }
        if(isset($_POST['select_extn_list'])){
            $queueName = $_POST['select_extn_list'];
        }
        
        $startTime = date('H:i:s',  strtotime($startDate));
        $endTime = date('H:i:s',  strtotime($endDate));
        
        $startDate = date('Y-m-d',  strtotime($startDate));
        $endDate = date('Y-m-d',  strtotime($endDate));
        
        if($queueName[0]=='all'){
            $sql_all = "SELECT DISTINCT(iqueue_name) FROM extension_list WHERE extension_name IS NOT NULL";
            $query_all = $this->db->query($sql_all);
            foreach($query_all->result() as $data){
                $queueName[$k]=$data->iqueue_name;
                $k++;
            }
        }
        //echo "Date -- $startDate -- $endDate";exit;
        $count = count($queueName);
        for($i=0;$i<$count;$i++){
            $iqueue_name = $queueName[$i];
            $sql = "SELECT extension_name FROM extension_list WHERE iqueue_name='$iqueue_name' AND extension_name IS NOT NULL;";
            $query = $this->db->query($sql);
            
            $dateArray=$this->outbound_report_cdr_model->dateRange($startDate,$endDate);
            $date_count=count($dateArray);  
            
            for($j=0;$j<$date_count;$j++){
                $obj=new StdClass(); 
                        
                $sd=$dateArray[$j].' '.$startTime;
                $ed=$dateArray[$j].' '.$endTime;
                
                $obj->date = $dateArray[$j];
                $obj->iqueue = $iqueue_name;
                
                if($query->num_rows() > 0){
                    $no_of_dials_temp = 0;
                    $calls_connected_temp = 0;
                    $tot_billsec_temp = 0;
                    foreach($query->result() as $row){
                        $sql2="SELECT 
                        SUM(case when src IN ('$row->extension_name') then 1 else 0 end) as no_of_dials,
                        SUM(case when src=('$row->extension_name') AND disposition='ANSWERED' then 1 else 0 end) as calls_connected,
                        SUM(case when src IN ('$row->extension_name') then billsec else 0 end) as tot_billsec 
                        FROM cdr c 
                        where c.calldate>='$sd'
                        AND c.calldate<='$ed' 
                        AND c.lastapp='Dial'";
                        
                        $query2 = $this->db->query($sql2);
                        
                        $no_of_dials=(empty($query2->row()->no_of_dials)?0:$query2->row()->no_of_dials);
                        $no_of_dials_temp = $no_of_dials_temp + $no_of_dials;
                        
                        $calls_connected=(empty($query2->row()->calls_connected)?0:$query2->row()->calls_connected);
                        $calls_connected_temp = $calls_connected_temp + $calls_connected;
                        
                        $tot_billsec=(empty($query2->row()->tot_billsec)?0:$query2->row()->tot_billsec);
                        $tot_billsec_temp = $tot_billsec_temp + $tot_billsec;
                    }
                    
                    $obj->no_of_dial = $no_of_dials_temp;
                    $obj->calls_connected = $calls_connected_temp;
                    
                    if($no_of_dials_temp==0){
                        $no_of_dials_temp = 1;
                    }
                    
                    $conn_per=(($calls_connected_temp/$no_of_dials_temp)*100);
                    $obj->conn_per=round($conn_per,2);
                    
                    $obj->tot_duration= gmdate("H:i:s", $tot_billsec_temp);
                    if($calls_connected_temp==0){
                        $obj-> avg_duration=0;
                    }else{
                        $obj-> avg_duration= gmdate("H:i:s", round($tot_billsec_temp/$calls_connected_temp,0));
                    }
                    array_push($result_arr, $obj);
                }
            }
        }
        
        return json_encode($result_arr);
    }
    
    function ob_SLAreport_excelData($inpstdatetime, $inpEnddatetime, $queueList){
        //Fetch data for excel generation of sla report
        $queueList = explode(',', $queueList);
        $startDate = '';
        $endDate = '';
        $queueName = array();
        $result_arr = array();
        $arr = array();
        $k=0;
        
        if(isset($inpstdatetime)){
            $startDate = $inpstdatetime;
        }
        if(isset($inpEnddatetime)){
            $endDate = $inpEnddatetime;
        }
        if(isset($queueList)){
            $queueName = $queueList;
        }
        
        $startTime = date('H:i:s',  strtotime($startDate));
        $endTime = date('H:i:s',  strtotime($endDate));
        
        $startDate = date('Y-m-d',  strtotime($startDate));
        $endDate = date('Y-m-d',  strtotime($endDate));
        
        if($queueName[0]=='all'){
            $sql_all = "SELECT DISTINCT(iqueue_name) FROM extension_list WHERE extension_name IS NOT NULL";
            $query_all = $this->db->query($sql_all);
            foreach($query_all->result() as $data){
                $queueName[$k]=$data->iqueue_name;
                $k++;
            }
        }
        //echo "Date -- $startDate -- $endDate--";print_r($queueName);
        $count = count($queueName);
        for($i=0;$i<$count;$i++){
            $iqueue_name = $queueName[$i];
            $sql = "SELECT extension_name FROM extension_list WHERE iqueue_name='$iqueue_name' AND extension_name IS NOT NULL;";
            $query = $this->db->query($sql);

            $dateArray=$this->outbound_report_cdr_model->dateRange($startDate,$endDate);
            $date_count=count($dateArray);  
            
            for($j=0;$j<$date_count;$j++){
                $obj=new StdClass(); 
                        
                $sd=$dateArray[$j].' '.$startTime;
                $ed=$dateArray[$j].' '.$endTime;
                
                $obj->date = $dateArray[$j];
                $obj->iqueue = $iqueue_name;
                
                if($query->num_rows() > 0){
                    $no_of_dials_temp = 0;
                    $calls_connected_temp = 0;
                    $tot_billsec_temp = 0;
                    foreach($query->result() as $row){
                        $sql2="SELECT 
                        SUM(case when src IN ('$row->extension_name') then 1 else 0 end) as no_of_dials,
                        SUM(case when src=('$row->extension_name') AND disposition='ANSWERED' then 1 else 0 end) as calls_connected,
                        SUM(case when src IN ('$row->extension_name') then billsec else 0 end) as tot_billsec 
                        FROM cdr c 
                        where c.calldate>='$sd'
                        AND c.calldate<='$ed' 
                        AND c.lastapp='Dial'";
                        
                        $query2 = $this->db->query($sql2);
                        
                        $no_of_dials=(empty($query2->row()->no_of_dials)?0:$query2->row()->no_of_dials);
                        $no_of_dials_temp = $no_of_dials_temp + $no_of_dials;
                        
                        $calls_connected=(empty($query2->row()->calls_connected)?0:$query2->row()->calls_connected);
                        $calls_connected_temp = $calls_connected_temp + $calls_connected;
                        
                        $tot_billsec=(empty($query2->row()->tot_billsec)?0:$query2->row()->tot_billsec);
                        $tot_billsec_temp = $tot_billsec_temp + $tot_billsec;
                    }
                    
                    $obj->no_of_dial = $no_of_dials_temp;
                    $obj->calls_connected = $calls_connected_temp;
                    
                    if($no_of_dials_temp==0){
                        $no_of_dials_temp = 1;
                    }
                    
                    $conn_per=(($calls_connected_temp/$no_of_dials_temp)*100);
                    $obj->conn_per=round($conn_per,2);
                    
                    $obj->tot_duration= gmdate("H:i:s", $tot_billsec_temp);
                    if($calls_connected_temp==0){
                        $obj-> avg_duration=0;
                    }else{
                        $obj-> avg_duration= gmdate("H:i:s", round($tot_billsec_temp/$calls_connected_temp,0));
                    }
                    array_push($result_arr, $obj);
                }
            }
        }
        
        return $result_arr;
    }
    // End 20.12.2016
    
    function getCallAbandon($newStartDate,$newEndDate){
        $startDate = $newStartDate;
        $endDate = $newEndDate;
        $startDate=str_replace("-","/",$startDate);
        $endDate=str_replace("-","/",$endDate);
        $startDate = date('Y-m-d H:i:s', strtotime($startDate));
        //$startDate = $startDate.' '.'00:00:00';
        $endDate = date('Y-m-d H:i:s', strtotime($endDate));
        //echo "$startDate | $endDate";exit;
        //$endDate = $endDate.' '.'23:59:59';
        $query = $this->db->query("SELECT 
                                            time,cli,qname,time_queue_abandon
                                    FROM 
                                            asteriskcdrdb.each_call_details
                                    WHERE 
                                            abandon_flag=1 AND 
                                        time BETWEEN '$startDate' AND
                                        '$endDate'"); 
        return $query;
    }
}

?>
