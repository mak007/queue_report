<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Dashboard_report_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getrefreshrate() {
        $qlist = array();
        $qry = $this->db->select('refresh_rate')->get('report_configuration');
        $refreshRate = (empty($qry->row()->refresh_rate) ? REFRESHRATE : $qry->row()->refresh_rate);
        return $refreshRate;
    }

    function getQueuename() {
        $qlist = array();
        $qry = $this->db->select('queue')->get('qname');
        if ($qry->num_rows() > 0) {
            foreach ($qry->result() as $row) {
                $obj = new stdClass();
                $obj->qname = $row->queue;
                array_push($qlist, $obj);
            }
            return json_encode($qlist);
        }
    }

    function getForTheDayData() {
        $x = '';
        $final_result = array();
        $obj = new stdClass();
//        $startTime='2016-11-23 00:00:00';
//        $endTime='2016-11-23 16:00:00';
        $startTime = date('Y-m-d') . ' 00:00:00';
        $startTime = date('Y-m-d H:i:s', strtotime($startTime));
        $curDate = date('Y-m-d H');
        $minute = (date('i') >= 30) ? '30' : '00';
        $endTime = $curDate . ':' . $minute . ':00';
        $endTime = date('Y-m-d H:i:s', strtotime($endTime));
        $queue = $this->input->post('rpt');
        $queue = urldecode($queue);
        if ($queue == '"0"') {
            $qry = $this->db->select('queue')->get('qname');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->queue . '",';
                }
            }
            $x = rtrim($x, ",");
            $queue = $x;
        } else {
            $queue;
        }
        $query = "SELECT SUM(CASE WHEN id THEN 1 ELSE 0 END) AS coftd,
               SUM(CASE WHEN abandon_flag = 0 THEN 1 ELSE 0 END) AS caftd,
               ROUND(((SUM(CASE WHEN(abandon_flag = 0 AND time_queue_ans < 
               (SELECT service_level_threshold FROM report_configuration LIMIT 1)) 
               THEN 1 ELSE 0 END) / SUM(CASE WHEN (id) THEN 1 ELSE 0 END)) * 100),2) AS slftd
               FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime'
                AND qname IN (" . $queue . ") AND action != 'ENTERQUEUE'";
        $qry = $this->db->query($query);
        $obj->coftd = (empty($qry->row()->coftd) ? 0 : $qry->row()->coftd);
        $obj->caftd = (empty($qry->row()->caftd) ? 0 : $qry->row()->caftd);
        $obj->slftd = (empty($qry->row()->slftd) ? 0 : $qry->row()->slftd);

        $query = "SELECT count(id) AS ciqftd FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime' AND 
                qname IN (" . $queue . ")  AND action='ENTERQUEUE'";
        $qry = $this->db->query($query);
        $obj->ciqftd = (empty($qry->row()->ciqftd) ? 0 : $qry->row()->ciqftd);

        $query = "SELECT 
        sum(case when (abandon_flag=1) then 1 else 0 end) as abftd,
        ROUND(((sum(case when (abandon_flag=1) then 1 else 0 end)
        /sum(case when (id) then 1 else 0 end))*100),2) as ablftd,
        ROUND(((sum(case when (abandon_flag=0) then 1 else 0 end)
        /sum(case when (id) then 1 else 0 end))*100),2) as anslftd
         FROM each_call_details 
        WHERE time BETWEEN '$startTime' AND '$endTime' AND 
        qname IN (".$queue.") AND action!='ENTERQUEUE'";
        $qry = $this->db->query($query);
        $obj->abftd = (empty($qry->row()->abftd) ? 0 : $qry->row()->abftd);
        $obj->ablftd = (empty($qry->row()->ablftd) ? 0 : $qry->row()->ablftd);
        $obj->anslftd = (empty($qry->row()->anslftd) ? 0 : $qry->row()->anslftd);
        $query="SELECT ROUND(((SUM(time_queue_abandon)+SUM(time_queue_ans))/count(id)),0) as asa FROM each_call_details
        WHERE qname IN (".$queue.") AND time BETWEEN '$startTime' AND '$endTime'";
        $qry = $this->db->query($query);
        $obj->asa = (empty($qry->row()->asa) ? 0 : $qry->row()->asa);
        $query="SELECT 
                    ROUND((SUM(talk_time) / SUM(CASE
                                WHEN abandon_flag = 0 THEN 1
                                ELSE 0
                            END)),
                            2) AS att
                FROM
                    each_call_details
                WHERE qname IN (".$queue.") AND 
                    time BETWEEN '$startTime' AND '$endTime'";
         $qry = $this->db->query($query);
        $obj->att = (empty($qry->row()->att) ? 0 : $qry->row()->att);
        //------Final Push---------------
        array_push($final_result, $obj);
        return json_encode($final_result);
    }

    function getCurrentIntervalData() {
        $x = '';
        $final_result = array();
        $obj = new stdClass();
//        $startTime='2016-11-23 16:00:00';
//        $endTime='2016-11-23 16:22:00';
        $curDate = date('Y-m-d H');
        $minute = (date('i') >= 30) ? '30' : '00';
        $startTime = $curDate . ':' . $minute . ':00';
        $startTime = date('Y-m-d H:i:s', strtotime($startTime));
        $endTime = date('Y-m-d H:i:s');
        $queue = $this->input->post('rpt');
        $queue = urldecode($queue);
        if ($queue == '0') {
            $qry = $this->db->select('queue')->get('qname');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->queue . '",';
                }
            }
            $x = rtrim($x, ",");
            $queue = $x;
        } else {
            $queue;
        }
        $query = "SELECT SUM(CASE WHEN id THEN 1 ELSE 0 END) AS coftd,
               SUM(CASE WHEN abandon_flag = 0 THEN 1 ELSE 0 END) AS caftd,
               ROUND(((SUM(CASE WHEN(abandon_flag = 0 AND time_queue_ans < 
               (SELECT service_level_threshold FROM report_configuration LIMIT 1)) 
               THEN 1 ELSE 0 END) / SUM(CASE WHEN (id) THEN 1 ELSE 0 END)) * 100),2) AS slftd
               FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime'
                AND qname IN (" . $queue . ") AND action != 'ENTERQUEUE'";
        
        $qry = $this->db->query($query);
        $obj->coftd = (empty($qry->row()->coftd) ? 0 : $qry->row()->coftd);
        $obj->caftd = (empty($qry->row()->caftd) ? 0 : $qry->row()->caftd);
        $obj->slftd = (empty($qry->row()->slftd) ? 0 : $qry->row()->slftd);

        $query = "SELECT count(id) AS ciqftd FROM each_call_details WHERE qname IN (" . $queue . ")  AND action='ENTERQUEUE'";
        $qry = $this->db->query($query);
        $obj->ciqftd = (empty($qry->row()->ciqftd) ? 0 : $qry->row()->ciqftd);

        $query = "SELECT 
        sum(case when (abandon_flag=1) then 1 else 0 end) as abftd,
        ROUND(((sum(case when (abandon_flag=1) then 1 else 0 end)
        /sum(case when (id) then 1 else 0 end))*100),2) as ablftd,
        ROUND(((sum(case when (abandon_flag=0) then 1 else 0 end)
        /sum(case when (id) then 1 else 0 end))*100),2) as anslftd
         FROM each_call_details 
        WHERE time BETWEEN '$startTime' AND '$endTime' AND 
        qname IN (".$queue.") AND action!='ENTERQUEUE'";
        $qry = $this->db->query($query);
        $obj->abftd = (empty($qry->row()->abftd) ? 0 : $qry->row()->abftd);
        $obj->ablftd = (empty($qry->row()->ablftd) ? 0 : $qry->row()->ablftd);
        $obj->anslftd = (empty($qry->row()->anslftd) ? 0 : $qry->row()->anslftd);
        $obj->refreshFTD = 0;
        if (date('i') == 30 || date('i') ==00) {
            $obj->refreshFTD = 1;
        } else {
            $obj->refreshFTD = 0;
        }
        $query="SELECT ROUND(((SUM(time_queue_abandon)+SUM(time_queue_ans))/count(id)),0) as asa FROM each_call_details
        WHERE time BETWEEN '$startTime' AND '$endTime'";
        $qry = $this->db->query($query);
        $obj->asa = (empty($qry->row()->asa) ? 0 : $qry->row()->asa);
        $query="SELECT 
            ROUND((SUM(talk_time) / SUM(CASE
                        WHEN abandon_flag = 0 THEN 1
                        ELSE 0
                    END)),
                    2) AS att
        FROM
            each_call_details
        WHERE qname IN (".$queue.") AND 
            time BETWEEN '$startTime' AND '$endTime'";
         $qry = $this->db->query($query);
        $obj->att = (empty($qry->row()->att)?0:$qry->row()->att);
        //------Final Push---------------
        array_push($final_result, $obj);
        return json_encode($final_result);
    }

    function getGraphData() {
        $x = '';
        $arrT = array();
        $queue = $this->input->post('rpt');
        $queue = urldecode($queue);
        if ($queue == '"0"') {
            $qry = $this->db->select('queue')->get('qname');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->queue . '",';
                }
            }
            $x = rtrim($x, ",");
            $queue = $x;
        } else {
            $queue;
        }
   //     $selDate=date('2016-11-23');
                $selDate = date('Y-m-d');
        $arrInterval = $this->getTimeInInterval($selDate, TIMEDIFF);
        $x = count($arrInterval) - 1;
        for ($i = 0; $i < $x; $i++) {
            $obj = new stdClass();
            //$calls_offered=0;
            $date1 = $selDate . ' ' . $arrInterval[$i];
            $date2 = $selDate . ' ' . $arrInterval[$i + 1];
            $query = "SELECT 
                        SUM(CASE
                            WHEN id THEN 1
                            ELSE 0
                        END) AS calls_offfered,
                        SUM(CASE
                            WHEN abandon_flag = 0 THEN 1
                            ELSE 0
                        END) AS calls_answered,
                        ROUND(((SUM(CASE
                                    WHEN
                                        (abandon_flag = 0
                                            AND time_queue_ans < (SELECT 
                                                service_level_threshold
                                            FROM
                                                report_configuration
                                            LIMIT 1))
                                    THEN
                                        1
                                    ELSE 0
                                END) / SUM(CASE
                                    WHEN (id) THEN 1
                                    ELSE 0
                                END)) * 100),
                                2) AS service_level
                    FROM
                        each_call_details
                    WHERE
                        time > '$date1'
                            AND time < '$date2'
                            AND qname IN (" . $queue . ")
                            AND action != 'ENTERQUEUE'";

            $qry = $this->db->query($query);
            $obj->date1 = $arrInterval[$i];
            $obj->calls_offered = (empty($qry->row()->calls_offfered) ? 0 : $qry->row()->calls_offfered);
            $obj->calls_answered = (empty($qry->row()->calls_answered) ? 0 : $qry->row()->calls_answered);
            $obj->service_level = (empty($qry->row()->service_level) ? 0 : $qry->row()->service_level);
            array_push($arrT, $obj);
        }
        return json_encode($arrT);
    }

    //----------Common Function for getting intervals of a day-------------------------
    function getTimeInInterval($selDate, $interval) {
        $arrInterval = array();
        date_default_timezone_set('Asia/Kolkata');
        $selDate = date('Y-m-d', strtotime($selDate));
        $startTime = strtotime($selDate . "00:00:00");
        $endTime = strtotime($selDate . "23:59:59");

        $newStartTime = $startTime;
        $newendTime = $endTime;
        $number_of_interaval = (24 * 60) / $interval;
        $interval_slot = $interval * 60;
        $slot_start_time = $newStartTime;
        for ($i = 1; $i <= $number_of_interaval; $i++) {
            $time_interval = date('H:i', $slot_start_time);
            array_push($arrInterval, $time_interval . ':00');
            $slot_end_time = $slot_start_time + $interval_slot;
            $slot_start_time = $slot_end_time;
        }
        return $arrInterval;
    }

    function getRealtimeQueueAPIData(){
        $url=null;
        $queue_arr=array();
        $total_login=0;
        $count_pause=0;
        $count_incall=0;
        $total_hold=0;
        $total_ready=0;
        $array_res=array();
            $queue= urldecode($this->input->post('rpt'));
            if ($queue =='"0"') {
                array_push($queue_arr,' '); //array of queues
            } else{
                $queue=str_replace('"','',$queue);
                $queue_arr = explode(",",$queue);   //array of queues
            }
            if(count($queue_arr)==0){
                $obj=new stdClass();
                $obj->total_login=0;
                $obj->count_pause=0;
                $obj->count_incall=0;
                $obj->total_hold=0;
                $obj->total_ready=0;
                array_push($array_res,$obj);
                return json_encode($array_res); 
            }
            foreach($queue_arr as $queue1){
                $url = 'http://' . API_SERVER . '/tel_api/queue_test.php?queue='.$queue1.'';;
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $json_response = curl_exec($curl);
                $response = json_decode($json_response,true);
                //print_r($response);exit;
                $total_login=$total_login+(empty($response[2]['total_login'])?0:($response[2]['total_login']));
                $count_pause=$count_pause+(empty($response[2]['count_pause'])?0:($response[2]['count_pause']));
                $count_incall=$count_incall+(empty($response[2]['count_incall'])?0:($response[2]['count_incall']));
                $total_hold=$total_hold+(empty($response[2]['total_hold'])?0:($response[2]['total_hold']));
                $total_ready=$total_ready+(empty($response[2]['total_ready'])?0:($response[2]['total_ready']));
                curl_close($curl);
            }
            $obj=new stdClass();
            $obj->total_login=trim($total_login);
            $obj->count_pause=trim($count_pause);
            $obj->count_incall=trim($count_incall);
            $obj->total_hold=trim($total_hold);
            $obj->total_ready=trim($total_ready);
            array_push($array_res,$obj);
            return json_encode($array_res);
    }
    
    function display_rtm_report(){
        $queue = 0;
        $str = '';
        if(isset($_POST['queue']) && $_POST['queue']!=0){
            $queue = $_POST['queue'];
            for($i=0;$i<count($queue);$i++){
                if($i!=0){
                    $str.=',';
                }
                $str.=$queue[$i];
                $queue++;
            }
            
        }
        if($str==''){
            $qry2=$this->db->select('queue')->get('qname');
            if($qry2->num_rows()>0){
                foreach($qry2->result() as $qrows){
                    $str.='"'.$qrows->queue.'",';
                }
                $str=rtrim($str, ',');
            }
        }
        $curDate = date('Y-m-d H');
        $minute = (date('i') >= 30) ? '30' : '00';
        $startTime = $curDate . ':' . $minute . ':00';
        $startTime = date('Y-m-d H:i:s', strtotime($startTime));
        $endTime = date('Y-m-d H:i:s');
        $obj = new stdClass();
        
        $sql = "SELECT count(extension_no) AS extension_no, sum(case WHEN status like 'ACTIVE' then 1 else 0 end) AS active_call,    
                sum(case WHEN status like 'ONCALL' then 1 else 0 end) AS on_call,
                sum(case WHEN status like 'PAUSE' then 1 else 0 end) AS pause_call,
                sum(case WHEN status like 'ACW' then 1 else 0 end) AS acw_call,
                sum(case WHEN status like 'Other_Queue' then 1 else 0 end) AS other_queue
            FROM extension_status_report";
        if($str!=''){
            $sql.="  WHERE queue IN($str)";
        }
        //echo $sql;exit;
        //print_r($sql);
        $query = $this->db->query($sql);
        
        /*if($queue==0){
            $query1 = "SELECT IFNULL(count(id),0) AS ciqftd FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime' AND 
                action='ENTERQUEUE'";
        }else{
            $query1 = "SELECT IFNULL(count(id),0) AS ciqftd FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime' AND 
                qname IN (" . $queue . ")  AND action='ENTERQUEUE'";
        }*/
        $query1 = "SELECT IFNULL(count(id),0) AS ciqftd FROM each_call_details WHERE action='ENTERQUEUE'";
        if($str!=''){
            $query1.=" and qname IN($str)"; 
        }
        //echo $query1;exit;
        $qry = $this->db->query($query1);
        
        $obj->queue = $qry->row()->ciqftd;
        
        if($query->num_rows() > 0){
            $obj->rtm = $query->result();
            return json_encode($obj);
        }else{
            return false;
        }
    }
    
    function display_rtm_Extnreport(){
        $queue = 0;
        $str = '';
        if(isset($_POST['queue']) && $_POST['queue']!=0){
            $queue = $_POST['queue'];
            
            for($i=0;$i<count($queue);$i++){
                if($i!=0){
                    $str.=',';
                }
                $str.=$queue[$i];
                $queue++;
            }
            
        }
        
        $currentTime = date('Y-m-d H:i:s');
        $currentTime = strtotime($currentTime);
        $result_arr = array();
        
        $sql = "SELECT * FROM extension_status_report";
        if($str!=''){
            $sql.=" WHERE queue IN($str)";
        }
        //echo $sql;
        $query = $this->db->query($sql);
        
        if($query->num_rows() > 0){
            foreach ($query->result() AS $row){
                $obj = new stdClass();
                $time = date('Y-m-d H:i:s',strtotime($row->time));
                $unix_time = strtotime($time);
                $diff_time = $currentTime - $unix_time;
                
                $obj->extension_no = $row->extension_no;
                $obj->status = $row->status;
                $obj->time = gmdate("H:i:s",round($diff_time,0));
                
                array_push($result_arr, $obj);
            }
            return json_encode($result_arr);
        }else{
            return false;
        }
    }
    
    /**/
    
    public $slt_global;
    public $calls_offered_global;
    
    function get_service_level_threshold(){
        $sql = "SELECT service_level_threshold FROM report_configuration LIMIT 1";
        $qry = $this->db->query($sql);
        
        $slt = $qry->row()->service_level_threshold > 0 ? $qry->row()->service_level_threshold : 0;
        
        return $slt;
    }
    
    function get_call_offered($currentDate){
        $sql = "SELECT count(id) AS call_offered FROM each_call_details WHERE date(time)=date('$currentDate')";
        $qry = $this->db->query($sql);

        $calls_offered = $qry->row()->call_offered > 0 ? $qry->row()->call_offered : 0;
        
        return $calls_offered;
    }
    
    function get_sl_data(){
        $currentDate = date('Y-m-d');
        
        $startTime = date('Y-m-d') . ' 00:00:00';
        $startTime = date('Y-m-d H:i:s', strtotime($startTime));
        $curDate = date('Y-m-d H');
        $minute = (date('i') >= 30) ? '30' : '00';
        $endTime = $curDate . ':' . $minute . ':00';
        $endTime = date('Y-m-d H:i:s', strtotime($endTime));
        
        $slt = $this->get_service_level_threshold();

        $call_offered = $this->get_call_offered($currentDate);
        
        $str = '';
        if(isset($_POST['queue']) && $_POST['queue']!=0){
            $queue = $_POST['queue'];
            
            for($i=0;$i<count($queue);$i++){
                if($i!=0){
                    $str.=',';
                }
                $str.=$queue[$i];
                $queue++;
            }
            
        }
        
        $slt = is_numeric($slt) ? $slt : 0;
        $call_offered = $call_offered>0 ? $call_offered : 1;
        
        $sql = "SELECT count(id) AS call_in_sl FROM each_call_details WHERE time_queue_ans<$slt AND time BETWEEN '$startTime' AND '$endTime'";
        if($str!=''){
            $sql.=" AND qname IN($str)";
        }
        
        $qry = $this->db->query($sql);
        
        $calls_in_sl = $qry->row()->call_in_sl > 0 ? $qry->row()->call_in_sl : 0;
        
        $call_within_sl_per = ($calls_in_sl / $call_offered) * 100;
        
        return $call_within_sl_per;
    }
    
    function get_al_data(){
        $currentDate = date('Y-m-d');
        
        $startTime = date('Y-m-d') . ' 00:00:00';
        $startTime = date('Y-m-d H:i:s', strtotime($startTime));
        $curDate = date('Y-m-d H');
        $minute = (date('i') >= 30) ? '30' : '00';
        $endTime = $curDate . ':' . $minute . ':00';
        $endTime = date('Y-m-d H:i:s', strtotime($endTime));

        $call_offered = $this->get_call_offered($currentDate);
        
        $call_offered = $call_offered>0 ? $call_offered : 1;
        
        $str = '';
        if(isset($_POST['queue']) && $_POST['queue']!=0){
            $queue = $_POST['queue'];
            
            for($i=0;$i<count($queue);$i++){
                if($i!=0){
                    $str.=',';
                }
                $str.=$queue[$i];
                $queue++;
            }
            
        }
        
        $sql = "SELECT count(id) AS call_ans FROM each_call_details WHERE abandon_flag=0 AND time BETWEEN '$startTime' AND '$endTime'";
        if($str!=''){
            $sql.=" AND qname IN($str)";
        }
        $qry = $this->db->query($sql);
        
        $call_ans = $qry->row()->call_ans > 0 ? $qry->row()->call_ans : 0;
        
        $call_ans_per = ($call_ans / $call_offered) * 100;
        
        return $call_ans_per;
    }
    
    function display_rtm_Extnreport_v1(){
        $queue = 0;
        $str = '';
        if(isset($_POST['queue']) && $_POST['queue']!=0){
            $queue = $_POST['queue'];
            
            for($i=0;$i<count($queue);$i++){
                if($i!=0){
                    $str.=',';
                }
                $str.=$queue[$i];
                $queue++;
            }
            
        }
        
        $sort_by = isset($_POST['dropdown_sort']) ? $_POST['dropdown_sort'] : '';
        $order_by = isset($_POST['dropdown_order_by']) ? $_POST['dropdown_order_by'] : '';
        
        $currentTime = date('Y-m-d H:i:s');
        $currentTime = strtotime($currentTime);
        $result_arr = array();
        
        $sql = "SELECT * FROM extension_status_report";
        if($str!=''){
            $sql.=" WHERE queue IN($str)";
        }
        if($sort_by!=='' && $order_by!=''){
            switch($sort_by){
                case 'ext':
                    $sort_string = 'extension_no '.$order_by;
                case 'sta':
                    $sort_string = 'status '.$order_by;
                case 'que':
                    $sort_string = 'queue '.$order_by;
            }
            $sql.=" ORDER BY ".$sort_string;
        }
        //echo $sql;
        $query = $this->db->query($sql);
        
        if($query->num_rows() > 0){
            foreach ($query->result() AS $row){
                $obj = new stdClass();
                $time = date('Y-m-d H:i:s',strtotime($row->time));
                $unix_time = strtotime($time);
                $diff_time = $currentTime - $unix_time;
                
                $obj->extension_no = $row->extension_no;
                $obj->status = $row->status;
                $obj->time = gmdate("H:i:s",round($diff_time,0));
                $obj->queue = empty($row->queue) ? '-' : $row->queue ;
                
                array_push($result_arr, $obj);
            }
            return json_encode($result_arr);
        }else{
            return false;
        }
    }
    
    function display_rtm_report_v1(){
        $queue = 0;
        $str = '';
        if(isset($_POST['queue']) && $_POST['queue']!=0){
            $queue = $_POST['queue'];
            for($i=0;$i<count($queue);$i++){
                if($i!=0){
                    $str.=',';
                }
                $str.=$queue[$i];
                $queue++;
            }
            
        }
        if($str==''){
            $qry2=$this->db->select('queue')->get('qname');
            if($qry2->num_rows()>0){
                foreach($qry2->result() as $qrows){
                    $str.='"'.$qrows->queue.'",';
                }
                $str=rtrim($str, ',');
            }
        }
        $curDate = date('Y-m-d H');
        $minute = (date('i') >= 30) ? '30' : '00';
        $startTime = $curDate . ':' . $minute . ':00';
        $startTime = date('Y-m-d H:i:s', strtotime($startTime));
        $endTime = date('Y-m-d H:i:s');
        $obj = new stdClass();
        
        $sql = "SELECT count(extension_no) AS extension_no, sum(case WHEN status like 'ACTIVE' then 1 else 0 end) AS active_call,    
                sum(case WHEN status like 'ONCALL' then 1 else 0 end) AS on_call,
                sum(case WHEN status like 'PAUSE' then 1 else 0 end) AS pause_call,
                sum(case WHEN status like 'ACW' then 1 else 0 end) AS acw_call,
                sum(case WHEN status like 'Other_Queue' then 1 else 0 end) AS other_queue
            FROM extension_status_report";
        if($str!=''){
            $sql.="  WHERE queue IN($str)";
        }
        //echo $sql;exit;
        //print_r($sql);
        $query = $this->db->query($sql);
        
        /*if($queue==0){
            $query1 = "SELECT IFNULL(count(id),0) AS ciqftd FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime' AND 
                action='ENTERQUEUE'";
        }else{
            $query1 = "SELECT IFNULL(count(id),0) AS ciqftd FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime' AND 
                qname IN (" . $queue . ")  AND action='ENTERQUEUE'";
        }*/
        $query1 = "SELECT IFNULL(count(id),0) AS ciqftd FROM each_call_details WHERE action='ENTERQUEUE'";
        if($str!=''){
            $query1.=" and qname IN($str)"; 
        }
        //echo $query1;exit;
        $qry = $this->db->query($query1);
        
        $obj->queue = $qry->row()->ciqftd;
        
        if($query->num_rows() > 0){
            $obj->rtm = $query->result();
            return json_encode($obj);
        }else{
            return false;
        }
    }
    
    function getForTheDayData_v1() {
        $x = '';
        $final_result = array();
        $obj = new stdClass();

        $startTime = date('Y-m-d') . ' 00:00:00';
        $startTime = date('Y-m-d H:i:s', strtotime($startTime));
        $curDate = date('Y-m-d H');
        $minute = (date('i') >= 30) ? '30' : '00';
        $endTime = $curDate . ':' . $minute . ':00';
        $endTime = date('Y-m-d H:i:s', strtotime($endTime));
        $queue = $this->input->post('rpt');
        
        $queue = urldecode($queue);
        if ($queue == '"0"') {
            $qry = $this->db->select('queue')->get('qname');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->queue . '",';
                }
            }
            $x = rtrim($x, ",");
            $queue = $x;
        } else {
            $queue;
        }
        
        $query = "SELECT sum(case when (abandon_flag=1) then 1 else 0 end) as abftd,
            SUM(CASE WHEN id THEN 1 ELSE 0 END) AS coftd,
               SUM(CASE WHEN abandon_flag = 0 THEN 1 ELSE 0 END) AS caftd,
               ROUND(((SUM(CASE WHEN(abandon_flag = 0 AND time_queue_ans < 
               (SELECT service_level_threshold FROM report_configuration LIMIT 1)) 
               THEN 1 ELSE 0 END) / SUM(CASE WHEN (id) THEN 1 ELSE 0 END)) * 100),2) AS slftd
               FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime'
                AND action != 'ENTERQUEUE'";
        if($queue!=''){
            $query.=" AND qname IN (" . $queue . ")";
        }
        $qry = $this->db->query($query);
        $obj->coftd = (empty($qry->row()->coftd) ? 0 : $qry->row()->coftd);
        $obj->caftd = (empty($qry->row()->caftd) ? 0 : $qry->row()->caftd);
        $obj->abftd = (empty($qry->row()->abftd) ? 0 : $qry->row()->abftd);
        //$obj->slftd = (empty($qry->row()->slftd) ? 0 : $qry->row()->slftd);

        $query="SELECT 
                    ROUND((SUM(talk_time) / SUM(CASE
                                WHEN abandon_flag = 0 THEN 1
                                ELSE 0
                            END)),
                            2) AS att
                FROM
                    each_call_details
                WHERE time BETWEEN '$startTime' AND '$endTime'";
        if($queue!=''){
            $query.=" AND qname IN (" . $queue . ")";
        }
        $qry = $this->db->query($query);
        $obj->att = (empty($qry->row()->att) ? 0 : $qry->row()->att);
        //------Final Push---------------
        array_push($final_result, $obj);
        return json_encode($final_result);
    }
    
    function getCurrentIntervalData_v1() {
        $x = '';
        $final_result = array();
        $obj = new stdClass();

        $curDate = date('Y-m-d H');
        $minute = (date('i') >= 30) ? '30' : '00';
        $startTime = $curDate . ':' . $minute . ':00';
        $startTime = date('Y-m-d H:i:s', strtotime($startTime));
        $endTime = date('Y-m-d H:i:s');
        $queue = $this->input->post('rpt');
        $queue = urldecode($queue);
        if ($queue == '0') {
            $qry = $this->db->select('queue')->get('qname');
            if ($qry->num_rows() > 0) {
                foreach ($qry->result() as $row) {
                    $x.='"' . $row->queue . '",';
                }
            }
            $x = rtrim($x, ",");
            $queue = $x;
        } else {
            $queue;
        }
        $query = "SELECT SUM(CASE WHEN id THEN 1 ELSE 0 END) AS coftd,
               SUM(CASE WHEN abandon_flag = 0 THEN 1 ELSE 0 END) AS caftd,
               ROUND(((SUM(CASE WHEN(abandon_flag = 0 AND time_queue_ans < 
               (SELECT service_level_threshold FROM report_configuration LIMIT 1)) 
               THEN 1 ELSE 0 END) / SUM(CASE WHEN (id) THEN 1 ELSE 0 END)) * 100),2) AS slftd
               FROM each_call_details WHERE time BETWEEN '$startTime' AND '$endTime'
                AND action != 'ENTERQUEUE'";
        if($queue!=''){
            $query.=" AND qname IN (" . $queue . ")";
        }
        
        $qry = $this->db->query($query);
        $obj->coftd = (empty($qry->row()->coftd) ? 0 : $qry->row()->coftd);
        $obj->caftd = (empty($qry->row()->caftd) ? 0 : $qry->row()->caftd);
        $obj->slftd = (empty($qry->row()->slftd) ? 0 : $qry->row()->slftd).' %';

        $query = "SELECT count(id) AS ciqftd FROM each_call_details WHERE action='ENTERQUEUE'";
        if($queue!=''){
            $query.=" AND qname IN (" . $queue . ")";
        }
        $qry = $this->db->query($query);
        $obj->ciqftd = (empty($qry->row()->ciqftd) ? 0 : $qry->row()->ciqftd);

        $query = "SELECT 
        sum(case when (abandon_flag=1) then 1 else 0 end) as abftd,
        ROUND(((sum(case when (abandon_flag=1) then 1 else 0 end)
        /sum(case when (id) then 1 else 0 end))*100),2) as ablftd,
        ROUND(((sum(case when (abandon_flag=0) then 1 else 0 end)
        /sum(case when (id) then 1 else 0 end))*100),2) as anslftd
         FROM each_call_details 
        WHERE time BETWEEN '$startTime' AND '$endTime' AND 
        action!='ENTERQUEUE'";
        if($queue!=''){
            $query.=" AND qname IN (" . $queue . ")";
        }
        $qry = $this->db->query($query);
        $obj->abftd = (empty($qry->row()->abftd) ? 0 : $qry->row()->abftd);
        $obj->ablftd = (empty($qry->row()->ablftd) ? 0 : $qry->row()->ablftd).' %';
        $obj->anslftd = (empty($qry->row()->anslftd) ? 0 : $qry->row()->anslftd).' %';
        $obj->refreshFTD = 0;
        if (date('i') == 30 || date('i') ==00) {
            $obj->refreshFTD = 1;
        } else {
            $obj->refreshFTD = 0;
        }
        $query="SELECT ROUND(((SUM(time_queue_abandon)+SUM(time_queue_ans))/count(id)),0) as asa FROM each_call_details
        WHERE time BETWEEN '$startTime' AND '$endTime'";
        $qry = $this->db->query($query);
        $obj->asa = (empty($qry->row()->asa) ? 0 : $qry->row()->asa);
        $query="SELECT 
            ROUND((SUM(talk_time) / SUM(CASE
                        WHEN abandon_flag = 0 THEN 1
                        ELSE 0
                    END)),
                    2) AS att
        FROM
            each_call_details
        WHERE time BETWEEN '$startTime' AND '$endTime'";
        if($queue!=''){
            $query.=" AND qname IN (" . $queue . ")";
        }
        $qry = $this->db->query($query);
        $obj->att = (empty($qry->row()->att)?0:$qry->row()->att);
        
        
        $ocw_query = "SELECT time AS old_time FROM each_call_details WHERE action='ENTERQUEUE' AND
            time BETWEEN '$startTime' AND '$endTime' ORDER BY time ASC ";
        $ocw_qry = $this->db->query($ocw_query);
        $old_time = (empty($ocw_qry->row()->old_time)?0:$ocw_qry->row()->old_time);
        if($old_time!=0){
            $dteStart = new DateTime($old_time);
            $dteEnd   = new DateTime($endTime);

            $dteDiff  = $dteStart->diff($dteEnd);
            $difference = $dteDiff->h.' : '.$dteDiff->i.' : '.$dteDiff->s;
            $obj->ocw = !empty($dteDiff)?$difference:'00:00:00';
        }else{
            $obj->ocw = '00:00:00';
        }
        
        $sql = "SELECT service_level_threshold FROM report_configuration LIMIT 1";
        $qry = $this->db->query($sql);
        
        $slt = $qry->row()->service_level_threshold > 0 ? $qry->row()->service_level_threshold : 0;
        
        $sql = "SELECT count(id) AS call_in_sl FROM each_call_details WHERE time_queue_ans<$slt AND time BETWEEN '$startTime' AND '$endTime'";
        if($queue!=''){
            $sql.=" AND qname IN($queue)";
        }
        
        $qry = $this->db->query($sql);
        
        $calls_in_sl = $qry->row()->call_in_sl > 0 ? $qry->row()->call_in_sl : 0;
        
        $obj->call_in_sl = $calls_in_sl;
        
        //------Final Push---------------
        array_push($final_result, $obj);
        return json_encode($final_result);
    }
    /**/
}
