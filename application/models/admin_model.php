<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Admin_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
      //  $this->load->model('orders_model');
    }
    
	
	function firstTimeCalledReportExcel($startDate,$endDate){
	$startDate=date('Y-m-d', strtotime($startDate));
	$endDate=date('Y-m-d', strtotime($endDate));
	//	echo $startdate.$enddate;exit;
       // $dt=date('Y-m-d', strtotime($dt));
		//print_r($dt);
	//	return;
        $qry=$this->db->query("SELECT * FROM `vicidial_log` WHERE `call_date` >= '$startDate' AND `call_date`<= '$endDate' AND `called_count` = '1'");
        if($qry->num_rows() > 0){
            $mode_of_order='';
			$arr='';
			$i=0;
           foreach($qry->result() as $row) {
			   
               $arr['lead_id'][$i]=$row->lead_id;
			   $arr['phone_number'][$i]=$row->phone_number;
			   $arr['campaign_id'][$i]=$row->campaign_id;
			   $arr['call_date'][$i]=$row->call_date;
			   $arr['status'][$i]=$row->status;
			   $arr['called_count'][$i]=$row->called_count;
           
            $i++;
            
		   }
            
            $msg=$arr;
            
        }
        else {
            $msg.='No records found';
        }
        return $msg;
    }
    
    
    function agentReportExcel($startDate,$endDate){
	$startDate=date('Y-m-d', strtotime($startDate));
	$endDate=date('Y-m-d', strtotime($endDate));
	//	echo $startdate.$enddate;exit;
       // $dt=date('Y-m-d', strtotime($dt));
		//print_r($dt);
	//	return;
        $qry=$this->db->query("SELECT DISTINCT user FROM `vicidial_agent_log` WHERE `event_time` >= '$startDate' AND `event_time`<= '$endDate'");
        if($qry->num_rows() > 0){
            $mode_of_order='';
			$arr='';
			$i=0;
           foreach($qry->result() as $row) {
			   $username=$row->user;
               $arr['username'][$i]=$row->user;
			   $laedSql=$this->db->query("SELECT count(lead_id) as LeadId FROM `vicidial_agent_log` WHERE user ='$username'  and `lead_id` IS NOT NULL");
			   $meinSql=$this->db->query("SELECT sum(pause_sec) as pauseSec,sum(wait_sec) as waitSec,sum(talk_sec) as talkSec,sum(dispo_sec) as dispoSec,sum(dead_sec) as deadTime FROM `vicidial_agent_log` WHERE user ='$username'");
			   // $firstDiffSql=$this->db->query("SELECT event_time FROM `vicidial_agent_log` WHERE user ='$username'  and `lead_id` IS NOT NULL ORDER BY column LIMIT 1) as 'first'";
				//$firstDiffSql=$this->db->query("SELECT column FROM table WHERE [condition] ORDER BY column DESC LIMIT 1) as 'last'";
				$totalTime=$meinSql->row()->pauseSec+$meinSql->row()->waitSec+$meinSql->row()->talkSec+$meinSql->row()->dispoSec+$meinSql->row()->deadTime;
			   $arr['toal_time'][$i]=round(($totalTime/60),2);
			   $arr['leadId'][$i]=$laedSql->row()->LeadId;
			   $arr['pauseSec'][$i]=round(($meinSql->row()->pauseSec)/60,2);
			   $arr['waitSec'][$i]=round(($meinSql->row()->waitSec)/60,2);
			   $arr['talkSec'][$i]=round(($meinSql->row()->talkSec)/60,2);
			   $arr['dispoSec'][$i]=round(($meinSql->row()->dispoSec)/60,2);
			   $arr['deadTime'][$i]=round(($meinSql->row()->deadTime)/60,2);
			   
			   
           
            $i++;
            
		   }
            
            $msg=$arr;
            
        }
        else {
            $msg.='No records found';
        }
        return $msg;
    }
    
    
    
    
     function userLogExcel($startDate,$endDate){
	$startDate=date('Y-m-d', strtotime($startDate));
	$endDate=date('Y-m-d', strtotime($endDate));
	//	echo $startdate.$enddate;exit;
       // $dt=date('Y-m-d', strtotime($dt));
		//print_r($dt);
	//	return;
        $qry=$this->db->query("SELECT DISTINCT user FROM `vicidial_agent_log` WHERE `event_time` >= '$startDate' AND `event_time`<= '$endDate'");
        if($qry->num_rows() > 0){
            $mode_of_order='';
			$arr='';
			$i=0;
           foreach($qry->result() as $row) {
			   $username=$row->user;
               $arr['username'][$i]=$row->user;
			   $laedSql=$this->db->query("SELECT count(lead_id) as LeadId FROM `vicidial_agent_log` WHERE user ='$username'  and `lead_id` IS NOT NULL");
			   $meinSql=$this->db->query("SELECT sum(pause_sec) as pauseSec,sum(wait_sec) as waitSec,sum(talk_sec) as talkSec,sum(dispo_sec) as dispoSec,sum(dead_sec) as deadTime FROM `vicidial_agent_log` WHERE user ='$username'");
			   // $firstDiffSql=$this->db->query("SELECT event_time FROM `vicidial_agent_log` WHERE user ='$username'  and `lead_id` IS NOT NULL ORDER BY column LIMIT 1) as 'first'";
				//$firstDiffSql=$this->db->query("SELECT column FROM table WHERE [condition] ORDER BY column DESC LIMIT 1) as 'last'";
				$totalTime=$meinSql->row()->pauseSec+$meinSql->row()->waitSec+$meinSql->row()->talkSec+$meinSql->row()->dispoSec+$meinSql->row()->deadTime;
			   $arr['toal_time'][$i]=round(($totalTime/60),2);
			   $arr['leadId'][$i]=$laedSql->row()->LeadId;
			   $arr['pauseSec'][$i]=round(($meinSql->row()->pauseSec)/60,2);
			   $arr['waitSec'][$i]=round(($meinSql->row()->waitSec)/60,2);
			   $arr['talkSec'][$i]=round(($meinSql->row()->talkSec)/60,2);
			   $arr['dispoSec'][$i]=round(($meinSql->row()->dispoSec)/60,2);
			   $arr['deadTime'][$i]=round(($meinSql->row()->deadTime)/60,2);
			   
			   
           
            $i++;
            
		   }
            
            $msg=$arr;
            
        }
        else {
            $msg.='No records found';
        }
        return $msg;
    }
	}
?>