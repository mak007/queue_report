<?php

Class Imports_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

   
    function getCampaignsList() {
        $qry=$this->db->select('*')->where('datafile_added', 0)->get('campaigns');
        if($qry->num_rows() > 0) {
            $msg='<select class="inp" id="selectCampaign" onclick="getCampaignID();">';
            $msg.='<option value="0">--Choose--</option>';
            foreach($qry->result() as $row){
                $msg.='<option value="' . $row->campaign_id .'">'. $row->title .'</option>';
            }
            $msg.='</select>';
//            $msg.='<form id="formCampaign" name="formCampaign" method="post" action"">';
//            $msg.='<input style="display: none;" type="text" id="campaignID" name="campaignID"/>';
//            $msg.='</form>';
        }
        else {
            $msg='No campaign data pending...';
        }
        return $msg;
        
    }

    function markCampaignDataAsImported($campaignID){
        $this->db->where('campaign_id', $campaignID);
        $this->db->set('datafile_added',1);
        $this->db->update('campaigns');
        return;
    }

    function insertCustomer($arr) {
	if( $this->db->insert('amazon', $arr))
           {
	$i=$this->db->insert_id();
	}
	else{
	$i=0;
	}
           
       
        return $i;
    }









function genrateExcel(){
	
        $qry=$this->db->query("SELECT * FROM `amazon`");
        if($qry->num_rows() > 0){
            
			$arr='';
			$i=0;
           foreach($qry->result() as $row) {
		   $ph_no=$row->Phone;
		   $cdrSql=$this->db->query("select * from cdr where dst='$ph_no' order by calldate DESC limit 1");
				
               $arr['Tracking_ID'][$i]=$row->Tracking_ID;
			   $arr['Previous_scan'][$i]=$row->Previous_scan;
			   $arr['Phone'][$i]=$row->Phone;
			   $arr['Item_description'][$i]=$row->Item_description;
			   $arr['item_value'][$i]=$row->item_value;
			   $arr['Station_code'][$i]=$row->Station_code;
			   
			   $callDate=$cdrSql->row()->calldate;
			   if($callDate != ''){
				
				$splitTimeStamp = explode(" ",$callDate);
			    $arr['call_date'][$i] = $splitTimeStamp[0];
				$arr['call_time'][$i] = $splitTimeStamp[1];
			   }if($callDate == ''){
			    $arr['call_date'][$i] ='-';
				$arr['call_time'][$i] = '-';
			   }
			   
			   $str= $cdrSql->row()->accountcode;
			  // print_r($str);exit;
			   If($str!=''){
				$strLEN=strlen($str);
				$lang=substr($str, 0, 2);
				if($lang=='en') {
					$arr['lang'][$i]='English';
				}
				if($lang=='hi'){
					$arr['lang'][$i]='Hindi';
				}
				$todo=''; $dt='';
				if($strLEN > 2) {
					$todo=substr($str, 2, 1);
					if ($todo==1) {
						$arr['todo'][$i]="Reschedule";
					}
					if($todo==2){
						$arr['todo'][$i]='Cancel';
					}
					if($todo==3){
						$arr['todo'][$i]='Order details' ;
					}
					if($strLEN > 3){
					$dt=substr($str, 3, 1);
					$d=$cdrSql->row()->calldate;
					if($dt==1) {
						$arr['dt'][$i]=date('Y-m-d', strtotime("$d + 1 Day"));
					}
					if($dt==2) {
						$arr['dt'][$i]=date('Y-m-d', strtotime("$d + 2 Days"));
					}
					if($dt==3) {
						$arr['dt'][$i]=date('Y-m-d', strtotime("$d + 3 Days"));
					}
					if($dt==4) {
						$arr['dt'][$i]=date('Y-m-d', strtotime("$d + 4 Days"));
					}
					
					if($strLEN > 4){
					$time=substr($str, 4, 1);
					//$d=$cdrSql->row()->calldate;
					if($time==1) {
						$arr['time'][$i]="Between 9AM to 12 noon";
					}
					if($time==2) {
						$arr['time'][$i]="Between 12Pm to 4PM";
					}
					if($time==3) {
						$arr['time'][$i]="Between 4PM to 8PM";
					}
					
					
					}ELSE{
					$arr['time'][$i]='-';
					}
					
					}else{
					 $arr['dt'][$i]='-';
					}
				}else{
				$arr['todo'][$i]='-';
				}
			   }
			   else{
			   
			   $arr['lang'][$i]='-';
			   $arr['todo'][$i]='-';
			  $arr['dt'][$i]='-';
			   $arr['time'][$i]='-';
			   }
			   
			   
			   
			   
           
            $i++;
            
		   }
            
            $msg=$arr;
            
        }
        else {
            $msg.='No records found';
        }
        return $msg;
    }







}

//---------------------------------Model ends here------------------------------