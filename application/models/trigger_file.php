<?php
#--28 Mrch--2017 --  Changes in API affected trigger. need  to rectify issues.

$sql_28March2K17="CREATE DEFINER=`en-hop`@`%` TRIGGER `trigger_each_call_details` AFTER INSERT ON `queue_log` FOR EACH ROW
BEGIN
  IF (new.event='ENTERQUEUE') THEN
	INSERT INTO each_call_details (callid,time,cli,qname,action) 
    VALUES 
    (NEW.callid,NEW.created, NEW.data2, NEW.queuename,NEW.event);
    
   ELSEIF(new.event='ABANDON') THEN
        UPDATE each_call_details ecd , queue_log ql
        SET ecd.abandon_flag=1,ecd.time_queue_abandon=new.data3,ecd.action=new.event
        WHERE new.callid=ecd.callid and new.queuename=ecd.qname;
    
    ELSEIF(new.event='CONNECT') THEN
		UPDATE each_call_details ecd , queue_log ql
        SET ecd.time_queue_ans=new.data1,ecd.action=new.event 
        WHERE new.callid=ecd.callid and new.queuename=ecd.qname;
		UPDATE extension_status_report SET status='ONCALL', time=new.time, call_id=new.callid
        WHERE extension_no=new.agent and queue=new.queuename;
        UPDATE extension_status_report SET status='Other_Queue', time=new.time
        WHERE extension_no=new.agent and queue!=new.queuename;
        
   ELSEIF(new.event='ADDMEMBER') THEN
		IF EXISTS( SELECT extension_no FROM extension_status_report WHERE extension_no=new.agent AND queue=new.queuename) THEN
			UPDATE extension_status_report SET status='ACTIVE', time=new.time
            WHERE extension_no=new.agent;
		ELSE
			INSERT INTO extension_status_report (extension_no,queue,status,time)
			VALUES
			(new.agent,new.queuename,'ACTIVE',new.time);
		END IF;
        
	ELSEIF(new.event='COMPLETECALLER') THEN
		UPDATE extension_status_report SET status='ACW', time=new.time
        WHERE extension_no=new.agent and queue=new.queuename and call_id=new.callid;
		UPDATE each_call_details ecd , queue_log ql
        SET ecd.talk_time=new.data2,ecd.agent=new.agent,ecd.discon_by=if(new.event='COMPLETECALLER','CALLER','AGENT'),ecd.action=new.event
        WHERE new.callid=ecd.callid and new.queuename=ecd.qname;
        
	ELSEIF(new.event='COMPLETEAGENT') THEN
		UPDATE extension_status_report SET status='ACTIVE', time=new.time, call_id=null
        WHERE extension_no=new.agent;
		UPDATE each_call_details ecd , queue_log ql
        SET ecd.talk_time=new.data2,ecd.agent=new.agent,ecd.discon_by=if(new.event='COMPLETECALLER','CALLER','AGENT'),ecd.action=new.event
        WHERE new.callid=ecd.callid and new.queuename=ecd.qname;
        
	ELSEIF((new.event='PAUSE') OR new.event='PAUSEALL') THEN
		SET @status=(SELECT status FROM extension_status_report esr WHERE extension_no=new.agent LIMIT 1);
		IF(@status!='ONCALL') THEN
		UPDATE extension_status_report SET status='PAUSE', time=new.time
        WHERE extension_no=new.agent;
        END IF;
	ELSEIF(new.event='UNPAUSEALL') THEN
		UPDATE extension_status_report SET status='ACTIVE', time=new.time
        WHERE extension_no=new.agent;
        
	ELSEIF(new.event='REMOVEMEMBER') THEN
		DELETE FROM extension_status_report 
        WHERE extension_no=new.agent AND id>0;

	ELSEIF(new.event='UNPAUSE') THEN
    UPDATE extension_status_report SET status='ACTIVE', time=new.time
	WHERE extension_no=new.agent;
    SET @curtime =UNIX_TIMESTAMP(new.time);
    SET @cur_agent=new.agent;
    SET @pre_time=(SELECT UNIX_TIMESTAMP(q.time) FROM queue_log q 
    WHERE q.event='COMPLETECALLER' AND q.agent=@cur_agent order by q.id desc limit 1);
    
    SET @callid=(SELECT  callid FROM queue_log q 
    WHERE q.event='COMPLETECALLER' order by q.id desc limit 1);
    
    SET @acw_time =ROUND(@curtime - @pre_time);
		UPDATE each_call_details ecd , queue_log ql
		SET ecd.acw_time=@acw_time WHERE ecd.callid=@callid;
    END IF;
END";


$qry_4_april_2017="CREATE DEFINER=`root`@`localhost` TRIGGER `trigger_each_call_details` AFTER INSERT ON `queue_log` FOR EACH ROW
BEGIN
  IF (new.event='ENTERQUEUE') THEN
	INSERT INTO each_call_details (callid,time,cli,qname,action) 
    VALUES 
    (NEW.callid,NEW.created, NEW.data2, NEW.queuename,NEW.event);
    
   ELSEIF(new.event='ABANDON') THEN
        UPDATE each_call_details ecd , queue_log ql
        SET ecd.abandon_flag=1,ecd.time_queue_abandon=new.data3,ecd.action=new.event
        WHERE new.callid=ecd.callid and new.queuename=ecd.qname;
    
    ELSEIF(new.event='CONNECT') THEN
		UPDATE each_call_details ecd , queue_log ql
        SET ecd.time_queue_ans=new.data1,ecd.action=new.event 
        WHERE new.callid=ecd.callid and new.queuename=ecd.qname;
		UPDATE extension_status_report SET status='ONCALL', time=new.time, call_id=new.callid
        WHERE extension_no=new.agent and queue=new.queuename;
        UPDATE extension_status_report SET status='Other_Queue', time=new.time
        WHERE extension_no=new.agent and queue!=new.queuename;
        
   ELSEIF(new.event='ADDMEMBER') THEN
		IF EXISTS( SELECT extension_no FROM extension_status_report WHERE extension_no=new.agent AND queue=new.queuename) THEN
			UPDATE extension_status_report SET status='ACTIVE', time=new.time
            WHERE extension_no=new.agent;
		ELSE
			INSERT INTO extension_status_report (extension_no,queue,status,time)
			VALUES
			(new.agent,new.queuename,'ACTIVE',new.time);
		END IF;
        
	ELSEIF(new.event='COMPLETECALLER') THEN
		UPDATE extension_status_report SET status='ACW', time=new.time
        WHERE extension_no=new.agent and queue=new.queuename and call_id=new.callid;
		UPDATE each_call_details ecd , queue_log ql
        SET ecd.talk_time=new.data2,ecd.agent=new.agent,ecd.discon_by=if(new.event='COMPLETECALLER','CALLER','AGENT'),ecd.action=new.event
        WHERE new.callid=ecd.callid and new.queuename=ecd.qname;
        
	ELSEIF(new.event='COMPLETEAGENT') THEN
		UPDATE extension_status_report SET status='ACTIVE', time=new.time, call_id=null
        WHERE extension_no=new.agent;
		UPDATE each_call_details ecd , queue_log ql
        SET ecd.talk_time=new.data2,ecd.agent=new.agent,ecd.discon_by=if(new.event='COMPLETECALLER','CALLER','AGENT'),ecd.action=new.event
        WHERE new.callid=ecd.callid and new.queuename=ecd.qname;
        
	ELSEIF((new.event='PAUSE') OR new.event='PAUSEALL') THEN
		SET @status=(SELECT status FROM extension_status_report esr WHERE extension_no=new.agent LIMIT 1);
		IF(@status!='ONCALL') THEN
		UPDATE extension_status_report SET status='PAUSE', time=new.time
        WHERE extension_no=new.agent;
        END IF;
	ELSEIF(new.event='UNPAUSEALL') THEN
		UPDATE extension_status_report SET status='ACTIVE', time=new.time
        WHERE extension_no=new.agent;
        
    ELSEIF(new.event='RINGNOANSWER') THEN
        UPDATE extension_status_report SET status='RnR', time=new.time
        WHERE extension_no=new.agent;  

	ELSEIF(new.event='REMOVEMEMBER') THEN
		DELETE FROM extension_status_report 
        WHERE extension_no=new.agent AND id>0;

	ELSEIF(new.event='UNPAUSE') THEN
    UPDATE extension_status_report SET status='ACTIVE', time=new.time
	WHERE extension_no=new.agent;
    SET @curtime =UNIX_TIMESTAMP(new.time);
    SET @cur_agent=new.agent;
    SET @pre_time=(SELECT UNIX_TIMESTAMP(q.time) FROM queue_log q 
    WHERE q.event='COMPLETECALLER' AND q.agent=@cur_agent order by q.id desc limit 1);
    
    SET @callid=(SELECT  callid FROM queue_log q 
    WHERE q.event='COMPLETECALLER' order by q.id desc limit 1);
    
    SET @acw_time =ROUND(@curtime - @pre_time);
		UPDATE each_call_details ecd , queue_log ql
		SET ecd.acw_time=@acw_time WHERE ecd.callid=@callid;
    END IF;
END";