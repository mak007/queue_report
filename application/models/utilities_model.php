<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Utilities_model extends CI_Model {
   function _construct() {
        parent::__construct();
        }

   function init_data(){
        $data['base']=base_url();
	$data['js_path']=  base_url() . 'javascript/';
        $data['images_path']=base_url() . 'images/';
        $data['css_path']=base_url() . 'css/';
        return $data;
   } 
   
       public function convert_date($in_val) {
        //This function takes a string value as returned by Javascript Calendar and convertes into
        //mySQL datetime format.
        if ($in_val <> '') {
            $day = (int) abs(substr($in_val, 0, 2));
            $mon = (int) abs(substr($in_val, 3, 2));
            $yr = (int) abs(substr($in_val, 6, 4));
            $new_dt = date('Y-m-d', mktime(0, 0, 0, $mon, $day, $yr));
            return $new_dt;
        }
    }
    
    
   
} //--------------Model ends here------------

