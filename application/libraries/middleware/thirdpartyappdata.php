<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Thirdpartyappdata
    {
        #Invoked After Customer Saved in CRM
        #From c_generic_b2b (saveNewCustomerb2b) 6.07.2016
        function callMethod($method_name, $obj = null)
        {   
            
            if(!is_null($method_name) && $method_name != '')
            {                
                if((int)method_exists($this,$method_name) > 0)
                {
                   $msg=$this->$method_name($obj);
                   return $msg;
                }
                else
                {
                    return "false";
                }
            }
        }
        function getRealtimeQueueAPIData($arr){
            $queue_arr=array();
            $queue=  urldecode($arr->queue);
            if ($queue == '"0"' || $queue == 0) {
                $qry = $this->db->select('queue')->get('qname');
                if ($qry->num_rows() > 0) {
                    foreach ($qry->result() as $row) {
                        array_push($queue_arr, $row->queue); //array of queues
                    }
                }
            } else {
                $queue=str_replace('"','',$queue);
                $queue_arr = explode(",",$queue);   //array of queues
            }
            //print_r($queue_arr);exit;
        }

        #Invoked After Customer Edit Saved in CRM
        #From c_generic_b2b (saveEditCustomerb2b) 6.07.2016
        function edit_customer($cust_arr, $accountId, $clientId, $customerId){
            echo "Third Party Customer Edit Api ";
            return 0;
        }

        #Invoked After Address Saved in CRM
        #From c_address (save_address) 6.07.2016
        function save_address($arr, $accountId, $customerId){
            echo "Third Party Address Api ";
            return 0;
        }

        #Invoked After Address Edit Saved in CRM
        #From c_address (update_address) 6.07.2016
        function edit_address($arr, $customerId){
            echo "Third Party Address Edit Api ";
            return 0;
        }
    }
?>