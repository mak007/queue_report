<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
<!--        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-timepicker-addon.css' ?>"/>
        -->        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/datatables/dataTables.bootstrap.css'; ?>"/>


        <link rel="stylesheet" href="<?php echo $css_path . 'css/ionicons.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/fullcalendar/fullcalendar.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/daterangepicker/daterangepicker-bs3.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>"/>


        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-timepicker-addon.js'; ?>"></script>
<!--        <script type="text/javascript" src="<?php echo $js_path . 'agents.js'; ?>"></script> -->


        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/raphael.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/fullcalendar/fullcalendar.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/daterangepicker/daterangepicker.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'ss3.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'admin.js'; ?>"></script>
        <script src="<?php echo base_url('javascript/outbound_report_cdr.js'); ?>"></script>


        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>

        <script type="text/javascript" src="<?php echo $js_path . 'ss3.js'; ?>"></script>
        <script>
            $(function () {
                $("#datepickerEndDate").datepicker({
                    format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });
            $(document).ready(function () {
                hide_extn();

            });
        </script>
        <script>
            $(function () {
                $("#datepickerStartDate").datepicker({
                    format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });


        </script> 
        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>
        <script>
            $(function () {
                $("#datepickerEndDate").datepicker({
                    format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });
        </script>
        <script>
            $(function () {
                $("#datepickerStartDate").datepicker({
                    format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });


        </script>
    </head>
    <body class="skin-blue" onload="select_extension();getObTeam();">

        <br></br>
        <aside class="right-side">  
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label>Outbound Report</label>
                        <select id="select_client_ticket" class="form-control" style="width:200px;" onchange="hide_extn();">
                            <!--                            <option  value="">Day Wise SL</option>
                                                        <option  value="">Intra Day SL Reports</option>-->
                            <option value="2">CDR Reports</option>
                            <option value="1">Each Call Details</option>

                        </select>
                    </div>

                    <div class="col-md-1"></div>
                    <div class="col-md-2" style="width: 300px; margin-left: -60px;">
                        <label> Start Date :</label>
                        <input class="form-control" id="datepickerStartDate" type="text" value="<?php echo date('d-m-Y'); ?>">
                    </div>
                    <div class="col-md-2" style="width: 300px; margin-left: -180px;">
                        <label> End Date :</label>
                        <input class="form-control" id="datepickerEndDate" type="text" value="<?php echo date('d-m-Y'); ?>">
                    </div>
                    <div class="col-md-2" style="margin-left: -180px;" id="exn_div">
                        <label id="extn_name">Extension List</label>
                        <select multiple id="select_extn_list" name="select_extn_list" class="form-control" style="width: 151px;">

                            <option value="0">Choose</option>
                        </select>

                    </div>
                    <div class="col-md-2" style="margin-left: -13px;">
                        <label>Ob Team</label>
                        <select multiple id="oblist" class="form-control" style="width:110px;">


                        </select>
                    </div>
                    <div class="col-md-2" style="margin-left: -70px;">
                        <label>Phone No</label>
                        <input type="text" id="ph_no" name="ph_no"></input>

                    </div>
                    <div class="col-md-2">
                        <label></label><br><br>
                                <button id="" class="btn btn-info" type="button" onclick="get_outbound_cdr_report();">View List</button>
                                </div>
                                </div>
                                </div>
                                </aside>
                                <!--	<aside class="right-side">  
                                        
                                                <div class="row">
                                                        <div class="col-md-12">
                                                        
                                                                                 <div class="box">
                                                                <div class="box-header">
                                                                    <h3 class="box-title">Call Center Reports (Inbound)</h3>
                                                                </div>
                                                                <div class="box-body">
                                                                    <table class="table table-bordered">
                                                                        <tr>
                                                                            <th style="width: 10px">#</th>
                                                                            <th>Task</th>
                                                                            <th>Progress</th>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td>1.</td>
                                                                            <td> 
                                                                                                                                        <a href="<?php echo base_url('c_report/day_wise_sl'); ?>"><button  class="btn bg-maroon margin">Day Wise SL</button></a>
                                                                                                                        </td>
                                                                            <td>
                                                                                                                                                        DESCRIPTION  
                                                                            </td>
                                                                            
                                                                        </tr>
                                                                        <tr>
                                                                            <td>2.</td>
                                                                            <td>
                                                                                                                                        <a href="<?php echo base_url('c_report/each_call_details'); ?>"><button  class="btn bg-maroon margin">Each Call Details</button></a>
                                                                                                                        </td>
                                                                            <td>
                                                                                                                                                        DESCRIPTION
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                                                                <tr>
                                                                            <td>3.</td>
                                                                            <td>
                                                                                                                                        <a href="<?php echo base_url('c_report/intra_day_sl'); ?>"><button  class="btn bg-maroon margin">Intra Day SL Reports</button></a>
                                                                                                                        </td>
                                                                            <td>
                                                                                                                                                        DESCRIPTION
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                                                                
                                                                                                                <tr>
                                                                            <td>4.</td>
                                                                            <td>
                                                                                                                                        <a href="<?php echo base_url('c_report/cdr_excel'); ?>"><button  class="btn bg-maroon margin">CDR Reports</button></a>
                                                                                                                        </td>
                                                                            <td>
                                                                                                                                                        DESCRIPTION
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        
                                                                        
                                                                    </table>
                                                                </div>
                                                                
                                                            </div>
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                             
                                                            <div class="box box-solid">
                                                                <div class="box-header">
                                                                    <h3 class="box-title">Reports</h3>
                                                                </div>
                                                                <div class="box-body">
                                                                   
                                                                <a href="<?php echo base_url('c_report/day_wise_sl'); ?>"><button  class="btn bg-maroon margin">Day Wise SL</button></a>
                                                                        <button class="btn bg-purple margin">Intra day SL</button>
                                                                   
                                                                </div>
                                                            </div>
                                                            </div> 
                                                        </div>
                                            <div class="col-xs-12">
                                
                                                                                
                                             </div>  
                                                         
                                        </div>           
                                     </aside>  -->
                                <br></br>
                                <div id="cdr_report">
                                    <aside class="right-side">  
                                        <div class="container-fluid" style="margin:0 15px; background-color:#fff; box-shadow:0px 0px 5px #777; padding:15px;overflow-y:auto;  ">
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <table id="tableOutboundReport" class="table table-bordered " style="width: 100%;">
                                                        <thead style="background-color: rgb(249,249,249); color: white;">
                                                            <tr>
                                                                <th>Sr No</th>
                                                                <th>Call date</th>
                                                                <th>Extension No</th>
                                                                <th>Phone No.</th>
                                                                <th>Duration</th>
                                                                <th>Bill Sec.</th>
                                                                <th>Disposition</th>
                                                                <th>Recording File</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tbody_ObReport">
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                                <div id="each_call_report">
                                    <aside class="right-side">  
                                        <div class="container-fluid" style="margin:0 15px; background-color:#fff; box-shadow:0px 0px 5px #777; padding:15px;overflow-y:auto;  ">
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <table id="tableEachCallReport" class="table table-bordered ">
                                                        <thead style="background-color: rgb(249,249,249); color: white;">
                                                            <tr>
                                                                <th>ID</th>
                                                                <th>Phone No.</th>
                                                                <th>Date</th>
                                                                <th>Duration</th>
                                                                <th>Disposition</th>
                                                                <th>Wait</th>
                                                                <th>Talk</th>
                                                                <th>Agent</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="tbody_report">
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                                <center><h3></h3></center>
                                <script type="text/javascript">
                                    $(function () {
                                        $("#Campaign").dataTable();
                                        $("#Choose Campaign").dataTable();
                                        $('#Campaign1').dataTable({
                                            "bPaginate": true,
                                            "bLengthChange": true,
                                            "bFilter": true,
                                            "bSort": true,
                                            "bInfo": true,
                                            "bAutoWidth": true
                                        });
                                    });
                                </script>


                                </body>
                                </html>
