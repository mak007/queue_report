<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript" src="<?php echo base_url('javascript/jquery-1.10.js');?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('css/jquery-ui-1.10.4.custom.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/css/font-awesome.min.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/css/AdminLTE.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/bridge_login.css');?>">
        <script type="text/javascript" src="<?php echo base_url('javascript/js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('javascript/js/AdminLTE/app.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('javascript/bridge_login.js')?>"></script>
        <script>
            $(document).ready(function(){
                 $.ajax({
                    url: BASEPATH + 'c_report/get_did_number',
                    type: 'POST',
                    success: function (msg){
                        $('#bridge_did_numr').html(msg);
                    }
                });
            });
        </script>
    </head> 
<body>
    <div class="container">
        <section id="content">
            <span>
                <h4>
                 <div class='error_msg' id="error_msg"></div>
                </h4>
            </span>
            <form id="frm_rest" name="frm_rest">
                <h1>Reset Password</h1>
                <div>
                    <select name="bridge_did_numr" id="bridge_did_numr">
                        <option value="">Select DID Number</option>
                    </select> 
                    <!--<input type="text" placeholder="DID Number" required="" id="bridge_did_numr" name="bridge_did_numr"/>-->
                </div>
                <div>
                    <input type="password" placeholder="Old Password" required="" id="bridge_old_password" name="bridge_old_password"/>
                </div>
                <div>
                    <input type="password" placeholder="New Password" required="" id="bridge_new_password" name="bridge_new_password"/>
                </div>
                <div>
                    <input type="password" placeholder="Confirm Password" required="" id="bridge_confirm_password" name="bridge_confirm_password"/>
                </div>
                <div>
                    <input onclick="bridge_reset_pass();" type="button" value="Reset" id="btdn_bridge_reset" name="btdn_bridge_reset"/>
                </div>
            </form>
            <div class="button">
                <a href="">Update Password</a>
            </div>
        </section>
    </div>
</body>
</html>
