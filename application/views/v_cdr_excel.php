<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
                <script type="text/javascript">
        var BASEPATH = "<?php echo base_url() ;   ?>"
        </script>
        <script type="text/javascript">
     //   var user_id = 5;
        </script>
		<script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css' ; ?>"/>
<!--        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-timepicker-addon.css'     ?>"/>
-->        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css' ; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css' ; ?>"/>
		<link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css' ; ?>"/>
		<link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css' ; ?>"/>
		<link rel="stylesheet" href="<?php echo $css_path . 'css/datatables/dataTables.bootstrap.css' ; ?>"/>
		
		
		<link rel="stylesheet" href="<?php echo $css_path . 'css/ionicons.min.css.css' ; ?>"/>
		<link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css' ; ?>"/>
		<link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css' ; ?>"/>
		<link rel="stylesheet" href="<?php echo $css_path . 'css/fullcalendar/fullcalendar.css' ; ?>"/>
		<link rel="stylesheet" href="<?php echo $css_path . 'css/daterangepicker/daterangepicker-bs3.css' ; ?>"/>
		<link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css' ; ?>"/>
		
		
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-timepicker-addon.js'; ?>"></script>
<!--        <script type="text/javascript" src="<?php echo $js_path . 'agents.js'; ?>"></script> -->
		
		
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/raphael.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>
		
		<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/fullcalendar/fullcalendar.min.js'; ?>"></script>
		
		<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/daterangepicker/daterangepicker.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>
		
		<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
		
		<script type="text/javascript" src="<?php echo $js_path . 'ss3.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'admin.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo $js_path . 'reports.js'; ?>"></script>
		
       
	 <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }
           
            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
			.form-control {
    display: block;
    width: 40%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857;
    color: #555;
    vertical-align: middle;
    background-color: #FFF;
    background-image: none;
    border: 1px solid #CCC;
    border-radius: 4px;
	}
        </style>
	
        <script type="text/javascript" src="<?php echo $js_path . 'ss3.js'; ?>"></script>
        <script type="text/javascript">
        
			 function selectQueue(){
                $.ajax({
                    url: BASEPATH +  'c_report/selectQueue',
                    success: function(msg){
                        $('#viewQueue').html(msg);
                    }
                })
                
                
            } 
         
        </script>
    </head>
    <body class="skin-blue" onload='selectQueue();'>		
	<aside class="right-side">  
        
		<div class="row">
			<div class="col-md-12">
                            <!-- Custom Tabs -->
                            <div class="box box-solid">
                                <div class="box-header">
                                    <h3 class="box-title">CDR
									<placeholder>  </placeholder>
									</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
								
								<div class="row">
								<div class="col-md-6">
                                 <form id="formDateUnpaid">
								  Start Date:
								  <input type="date" name="inpFromDate" id = "inpFromDate">
								  End Date:
								  <input type="date" name="inpToDate" id = "inpToDate">
								
									<input type="text" style="display: none;" id="inpQueue" name="inpQueue" value=""/>
									
								
								</form>
								</div>
								<!--div class="col-md-6" id="viewQueue">
								</div-->
								
								</div>
									<table class="table table-bordered table-striped dataTable">
									<tr>
									
									<td><?php
													$d1 = array(
                                                        'name' => 'inpstdate',
                                                        'id' => 'inpstdate',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													$d2 = array(
                                                        'name' => 'inpEnddate',
                                                        'id' => 'inpEnddate',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													$d3 = array(
                                                        'name' => 'inpPhone',
                                                        'id' => 'inpPhone',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: non; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													
													echo form_open('c_report/genrate_cdr_excel/');
                                                    echo form_input($d1);
													echo form_input($d2);
													echo "Enter Phone No : - ".form_input($d3);
													
                                                    '<br/>'; '<br/>';
                                                    echo '<br/><br/><br/>'.form_submit('Login','Generate Excel');
                                                    echo form_close();
													?></td></tr>
													<tr><td><button onclick="display_records();">Diplay Records</button></td>
									</tr>
									</table>
                                </div><!-- /.box-body -->
								
								
                                
                                                                <table id="cdrExcel" class="table table-bordered ">
                                                                <thead style="background-color: rgb(249,249,249); color: white;">
                                                                    <tr>
                                                                        <th>Date Time</th>
                                                                        <th>Phone No</th>
                                                                        <th>Queue</th>
                                                                        <th>Bill Time</th>
                                                                        <th>Status</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="display_records_div">
                                                                </tbody>
                                                            </table>
								
								
                            </div><!-- /.box -->
                            </div><!-- nav-tabs-custom -->
                        </div><!-- /.col -->
            <div class="col-xs-6">

			</div>  
			 
        </div>           
     </aside>   
      
        <center><h3></h3></center>
       <script type="text/javascript">
            $(function() {
				$("#Campaign").dataTable();
				$("#Choose Campaign").dataTable();
				$('#Campaign1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true
                });
            });
        </script>
    </body>
</html>
