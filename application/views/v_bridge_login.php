<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript" src="<?php echo base_url('javascript/jquery-1.10.js');?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('css/jquery-ui-1.10.4.custom.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/css/font-awesome.min.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/css/AdminLTE.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/bridge_login.css');?>">
        <script type="text/javascript" src="<?php echo base_url('javascript/js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('javascript/js/AdminLTE/app.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('javascript/bridge_login.js')?>"></script>
    </head> 
<body>
    <div class="container">
        <section id="content">
            <!--<form action="">-->
            <span>
                <h4>
                <?php
                echo "<div class='error_msg'>";
                if (isset($error_message)) {
                    echo $error_message;
                }
                echo validation_errors();
                echo "</div>";
                ?>
                </h4>
            </span>
            <?php echo form_open("c_report/bridge_login_check"); ?>
                <h1>Login Form</h1>
                <div>
                    <input type="text" placeholder="Username" required="" id="bridge_username" name="bridge_username"/>
                </div>
                <div>
                    <input type="password" placeholder="Password" required="" id="bridge_password" name="bridge_password"/>
                </div>
                <div>
                    <input type="submit" value="Log in" id="btn_bridge_login" name="btn_bridge_login"/>
                </div>
            <?php echo form_close();   ?>
            <!--</form>-->
            <div class="button">
                <a href="">Sign In</a>
            </div>
        </section>
    </div>
</body>
</html>
