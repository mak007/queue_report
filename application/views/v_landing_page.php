<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'report_telephony_custom.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'hover-min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <!--<link rel="stylesheet" href="<?php // echo $css_path . 'css/ionicons.min.css.css';    ?>"/>-->
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>"/>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>
        
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
        
        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
            <script src=<?php echo base_url('javascript/highcharts/highcharts.js'); ?>></script>
        <!--<script src="https://code.highcharts.com/modules/exporting.js"></script>-->
        <script type="text/javascript" src="<?php echo $js_path . 'dashboard_report.js'; ?>"></script>
        <style>
            #reports_telephony_titleBar{
                padding-top: 5px;
                padding-bottom: 5px;
                height: 70px;
                background-color: rgba(245,245,245,1.00);
            }
            #queuename{
                height:60px;
            }
            #heading_report_telephony{
                margin-top: 15px;
                text-align:center;
            }
            .status_interval_reportsT{
                width:15px;
                height:15px;
                /*background-color:rgba(1,211,255,1.00);*/
                background-color: rgba(1, 211, 255, 0.49);
                border-radius:2px;
            }
            .status_FTD_reportsT{
                width:15px;
                height:15px;
                /*background-color:rgba(54,213,0,1.00);*/
                background-color: rgba(129, 65, 241, 0.28);
                border-radius:2px;
            }
            .skin-blue .sidebar{
                border-bottom: 0px solid #fff;
            }
            #highcharts{
                background-color: red !important;
            }
        </style>
    </head>
    <!-- init(); -->
    <body onload='init();'>
        <aside class="right-side" > 	
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12" id="reports_telephony_titleBar">
                        <div class="col-sm-1">
                            
                        </div>
                        <div class="col-sm-2">
                        <select multiple id="select_queue_list" name="select_queue_list" class="form-control" style="padding:0px;width:170px; height: 65px;">
                            <option value="0">Choose</option>
                        </select>
                        </div>
                        <div class="col-sm-6">
                            <h3 id="heading_report_telephony"><span class="fa fa-safari"></span> Realtime Dashboard</h3>
                        </div>
                        <div class="col-sm-3">
                            <div class="col-sm-12" style="margin-top: 5px;">
                                <div class="col-sm-3">
                                    <h5 class="status_interval_reportsT"></h5>
                                    <h5 class="status_FTD_reportsT"></h5>
                                </div>
                                <div class="col-sm-9" style="padding:0">
                                    <h5>Current Interval</h5>
                                    <h5>For The Day</h5>
                                </div>
                            </div>
                        </div>
                        <!--<button class="btn btn-flat btn-primary">Show Details</button>-->
                    </div>
                </div>
                <div class="row" style="margin-top:10px;">
                    <div class="col-sm-12" id="reports_telephony_campaign">
                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Calls Offered</p>
                                    <p class="reports_telephony_interval" id="cofci">0</p>
                                    <p class="reports_telephony_FTD" id="coftd" >0</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Calls Answered</p>
                                    <p class="reports_telephony_interval" id="cafci">0</p>
                                    <p class="reports_telephony_FTD"id="caftd">0</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Service Level (80/20)</p>
                                    <p class="reports_telephony_interval" id="slfci">0</p>
                                    <p class="reports_telephony_FTD" id="slftd">0</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Average Handle Time</p>
                                    <p class="reports_telephony_interval" id="att_ci">NA</p>
                                    <p class="reports_telephony_FTD" id="att_ftd">NA</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Call Queue</p>
                                    <p class="reports_telephony_interval" id="ciqfci">0</p>
                                    <p class="reports_telephony_FTD" id="ciqftd">0</p>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                        <div class="row" style="margin-top:10px;">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Abandon</p>
                                    <p class="reports_telephony_interval" id="abfci">0</p>
                                    <p class="reports_telephony_FTD" id="abftd">0</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Agent Abandon</p>
                                    <p class="reports_telephony_interval">NA</p>
                                    <p class="reports_telephony_FTD">NA</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Answered Level</p>
                                    <p class="reports_telephony_interval" id="anslfci">0</p>
                                    <p class="reports_telephony_FTD" id="anslftd">0</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Abandon Level</p>
                                    <p class="reports_telephony_interval" id="ablfci">0</p>
                                    <p class="reports_telephony_FTD" id="ablftd">0</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">ASA</p>
                                     <p class="reports_telephony_interval" id="asa_ci">NA</p>
                                    <p class="reports_telephony_FTD" id="asa_ftd">NA</p>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                    </div>
                </div>
                <div class="row" style="border-top:1px solid rgba(0,0,0,0.1); margin-top:15px;">
                    <div class="col-sm-12" id="reports_telephony_agent">
                        <div class="row">
                            <div class="col-sm-1"></div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Agents Logged In</p>
                                    <p class="reports_telephony_interval" id="total_login">0</p>
                                    <p class="reports_telephony_FTD" id="total_login">NA</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Agents Idle</p>
                                    <p class="reports_telephony_interval" id="total_ready">0</p>
                                    <p class="reports_telephony_FTD" id="total_ready">NA</p>
                                </div>
                            </div>                          
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Agent On Pause</p>
                                    <p class="reports_telephony_interval" id="count_pause">0</p>
                                    <p class="reports_telephony_FTD" id="count_pause">NA</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Agents On Call</p>
                                    <p class="reports_telephony_interval" id="count_incall">0</p>
                                    <p class="reports_telephony_FTD" id="count_incall">NA</p>
                                </div>
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="col-sm-12 reports_telephony_statusBox hvr-grow-shadow">
                                    <p class="reports_telephony_status_head hideHead_reportTelephony">Agent On Hold</p>
                                    <p class="reports_telephony_interval" id="total_hold">0</p>
                                    <p class="reports_telephony_FTD" id="total_hold">NA</p>
                                </div>
                            </div>
                            <div class="col-sm-1"></div>
                        </div>
                    </div>
                </div>
                <br/><br/>
                <div class="row">
                    <div class="col-sm-12" id="container">
                         BAR CHART HERE..
                    </div>
                </div>
                
                <div class="row">
                    
                </div>
<!--                <div class="row">
                    <div class="col-md-12" align="center" id="div_rtm">
                        <table style="margin-top: 10px;" class="table-bordered" style="width:70%">
                            <thead style="background-color: rgb(91,171,223); color: white;">
                                <tr>
                                    <th>Extension No</th>
                                    <th>Current Status</th>
                                    <th>Duration (sec)</th>
                                </tr>
                            </thead>
                            <tbody id="extn_tbl_body">
                                <tr>
                                    <td> - </td>
                                    <td> - </td>
                                    <td> - </td>
                                </tr>
                            </tbody>
                            
                        </table>
                    </div>
                </div>-->
            </div>              

        </aside>      
    </body>
</html>
