<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'report_telephony_custom.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'hover-min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        
        <link href="<?php echo $css_path . 'bootstrap.min.css'; ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo $css_path . 'telephony-RTM.css'; ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo $css_path . 'font-awesome.css'; ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo $css_path . 'hover-min.css'; ?>" rel="stylesheet" type="text/css" />
        <!--<link rel="stylesheet" href="<?php // echo $css_path . 'css/ionicons.min.css.css';    ?>"/>-->
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>"/>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>
        <script src="<?php echo $js_path.'js/jquery-3.1.0.min.js'?>"></script> 
        <script src="<?php echo $js_path.'bootstrap.min.js'?>"></script> 
        <script type="text/javascript" src="<?php echo $js_path.'js/telephony-RTM.js'?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
        
        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
            <script src=<?php echo base_url('javascript/highcharts/highcharts.js'); ?>></script>
        <!--<script src="https://code.highcharts.com/modules/exporting.js"></script>-->
        <script type="text/javascript" src="<?php echo $js_path . 'dashboard_report.js'; ?>"></script>


<!--        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>-->
        <!-- Commited by mohan on 03-01-2017 because script written 2 times-->
<!--        <script>
            $(function () {
                $("#datepickerEndDate").datepicker({
                    format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });
        </script>-->
<!--        <script>
            $(function () {
                $("#datepickerStartDate").datepicker({
                    format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });


        </script> -->
<!--        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>-->
        <script>
            $(function () {
                $("#datepickerEndDate").datepicker({
                    format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });
        </script>
        <script>
            $(function () {
                $("#datepickerStartDate").datepicker({
                    format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });


        </script>
    </head><!--select_queue();-->
    <body onload='init_rtm();select_queue1();'>
      <section>
	<div class="container-fluid heading-RTM">
		<h2 style="margin-left: 41px;">Real Time Monitoring Reports</h2>
	</div>
	<div class="container-fluid margin_leftTop">
		<div class="col-sm-12">
<!-- LEFT SIDE OF TELEPHONY RTM HERE -->
			<div class="col-sm-6">
				<div class="col-sm-12" style="padding:0;">
					<div class="col-sm-6">
						<label>Select RTM</label>
                                                <select multiple id="select_queue_list" name="select_queue_list" class="form-control" style="padding:0px;width:170px; font-size:14px;height: 65px;">
                                                    <option value="0">Choose</option>
                                                </select>
					</div>
				</div>
<!-- RTM STATUS HERE -->
				<div class="col-sm-12 col-xs-12">
					<div class="col-sm-6 col-xs-12 currentStatus-all-RTM">
						<div class="col-sm-12 col-xs-12 backStatus-all-RTM hvr-bounce-to-top text-center" title="Logged In">
                                                        <div class="col-sm-2 marginTopStatus-RTM">
                                                            <h4><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span></h4>
                                                        </div>
                                                        <div class="col-sm-7 linHeightStatus-RTM">
                                                            <p>Logged In</p>
                                                        </div>
                                                        <div class="col-sm-3 marginTopStatus-RTM">
                                                            <h3 style="margin-top: 10px;"><label class="label label-success" id="logged_in">0</label></h3>
                                                        </div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12 currentStatus-all-RTM">
						<div class="col-sm-12 col-xs-12 backStatus-all-RTM hvr-bounce-to-top text-center" title="Available">
                                                        <div class="col-sm-2 marginTopStatus-RTM">
                                                            <h4><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></h4>
                                                        </div>
							<div class="col-sm-7 linHeightStatus-RTM">
                                                            <p>Available</p>
                                                        </div>
                                                        <div class="col-sm-3 marginTopStatus-RTM">
                                                            <h3 style="margin-top: 10px;"><label class="label label-info" id="available">0</label></h3>
                                                        </div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12 currentStatus-all-RTM">
						<div class="col-sm-12 col-xs-12 backStatus-all-RTM hvr-bounce-to-top text-center" title="On Call">
							<div class="col-sm-2 marginTopStatus-RTM">
                                                            <h4><span class=" glyphicon glyphicon-headphones" aria-hidden="true"></span></h4>
                                                        </div>
                                                        <div class="col-sm-7 linHeightStatus-RTM">
                                                            <p>On Call</p>
                                                        </div>
                                                        <div class="col-sm-3 marginTopStatus-RTM">
                                                            <h3 style="margin-top: 10px;"><label class="label label-warning" id="on_call">0</label></h3>
                                                        </div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12 currentStatus-all-RTM">
						<div class="col-sm-12 col-xs-12 backStatus-all-RTM hvr-bounce-to-top text-center" title="Other Queue">
							<div class="col-sm-2 marginTopStatus-RTM">
                                                            <h4><span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span></h4>
                                                        </div>
                                                        <div class="col-sm-7 linHeightStatus-RTM">
                                                            <p>Other Queue</p>
                                                        </div>
                                                        <div class="col-sm-3 marginTopStatus-RTM">
                                                            <h3 style="margin-top: 10px;"><label class="label label-default" id="other_queue">0</label></h3>
                                                        </div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12 currentStatus-all-RTM">
						<div class="col-sm-12 col-xs-12 backStatus-all-RTM hvr-bounce-to-top text-center" title="Pause">
							<div class="col-sm-2 marginTopStatus-RTM">
                                                            <h4><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></h4>
                                                        </div>
                                                        <div class="col-sm-7 linHeightStatus-RTM">	
                                                            <p>Pause</p>
                                                        </div>
							<div class="col-sm-3 marginTopStatus-RTM">
                                                            <h3 style="margin-top: 10px;"><label class="label label-danger" id="pause">0</label></h3>
                                                        </div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12 currentStatus-all-RTM">
						<div class="col-sm-12 col-xs-12 backStatus-all-RTM hvr-bounce-to-top text-center" title="After Call Work">
							<div class="col-sm-2 marginTopStatus-RTM">
                                                            <h4><span class="glyphicon glyphicon-file" aria-hidden="true"></span></h4>
                                                        </div>
                                                        <div class="col-sm-7 linHeightStatus-RTM">
                                                            <p>ACW</p>
                                                        </div>
                                                        <div class="col-sm-3 marginTopStatus-RTM">
                                                            <h3 style="margin-top: 10px;"><label class="label label-primary" id="acw">0</label></h3>
                                                        </div>
						</div>
					</div>

<!--THIS DIV TO BE REMOVED FOR EXTRA STATUS --><div class="col-sm-3"></div>

					<div class="col-sm-6 col-xs-12 currentStatus-all-RTM">
						<div class="col-sm-12 col-xs-12 backStatus-all-RTM hvr-bounce-to-top text-center" title="Calls In Queue">
							<div class="col-sm-2 marginTopStatus-RTM">
                                                            <h4><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></h4>
                                                        </div>
                                                        <div class="col-sm-7 linHeightStatus-RTM">
                                                            <p>Calls In Queue</p>
                                                        </div>
							<div class="col-sm-3 marginTopStatus-RTM">
                                                            <h3 style="margin-top: 10px;"><label class="label label-warning" id="call_in_queue">0</label></h3>
                                                        </div>
						</div>
					</div>

<!-- EXTRA STATUS DIV HERE 
					<div class="col-sm-6 col-xs-12 currentStatus-all-RTM">
						<div class="col-sm-12 col-xs-12 backStatus-all-RTM hvr-bounce-to-top text-center" title="title here..">
							<div class="col-sm-2 marginTopStatus-RTM">
                                                            <h4><span class=" glyphicon glyphicon-info-sign" aria-hidden="true"></span></h4>
                                                        </div>
                                                        <div class="col-sm-7 linHeightStatus-RTM">
                                                            <p>Extra</p>
                                                        </div>
                                                        <div class="col-sm-3 marginTopStatus-RTM">
                                                            <h3 style="margin-top: 10px;"><label class="label label-default">0</label></h3>
                                                        </div>
						</div>
					</div> -->
				</div>
			</div>
<!-- RIGHT SIDE OF TELEPHONY RTM HERE -->
<!-- TABLE FOR RTM HERE -->
			<div class="col-sm-6 ">
				<div class="table-responsive">          
					<table id="extn_tbl" class="table table-bordered table-hover scrollTable">
						<thead>
							<tr>
								<th>Extension No</th>
								<th class="text-center">Current Status</th>
								<th class="text-right">Duration (sec)</th>
							</tr>
						</thead>
						<tbody id="extn_tbl_body" name="">
							
						</tbody>
					</table>
				</div>
			</div>			
		</div>
            <div class="col-sm-12">
                <div class="col-sm-1"></div>
                <div class="col-sm-11">
                    <h3 align="center">Real Time extension status</h3>
                    <div id="real_time_extn_status">
                    </div>
                </div>
                
            </div>
	</div>
</section>   
    </body>
</html>
