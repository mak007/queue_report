<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
<!--        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-timepicker-addon.css' ?>"/>
        -->        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/datatables/dataTables.bootstrap.css'; ?>"/>


        <link rel="stylesheet" href="<?php echo $css_path . 'css/ionicons.min.css.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/fullcalendar/fullcalendar.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/daterangepicker/daterangepicker-bs3.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>"/>


        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-timepicker-addon.js'; ?>"></script>
<!--        <script type="text/javascript" src="<?php echo $js_path . 'agents.js'; ?>"></script> -->


        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/raphael.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/fullcalendar/fullcalendar.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/daterangepicker/daterangepicker.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'dashboard_report.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'reports.js'; ?>"></script>
        <script src="<?php echo base_url('javascript/outbound_report_cdr.js'); ?>"></script>

        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>
    </head>
    <body class="skin-blue" onload='get_extensionList()'>		
        <aside class="right-side">  

            <div class="row">
                <br></br>    
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="box box-solid">
                        <div class="box-header">
                            <h3 class="box-title">
                                <placeholder> Outbound SLA Report  </placeholder>
                            </h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                            <div class="col-md-6">
                                                <label> Start Date:</label>
                                                <input type="date" name="inpFromDate1" id = "inpFromDate1">
                                            </div>
                                            <div class="col-md-6">
                                                <label>End Date:</label>
                                                <input type="date" name="inpToDate1" id = "inpToDate1">
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label id="extn_name">Extension List</label>
                                        <select multiple id="sla_extn_list" name="sla_extn_list" class="form-control" onchange="update_ip_excel();">
                                            <option value="0">Choose</option>
                                        </select>
                                    </div>
                                    <!--div class="col-md-4">
                                        <label>Ob Team</label>
                                        <select multiple id="oblist" name="oblist" class="form-control" multiple onchange="getOBteam();">
                                        </select>
                                    </div-->
<!--                                    <div class="col-md-4">
                                     <label id="extn_name">Queue Name</label>   
                                        <select class="form-control" id="queuename" name="queuename" multiple onchange="getData();"></select>
                                    </div>-->
                                </div>
                            </div>
                                                    <table class="table table-bordered table-striped dataTable">
                                                        <tr>
                                                            <td>Outbound SLA Report</td>
                                                            <td><button class="bt btn-info" onclick="ob_SLAreport();">Display Details</button></td>
                                                            <td><?php
                                                                $d1 = array(
                                                                    'name' => 'inpFromDate11',
                                                                    'id' => 'inpFromDate11',
                                                                    'value' => '',
                                                                    'maxlength' => '100',
                                                                    'size' => '50',
                                                                    'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                                );
                                                                $d2 = array(
                                                                    'name' => 'inpToDate11',
                                                                    'id' => 'inpToDate11',
                                                                    'value' => '',
                                                                    'maxlength' => '100',
                                                                    'size' => '50',
                                                                    'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                                );
                                                                $d3 = array(
                                                                    'name' => 'sla_extn_list1',
                                                                    'id' => 'sla_extn_list1',
                                                                    'value' => '',
                                                                    'maxlength' => '100',
                                                                    'size' => '50',
                                                                    'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                                );
                                                                /*$d4 = array(
                                                                    'name' => 'extensionList',
                                                                    'id' => 'extensionList',
                                                                    'value' => '',
                                                                    'maxlength' => '100',
                                                                    'size' => '50',
                                                                    'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                                );*/
                                                                echo form_open('c_report/generateExcelOB_slaReport/');
                                                                echo form_input($d1);
                                                                echo form_input($d2);
                                                                echo form_input($d3);
                                                                //echo form_input($d4);
                                                                '<br/>';
                                                                echo form_submit('Login', 'Generate Excel');
                                                                echo form_close();
                                                                ?></td>
                                                        </tr>
                                                    </table>
                                                    </div><!-- /.box-body -->




                                                    <table id="agent_OB_slaReport" class="table table-bordered ">
                                                        <thead style="background-color: rgb(249,249,249); color: white;">
                                                            <tr>
                                                                <th>Date</th>
                                                                <th>Queue Name</th>
                                                                <!--th>Extn No</th-->
                                                                <th>No. of Dials</th>
                                                                <th>Calls Connected</th>
                                                                <th>Connect %</th>
                                                                <th>Total Talk time</th>
                                                                <th>Avg Talk time</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="agent_OB_slaReportBODY">
                                                        </tbody>
                                                    </table>

                                                    </div><!-- /.box -->
                                                    </div><!-- nav-tabs-custom -->
                                                    </div><!-- /.col -->
                                                    <div class="col-xs-12">


                                                    </div>  

                                                    </div>           
                                                    </aside>   

                                                    <center><h3></h3></center>
                                                    <script type="text/javascript">
                                                        $(function () {
                                                            $("#Campaign").dataTable();
                                                            $("#Choose Campaign").dataTable();
                                                            $('#Campaign1').dataTable({
                                                                "bPaginate": true,
                                                                "bLengthChange": true,
                                                                "bFilter": true,
                                                                "bSort": true,
                                                                "bInfo": true,
                                                                "bAutoWidth": true
                                                            });
                                                        });
                                                    </script>
                                                    <div class="modal fade" id="transferDetails" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header" style="background-color: rgb(136, 210, 253);">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title"><i class="fa fa-user"></i>Transfer Call Details</h4>
                                                                </div>

                                                                <div class="modal-body" id="transferDetailsBody">
                                                                </div>	
                                                                <div class="modal-footer clearfix">
                                                                <!--button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button-->
                                                                </div>

                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div>

                                                    </body>
                                                    </html>