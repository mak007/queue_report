<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Real Time Monitoring</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
        <!--<link rel="stylesheet" href="<?php echo $css_path . 'report_telephony_custom.css'; ?>"/>-->
        <!--<link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>-->
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'hover-min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <!--<link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>-->
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>

        <link href="<?php echo $css_path . 'bootstrap.min.css'; ?>" rel="stylesheet" type="text/css" />
        <!--<link href="<?php //echo $css_path . 'telephony-RTM.css';   ?>" rel="stylesheet" type="text/css" />-->
        <link href="<?php echo $css_path . 'font-awesome.css'; ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo $css_path . 'hover-min.css'; ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo $css_path . 'telephony-RTM-New-UI.css'; ?>" rel="stylesheet" type="text/css" />
        <!--<link rel="stylesheet" href="<?php // echo $css_path . 'css/ionicons.min.css.css';      ?>"/>-->
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>"/>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>
        <script src="<?php echo $js_path . 'js/jquery-3.1.0.min.js' ?>"></script> 
        <script src="<?php echo $js_path . 'bootstrap.min.js' ?>"></script> 
        <script type="text/javascript" src="<?php echo $js_path . 'js/telephony-RTM.js' ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>        
        <!--<script src=<?php echo base_url('javascript/highcharts/highcharts.js'); ?>></script>-->
        <!--        <script src=<?php echo base_url('javascript/highcharts/highcharts-3d.js'); ?>></script>
                <script src=<?php echo base_url('javascript/highcharts/modules/exporting.js'); ?>></script>
                <script src=<?php echo base_url('javascript/highcharts/modules/highcharts-more.js'); ?>></script>
                <script src=<?php echo base_url('javascript/highcharts/modules/solid-gauge.js'); ?>></script>-->
                <!--<script src="https://code.highcharts.com/modules/exporting.js"></script>-->

        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'dashboard_report.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'telephony-RTM-New-UI.js'; ?>"></script>




        <style type="text/css">
            .highcharts-yaxis-grid .highcharts-grid-line {
                display: none;
            }
        </style>


<!--        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>-->
        <!-- Commited by mohan on 03-01-2017 because script written 2 times-->
<!--        <script>
            $(function () {
                $("#datepickerEndDate").datepicker({
                    format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });
        </script>-->
<!--        <script>
            $(function () {
                $("#datepickerStartDate").datepicker({
                    format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });


        </script> -->
<!--        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>-->
        <script>
            $(function () {
                $("#datepickerEndDate").datepicker({
                    format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });
        </script>
        <script>
            $(function () {
                $("#datepickerStartDate").datepicker({
                    format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });


        </script>
    </head><!--select_queue();init_rtm();select_queue1();-->
    <!--<body onload='display_pie_chart_data();'>-->
    <body onload='select_queue();init_rtm_v1();select_queue1();display_pie_chart_data();'>
        <!-- MAIN SECTION FOR TELEPHONY RTM HERE --> 
        <!--SECTION FOR TITLE BAR STARTS HERE--> 

        <!--SECTION FOR TITLE BAR ENDS HERE--> 

        <!--SECTION FOR AGENT TABLE STARTS HERE-->
        <section>
            <div class="col-sm-12 paddingZero-RTM autoResizeScreen-RTM">
                <div class="col-sm-6">
                    <section class="marginLeft-RTM-New fontSize-12-RTM-New-UI">
                        <div class="col-sm-12" style="padding:0;">
                            <div class="col-sm-6">
                                <label>Select RTM</label>
                                <select multiple id="select_queue_list" name="select_queue_list" class="form-control" style="padding:0px;width:170px; font-size:14px;height: 65px;">
                                    <option value="0">Choose</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 marginBottom-10-RTM-New paddingZero-RTM">
                            <div class="col-sm-6 paddingZero-RTM">
                                <div class="col-sm-7">
                                    <h6 class="pull-left"> Agent Staffed : </h6>
                                    <h6 class="pull-left"> <span>000</span> </h6>
                                </div>
                                <div class="col-sm-5">
                                    <h6 class="pull-left"> Skillset : </h6>
                                    <h6 class="pull-left"> <span>---</span> </h6>
                                </div>
                            </div>
                            <div class="col-sm-6 paddingZero-RTM">
                                <div class="col-sm-6">
                                    <select id="dropdown_sort" class="form-control fontSize-12-RTM-New-UI">
                                        <option value="ext">Extension No</option>
                                        <option value="sta">Status</option>
                                        <option value="que">Queue</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <select id="dropdown_order_by" class="form-control fontSize-12-RTM-New-UI">
                                        <option value="ASC">Ascending</option>
                                        <option value="DESC">Descending</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="table-responsive col-sm-11">
                        <table id="extn_tbl" class="table table-bordered table-striped table-hover scrollTable-RTM-New-UI">
                            <thead class="fontSize-12-RTM-New-UI">
                                <tr>
                                    <th>Extension No</th>
                                    <th class="text-center">Current Status</th>
                                    <th class="text-right">Duration (sec)</th>
                                    <th class="text-right">Queue</th>
                                </tr>
                            </thead>
                            <tbody id="extn_tbl_body" name="" class="fontSize-12-RTM-New-UI">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-6"> 
                    <!--				<div class="hideChartPannel-RTM-New-UI text-center pull-right">
                                                        <h5><span class="glyphicon glyphicon-chevron-right"></span></h5>
                                                </div>-->
                    <div class="col-sm-12 paddingZero-RTM">
                        <div class="col-sm-12 chart-window-RTM-New-UI paddingZero-RTM">
                            <div class="col-sm-3 pull-left chart-window-qStatus-RTM-New-UI">
                                <div class="chart-boxes-RTM-New-UI" title="Logged In">
                                    <div class="chart-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">Logged In</div>
                                    <div class="chart-boxes-time-RTM-New-UI pull-right text-center">
                                        <h4 class="marginZero-RTM paddingTop-RTM marginTop_5"><span id="ag_logged_in" class="label label-default">000</span></h4>
                                    </div>
                                </div>
                                <div class="chart-boxes-RTM-New-UI" title="On Call">
                                    <div class="chart-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">On Call</div>
                                    <div class="chart-boxes-time-RTM-New-UI pull-right text-center">
                                        <h4 class="marginZero-RTM paddingTop-RTM marginTop_5"><span id="ag_on_call" class="label label-primary">000</span></h4>
                                    </div>
                                </div>
                                <div class="chart-boxes-RTM-New-UI" title="Hold">
                                    <div class="chart-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">Hold</div>
                                    <div class="chart-boxes-time-RTM-New-UI pull-right text-center">
                                        <h4 class="marginZero-RTM paddingTop-RTM marginTop_5"><span id="ag_on_hold" class="label label-warning">000</span></h4>
                                    </div>
                                </div>
                                <div class="chart-boxes-RTM-New-UI" title="ACW">
                                    <div class="chart-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">ACW</div>
                                    <div class="chart-boxes-time-RTM-New-UI pull-right text-center">
                                        <h4 class="marginZero-RTM paddingTop-RTM marginTop_5"><span id="ag_on_acw" class="label label-info">000</span></h4>
                                    </div>
                                </div>
                                <div class="chart-boxes-RTM-New-UI" title="Pause">
                                    <div class="chart-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">Pause</div>
                                    <div class="chart-boxes-time-RTM-New-UI pull-right text-center">
                                        <h4 class="marginZero-RTM paddingTop-RTM marginTop_5"><span id="ag_on_pause" class="label label-default">000</span></h4>
                                    </div>
                                </div>
                                <div class="chart-boxes-RTM-New-UI" title="RNR">
                                    <div class="chart-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">RNR</div>
                                    <div class="chart-boxes-time-RTM-New-UI pull-right text-center">
                                        <h4 class="marginZero-RTM paddingTop-RTM marginTop_5"><span id="ag_on_rnr" class="label label-danger">000</span></h4>
                                    </div>
                                </div>
                                <div class="chart-boxes-RTM-New-UI" title="Available">
                                    <div class="chart-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">Avail</div>
                                    <div class="chart-boxes-time-RTM-New-UI pull-right text-center">
                                        <h4 class="marginZero-RTM paddingTop-RTM marginTop_5"><span id="ag_on_available" class="label label-success">000</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9 chart-window-chart-RTM-New-UI paddingZero-RTM">
                                <div class="col-sm-12 paddingZero-RTM"> 
                                    <!--  CHART WILL BE PLACED HERE --> 
                                    <!--                                    <div class="pull-left" id="div_3d_pie_chart">          
                    <div style="width: auto; height: auto; margin: 0 auto">
                        <div id="container-speed" style="width: 300px; height: 180px; float: left"></div>
                    </div>
                    <div style="width: auto; height: auto; margin: 0 auto">
                        <div id="container-rpm" style="width: 300px; height: 180px; float: left; clear: both;"></div>
                    </div>      
                </div>-->
                                    <div style="width: 100%; height: 180px; margin: 0 auto">
                                        <div id="container-speed" style="width: 50%; height: 180px; float: left"></div>
                                        <div id="container-rpm" style="width: 50%; height: 180px; float: left"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 paddingZero-RTM absoluteBottom-RTM-New-UI">
                                    <div class="marginTop-al-sl">
                                        <h4 class="text-center">--Day Level Performance--</h4>
                                        <div class="col-sm-6">
                                            <div class="al-sl-boxes-RTM-New-UI">
                                                <div class="al-sl-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">Offered</div>
                                                <div class="al-sl-boxes-time-RTM-New-UI pull-right text-center">
                                                    <h4><span class="label label-primary" id="calls_off_ftd_v1">00000</span></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="al-sl-boxes-RTM-New-UI">
                                                <div class="al-sl-boxes-headings-RTM-New-UI pull-left text-center">ATT</div>
                                                <div class="al-sl-boxes-time-RTM-New-UI pull-right text-center">
                                                    <h4><span class="label label-primary" id="att_ftd_v1">00000</span></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="al-sl-boxes-RTM-New-UI">
                                                <div class="al-sl-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">Answered</div>
                                                <div class="al-sl-boxes-time-RTM-New-UI pull-right text-center">
                                                    <h4><span class="label label-primary" id="calls_ans_ftd_v1">00000</span></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="al-sl-boxes-RTM-New-UI">
                                                <div class="al-sl-boxes-headings-RTM-New-UI pull-left text-center">Abandoned</div>
                                                <div class="al-sl-boxes-time-RTM-New-UI pull-right text-center">
                                                    <h4><span class="label label-primary" id="calls_abn_ftd_v1">00000</span></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 interval-window-RTM-New-UI">
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left ellipsis-RTM-New-UI text-center">Calls ans in SL</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-primary" id="call_in_sl_int">000</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Service level</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-primary" id="sl_int">00.00%</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Calls waiting</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-danger" id="call_wt_int">000</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Answered level</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-primary" id="call_al_int">00.00%</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Calls offered</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-primary" id="call_off_int">000</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Abandon level</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-primary" id="call_abnl_int">00.00%</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Calls answered</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-warning" id="call_ans_int">000</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Avg ATT</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-info" id="att_int">00:30</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Calls abandon</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-danger" id="abn_int">000</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="interval-boxes-RTM-New-UI">
                                    <div class="interval-boxes-headings-RTM-New-UI pull-left text-center">Oldest call waiting</div>
                                    <div class="interval-boxes-time-RTM-New-UI marginTop--7-RTM-New-UI text-center">
                                        <h4><span class="label label-danger" id="ocw_int">00:00</span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--SECTION FOR AGENT TABLE ENDS HERE--> 

        <!-- MAIN SECTION FOR TELEPHONY RTM ENDS HERE --> 

        <!-- MAIN SECTION FOR TELEPHONY RTM ENDS HERE -->
    </body>
</html>
