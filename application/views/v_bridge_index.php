<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Queue Report</title>
        <!-- ALL CSS STYLES HERE -->
        <link rel="stylesheet" href="<?php echo base_url('css/queue_reports_styles.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css'); ?>"/>
    </head>
    <!-- QUEUE REPORT SECTION STARTS HERE -->
    <section>
        <!-- LOGIN PAGE STARTS HERE -->
        <div id="queue-report-login-page-div" class="col-sm-12">
            <div class="col-lg-4"></div>
            <div class="col-sm-4">
                <div class="queue-report-login-div">
                    <h3 class="text-center bottom-border-headings">-  Queue Reports Login  -</h3><br>
                    <div class="form-group">
                        <label>Username:</label>
                        <input type="text" class="form-control" placeholder="Username" required="" id="bridge_username" name="bridge_username">
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input type="password" class="form-control" placeholder="Password" required="" id="bridge_password" name="bridge_password">
                    </div>
                    <h3 class="bottom-border-headings"></h3>
                    <button id="queue-report-login-btn" class="btn btn-lg btn-success center-block" onclick="bridge_login();">Login  <small><span class="glyphicon glyphicon-log-in"></span></small></button>
                </div>
            </div>
        </div>
        <!-- LOGIN PAGE ENDS HERE -->

        <!-- BRIDEGE MENU PAGE STARTS HERE -->
        <div id="queue-report-bridge-menu-page-div" class="col-sm-12">
            <div class="container-fluid">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#queue-report-reset-password">Reset Bridge Password</a></li>
                    <li><a data-toggle="tab" href="#queue-report-add-bridge-DID">Add Bridge DID</a></li>
                    <li><a data-toggle="tab" href="#queue-report-add-contact" onclick="get_did_number();">Add Contact</a></li>
                    <li><a data-toggle="tab" href="#queue-report-bridge-panel" onclick="get_did_number();getDidContact();">Bridge Panel</a></li>
                </ul>

                <div class="tab-content">
                    <div id="queue-report-reset-password" class="tab-pane fade in active">
                        <!-- RESET PAGE STARTS HERE -->
                        <div class="col-sm-12">
                            <div class="col-lg-4"></div>
                            <div class="col-sm-4">
                                <div class="queue-report-login-div">
                                    <h4 class="text-center bottom-border-headings">-  Reset Bridge Password  -</h4><br>
                                    <div class="form-group">
                                        <label>Select DID Number:</label>
                                        <select class="form-control" id="bridge_did_numr" name="bridge_did_numr">
                                            <option value="0">-- Choose -- </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Old Password:</label>
                                        <input type="password" class="form-control" required="" id="bridge_old_password" name="bridge_old_password">
                                    </div>
                                    <div class="form-group">
                                        <label>New Password:</label>
                                        <input type="password" class="form-control" required="" id="bridge_new_password" name="bridge_new_password">
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password:</label>
                                        <input type="password" class="form-control" required="" id="bridge_confirm_password" name="bridge_confirm_password">
                                        <h4 class="text-center" id='errorMessage'></h4>
                                    </div>
                                    <!--<h3 class="bottom-border-headings"></h3>-->
                                    <button onclick="bridge_reset_pass();" id="queue-report-reset-btn" class="btn btn-lg btn-danger center-block"> Reset  <small><span class="glyphicon glyphicon-repeat"></span></small></button>
                                </div>
                            </div>
                        </div>
                        <!-- RESET PAGE ENDS HERE -->
                    </div>
                    <div id="queue-report-add-bridge-DID" class="tab-pane fade">
                        <!-- ADD DID NUMBER PAGE STARTS HERE -->
                        <div class="col-sm-12">
                            <div class="col-lg-4"></div>
                            <div class="col-sm-4">
                                <div class="queue-report-login-div">
                                    <h4 class="text-center bottom-border-headings">-  Add DID Number  -</h4><br>
                                    <div class="form-group">
                                        <label>Conference Name:</label>
                                        <input type="text" class="form-control" id="conf_name" name="conf_name" placeholder="Enter Conference Name"/>
                                    </div>
                                    <div class="form-group">
                                        <label>DID:</label>
                                        <input type="text" class="form-control" id="did" name="did" placeholder="Enter DID No"/>
                                    </div>
                                    <h3 class="bottom-border-headings"></h3>
                                        <button id="queue-report-add-DID-btn" class="btn btn-lg btn-primary center-block" onclick="save_addDid();">Add DID  <small><span class="glyphicon glyphicon-plus"></span></small></button>
                                </div>
                            </div>
                        </div>
                        <!-- ADD DID NUMBER PAGE ENDS HERE -->
                    </div>
                    <div id="queue-report-add-contact" class="tab-pane fade">
                        <!-- ADD CONTACT PAGE STARTS HERE -->
                        <div class="col-sm-12">
                            <div class="col-lg-4"></div>
                            <div class="col-sm-4">
                                <div class="queue-report-login-div">
                                    <h4 class="text-center bottom-border-headings">-  Add Contact  -</h4><br>
                                    <div class="form-group">
                                        <label>Select DID Number:</label>
                                        <select id="select_didno_list" name="select_didno_list" class="form-control">
                                            <option value="0"> -- Choose --</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Contact Number:</label>
                                        <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Enter Contact No">
                                    </div>
                                    <h3 class="bottom-border-headings"></h3>
                                        <button id="queue-report-add-contact-btn" class="btn btn-lg btn-primary center-block" onclick="save_addContact();">Add Contact  <small><span class="glyphicon glyphicon-plus"></span></small></button>
                                </div>
                            </div>
                        </div>
                        <!-- ADD CONTACT PAGE ENDS HERE -->
                    </div>
                    <div id="queue-report-bridge-panel" class="tab-pane fade">
                        <!-- BRIDGE PANEL PAGE STARTS HERE -->
                        <div class="col-sm-12">
                            <div class="col-lg-1"></div>
                            <div class="col-sm-10">
                                <div class="queue-report-login-div">
                                    <h4 class="text-center bottom-border-headings">-  Bridge Control Panel  -</h4><br>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            <div class="col-sm-3">
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Select DID Number:</label>
                                                    <select onchange="getDidContact();" class="form-control" id="select_didno_list_panel" name="select_didno_list_panel">
                                                        <option value="0">-- Choose --</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 queue-report-pannel-contact-list border-all-side well">
                                            <div class="col-sm-12 text-center">
                                                <label>- Contact Numbers -</label>
                                            </div>
                                            <div id="displayContactNumber">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <h3 class="bottom-border-headings"></h3>
                                    </div>
                                    <br>
                                        <button id="queue-report-conference-btn" onclick="initiate_conference();" name="call_conference" class="btn btn-lg btn-primary center-block">Initiate Conference  <small><span class="glyphicon glyphicon-volume-up"></span></small></button>
                                </div>
                            </div>
                        </div>
                        <!-- BRIDGE PANEL PAGE ENDS HERE -->
                    </div>
                </div>
            </div>
        </div>
        <!-- BRIDEGE MENU PAGE ENDS HERE -->
    </section>
    <!-- QUEUE REPORT SECTION ENDS HERE -->
    <body>
        <!-- ALL JAVASCRIPT FILES HERE -->
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript" src="<?php echo base_url('javascript/javaScript_3.2.1.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('bootstrap/js/bootstrap.min.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('javascript/queue_report_javaScript.js')?>"></script>
        <script type="text/javascript" src="<?php echo base_url('javascript/bridge.js')?>"></script>
    </body>
</html>