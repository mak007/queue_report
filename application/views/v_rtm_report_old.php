<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'report_telephony_custom.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'hover-min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <!--<link rel="stylesheet" href="<?php // echo $css_path . 'css/ionicons.min.css.css';    ?>"/>-->
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>"/>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>
        
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
        
        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
            <script src=<?php echo base_url('javascript/highcharts/highcharts.js'); ?>></script>
        <!--<script src="https://code.highcharts.com/modules/exporting.js"></script>-->
        <script type="text/javascript" src="<?php echo $js_path . 'dashboard_report.js'; ?>"></script>


        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>
        <!-- Commited by mohan on 03-01-2017 because script written 2 times-->
<!--        <script>
            $(function () {
                $("#datepickerEndDate").datepicker({
                    format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });
        </script>-->
<!--        <script>
            $(function () {
                $("#datepickerStartDate").datepicker({
                    format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });


        </script> -->
        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>
        <script>
            $(function () {
                $("#datepickerEndDate").datepicker({
                    format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });
        </script>
        <script>
            $(function () {
                $("#datepickerStartDate").datepicker({
                    format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datepicker('hide');

                });

            });


        </script>
    </head><!--select_queue();-->
    <body class="skin-blue" onload='init_rtm();select_queue1();'>
        <aside class="right-side">  
            <div class="row"><br><br>
                <div class="col-md-12">
                <div class="col-md-6">
                    <h3 style="margin-left: 16px;">RTM</h3>
                </div>
                </div>
                <div class="col-md-12">
                <div class="col-sm-6">
                    <select multiple id="select_queue_list" name="select_queue_list" class="form-control" style="padding:0px;width:170px; height: 65px;">
                        <option value="0">Choose</option>
                    </select>
                </div>
                </div>
            </div>
        </aside>
<!--        <div id="cdr_report">
            <aside class="right-side">  
                <div class="container-fluid" style="margin:0 15px; background-color:#fff; box-shadow:0px 0px 5px #777; padding:15px;overflow-y:auto;  ">
                    <div class="row">
                        <div class="col-md-12">

                            <table id="tableOutboundReport" class="table table-bordered ">
                                <thead style="background-color: rgb(249,249,249); color: white;">
                                    <tr>
                                        <th>Call date</th>
                                        <th>Extension No</th>
                                        <th>Phone No.</th>
                                        <th>Duration</th>
                                        <th>Bill Sec.</th>
                                        <th>Disposition</th>
                                        <th>Recording File</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_report">
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </aside>
        </div>-->
        <div style="margin-top: 10px;">
            <aside class="right-side">  
                <div class="container-fluid" style="margin:0 15px; background-color:#fff; box-shadow:0px 0px 5px #777; padding:15px;overflow-y:auto;  ">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-6" id="div_rtm">
                                <table class="table-bordered" style="width:100%;">
                                    <thead style="background-color: rgb(91,171,223); color: white !important;">
                                        <tr>
                                            <th>Logged In</th>
                                            <th>Available</th>
                                            <th>Oncall</th>
                                            <th>Other Queue</th>
                                            <th>Pause</th>
                                            <th>ACW</th>
                                            <th>Calls in queue</th>
                                        </tr>
                                    </thead>
                                    <tbody id="rtm_tbl_body">
                                        <tr>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-4" id="div_Extnrtm">
                                <table id="extn_tbl" class="table-bordered" style="width:100%;">
                                    <thead style="background-color: rgb(91,171,223); color: white !important;">
                                        <tr>
                                            <th>Extension No</th>
                                            <th>Current Status</th>
                                            <th>Duration (sec)</th>
                                        </tr>
                                    </thead>
                                    <tbody id="extn_tbl_body">
                                        <tr>
                                            <td> - </td>
                                            <td> - </td>
                                            <td> - </td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
<!--                            <script>
                                $("#extn_tbl").DataTable({
                                    "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]] ,
                                });
                            </script>-->
                            <div class="col-md-1"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </body>
</html>
