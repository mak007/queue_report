<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
<!--        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-timepicker-addon.css' ?>"/>
        -->        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/datatables/dataTables.bootstrap.css'; ?>"/>


        <link rel="stylesheet" href="<?php echo $css_path . 'css/ionicons.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/fullcalendar/fullcalendar.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/daterangepicker/daterangepicker-bs3.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>"/>


        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-timepicker-addon.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/raphael.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/fullcalendar/fullcalendar.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/daterangepicker/daterangepicker.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'dashboard_report.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'admin.js'; ?>"></script>


        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
            i { cursor: pointer; cursor: hand; }
        </style>

        <script type="text/javascript" src="<?php echo $js_path . 'outbound_report_cdr.js'; ?>"></script>
        <script type="text/javascript">
            function getCampaignsList() {
                $.ajax({
                    url: BASEPATH + 'c_import_test/getCampaignsList',
                    success: function (msg) {
                        $('#tdCampaign').html(msg);
                    }
                })


            }

            function getCampaignID() {
                var val = $('#selectCampaign option:selected').val();
                if (val > 0) {
                    $('#campaign_id').val(val);
                }
            }


        </script>
    </head>
    <body class="skin-blue" onload='select_didno();getReportsConfig();getQueuename();getObTeam();'>

        <br><br>
                <aside class="right-side"> 	
                    <div class="content-wrapper">
                        <section class="content">
                            <div class="row">
                                <div class="col-md-8" style="width: 80%;">
                                    <!-- Custom Tabs -->
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li><a href="#tab_2" data-toggle="tab">Bridge Control Panel</a></li>
                                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                                        </ul>
                                        <div class="tab-content" style="">
                                            <div class="tab-pane active" id="tab_1">
                                                    <div class="box-body">
                                                        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                                            <tr>
                                                                <td> DID NO</td>
                                                                <td>
                                                                <select id="select_didno_list" name="select_didno_list" class="form-control" onchange="getDidContact(this);">

                                                                    <option value="0">Choose</option>
                                                                </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Contact No</td>
                                                                <td><div class="col-md-6" id="displayContact">
                                                                    </div></td>
                                                            </tr>
                                                            
                                                        </table>
                                                    </div>

<!--                                                    <div class="box-footer">
                                                        <button type="button" class="btn btn-primary btn-flat btn-block" onclick="save_addContact();">Add</button>
                                                    </div>-->
                                            </div><!-- /.tab-pane -->
                                        </div><!-- /.tab-content -->
                                    </div><!-- nav-tabs-custom -->
                                </div>
                            </div>   <!-- /.row -->
                        </section><!-- /.content -->
                    </div>
                </aside>      
                <center><h3></h3></center>
                <script type="text/javascript">
                    $(function () {
                        $("#Campaign").dataTable();
                        $("#Choose Campaign").dataTable();
                        $('#Campaign1').dataTable({
                            "bPaginate": true,
                            "bLengthChange": true,
                            "bFilter": true,
                            "bSort": true,
                            "bInfo": true,
                            "bAutoWidth": true
                        });
                    });
                </script>


                </body>
                </html>
