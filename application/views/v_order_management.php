<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Raw Pressery</title>
       <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>";
        </script>
    <link rel="shortcut icon" href="<?php echo $images_path . 'favicon1.ico' ;    ?>">
	<script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css' ; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css' ; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css' ; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css' ; ?>"/>
		 <link rel="stylesheet" href="<?php echo $css_path . 'css/datatables/dataTables.bootstrap.css' ; ?>"/>

   
        <link rel="stylesheet" href="<?php echo $css_path . 'css/ionicons.min.css.css' ; ?>"/>

        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css' ; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/fullcalendar/fullcalendar.css' ; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/daterangepicker/daterangepicker-bs3.css' ; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css' ; ?>"/>
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-timepicker-addon.js'; ?>"></script>
		
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/fullcalendar/fullcalendar.min.js'; ?>"></script>
		
	<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/daterangepicker/daterangepicker.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
	<script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
<!--	<script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script> 
		
		<script type="text/javascript" src="<?php echo $js_path . 'ss3.js'; ?>"></script>-->
        <script type="text/javascript" src="<?php echo $js_path . 'admin.js'; ?>"></script>
		 
		
       
	 <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }
           
            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
			.form-control {
    display: block;
    width: 40%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857;
    color: #555;
    vertical-align: middle;
    background-color: #FFF;
    background-image: none;
    border: 1px solid #CCC;
    border-radius: 4px;
	}
	.box {
    position: relative;
    background: none repeat scroll 0% 0% #FFF;
    border-top: 2px solid #C1C1C1;
    margin-bottom: 20px;
	margin-top: 20px;
	margin-left: 4px;
    border-radius: 3px;
    width: 100%;
    box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
}
        </style>
        <script type="text/javascript">
        function createCommittmentOrders(){
        $('#imgloadData').fadeIn();    
        $.ajax({
                url: BASEPATH + 'c_test_orders/importRCOrders',
                success: function(msg){
                    $('#showData').html(msg);
                    $('#imgloadData').fadeOut();    
                },
                error: function() {
                    alert('Error');
                    $('#imgloadData').fadeOut();    
                }
            })
            
            
            
        }
        </script>
        
    </head>
    <body class="skin-blue" onload=''>
	<header  class="header">
            
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
				
                    <ul class="nav navbar-nav">
                       
                      <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                
                                <span><i class="caret"></i></span>
								
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->

                                 Menu Body -->
                              
                                <!-- Menu Footer-->
                                
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
		
		
		
		
			 <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                              </div>
                        <div class="pull-left info">
                           

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                       
						
		 	<li>
			<a href="<?php echo site_url('c_login/login') ;    ?>">
                            <i class="fa fa-calendar"></i> <span>Home</span>

                            </a>
                        </li>
                        
                       
             				
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
			
			
	<aside class="right-side">  
		<div class="row">
			
			<div class="col-md-10">
							<div class="box box-primary">
                                <div class="box-header" data-toggle="tooltip" title="Header tooltip">
                                    <h3 class="box-title">Report Archive</h3>
                                    <br/>
                                        <div class="box-tools pull-right" style="width: 100%; height: 20px;">
                                        <label for="inpDateApproval">Choose Start and End Date</label>
										
										<table border="0">
											<tr>
											<td><form id="formDateUnpaid"><input type="date" style="border: 1px solid #c1c1c1; width: 120px;" id="inpDateApproval" name="inpDateApproval" placeholder="Enter Start date"/></td>
											<td><input type="date" style="border: 1px solid #c1c1c1; width: 120px;" id="inpEndDate1" name="inpEndDate1" placeholder="Enter End date"/></form></td>
											
											</tr>
										</table>	
                                                
												
                                        
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        
                                    </div>
                                    
                                       
                                    </div>
                                </div>
                                <div class="box-body">
                                  <div id="divOuterArchive" style="z-index: -1;">
                                <div id="divArchiveTitle"></div>
								<div align="top">
									<?php
													$d1 = array(
                                                        'name' => 'inpLogdate',
                                                        'id' => 'inpLogdate',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													$d2 = array(
                                                        'name' => 'inpEnddate',
                                                        'id' => 'inpEnddate',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													
													$d3 = array(
                                                        'name' => 'inpReportStartDate',
                                                        'id' => 'inpReportStartDate',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													$d4 = array(
                                                        'name' => 'inpReportEndDate',
                                                        'id' => 'inpReportEndDate',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													$d5 = array(
                                                        'name' => 'inpUserLogStartDate',
                                                        'id' => 'inpUserLogStartDate',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													$d6 = array(
                                                        'name' => 'inpUsersLogEndDate',
                                                        'id' => 'inpUsersLogEndDate',
                                                        'value' => '',
                                                        'maxlength' => '100',
                                                        'size' => '50',
                                                        'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                                    );
													'<table style="top-margin :10px;"><tr><td>';
													echo form_open('c_admin/firstTimeCalledReportExcel/');
                                                    echo form_input($d1);
													echo form_input($d2);
                                                    '<br/>';
                                                    echo form_submit('Login','GenrateExcel');
                                                    echo form_close();
													'</td></tr></table>';
													
													echo form_open('c_admin/agentReportExcel/');
                                                    echo form_input($d3);
													echo form_input($d4);
                                                    '<br/>';
                                                    echo form_submit('Login','Agent Report');
                                                    echo form_close();
													
													//echo form_open('c_admin/userLogExcel/');
                                                //    echo form_input($d5);
												//	echo form_input($d6);
                                                    '<br/>';
                                                 //   echo form_submit('Login','User Log Report');
                                                 //   echo form_close();
													
													
													
													
                                                    ?>
													</div>
                                <div id="divArchive" style="margin-left: auto; margin-right: auto; overflow: auto; overflow-x: none; overflow-y: auto;border: 1px solid #c1c1c1;"align="bottom"></div>
								
						  </div>
                                </div><!-- /.box-body -->
                                <div class="box-footer">
                                   
                                </div><!-- /.box-footer-->
                            </div><!-- /.box -->
                           
                            
                        </div>
                </div>
            
         
        </aside>   
      
        <center><h3></h3></center>
       
		
    </body>
</html>