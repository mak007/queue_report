<body class="skin-blue sidebar-collapse sidebar-mini">
    <!--<div class="wrapper">-->
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo" style="background: transparent linear-gradient(to bottom, rgb(175, 221, 249) 0%, rgb(127, 208, 254) 100%) repeat scroll 0% 0%;height:51px;">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?php echo base_url('images/minilogo.png'); ?>" style="height: 40px;"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"> <img src="<?php echo base_url('images/logo-mobile.png'); ?>" style="height: 40px;"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation" style="background: transparent linear-gradient(to bottom, rgb(175, 221, 249) 0%, rgb(127, 208, 254) 100%) repeat scroll 0% 0%;">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- Messages: style can be found in dropdown.less-->

                    <!-- Notifications: style can be found in dropdown.less -->

                    <!-- Tasks: style can be found in dropdown.less -->

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                            <span class="hidden-xs">Supervisor</span>
                        </a>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    <!--                        <li>
                                                <a href="<?php // echo base_url('c_account/logout/' . $sessionId . '/' . $daily_sessions_id);   ?>">Sign out</a>
                                            </li>-->
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar" style="background: transparent linear-gradient(to bottom, rgb(175, 221, 249) 0%, rgb(127, 208, 254) 100%) repeat scroll 0% 0%;height:auto;">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <!--<div class="user-panel">
                <div class="pull-left image">

                </div>
            </div>-->
            <ul class="sidebar-menu">
                <li style="background-color: rgb(240, 244, 246);" class="header">MAIN NAVIGATION</li>
                <li style="background-color: rgb(240, 244, 246);">
                    <a href="<?php echo base_url('c_report/index'); ?>">
                       <i class="fa fa-home"></i> <span>Home </span></a></li>
                <li style="background-color: rgb(240, 244, 246);" class="treeview">
                    <a href="#"><i class="fa fa-fw fa-level-down"></i>
                    <span>Inbound Reports</span></a>
                    <span class="label label-primary pull-right"></span>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo base_url('c_report/day_wise_sl'); ?>">
                                <i class="fa fa-circle-o"></i>Day Wise SL
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('c_report/each_call_details'); ?>"><i class="fa fa-circle-o"></i>Each Call Details
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('c_report/intra_day_sl'); ?>"><i class="fa fa-circle-o"></i>Intra Day SL
                            </a>
                        </li>
                        <!--li>
                            <a href="<?php //echo base_url('c_report/cdr_excel');  ?>"><i class="fa fa-circle-o"></i>CDR Report
                            </a>
                         </li-->
                    </ul>
                    </li>
                <li style="background-color: rgb(240, 244, 246);" class="treeview">
                    <a href="#"><i class="fa fa-fw fa-level-up"></i>
                        <span>Outbound Reports</span></a>
                    <span class="label label-primary pull-right"></span>
                    <ul class="treeview-menu">
                        <li>
                    <a  href="<?php echo base_url('c_outbound_report_cdr/index'); ?>">
                        <i class="fa fa-circle-o"></i><span>Outbound Report </span></a>
                        </li>
                        <li style="background-color: rgb(240, 244, 246);">
                           <a  href="<?php echo base_url('c_report/ob_agent_wise'); ?>">
                               <i class="fa fa-circle-o"></i><span>Outbound Agent Wise </span></a>
                       </li>
                       <li style="background-color: rgb(240, 244, 246);">
                           <a  href="<?php echo base_url('c_report/ob_sla_report'); ?>">
                               <i class="fa fa-circle-o"></i><span>Outbound SLA Report </span></a>
                       </li>
                    </ul>
                </li>
<!--                <li style="background-color: rgb(240, 244, 246);">
                    <a  href="<?php echo base_url('c_report/ob_agent_wise'); ?>">
                        <i class="fa fa-fw fa-level-up"></i><span>Outbound Agent Wise </span></a>
                </li>-->
                <li style="background-color: rgb(240, 244, 246);">
                    
                    <a href="#"><i class="fa fa-fw fa-level-up"></i>
                        <span>RTM</span></a>
                    <span class="label label-primary pull-right"></span>
                    <ul class="treeview-menu">
<!--                        <li style="background-color: rgb(240, 244, 246);">
                            <a  href="<?php echo base_url('c_dashboard_report/rtm_report'); ?>">
                                <i class="fa fa-info"></i>  <span>RTM</span>
                            </a>
                        </li>-->
                        <li style="background-color: rgb(240, 244, 246);">
                            <a  href="<?php echo base_url('c_dashboard_report/rtm_report_v1'); ?>">
                                <i class="fa fa-info"></i>  <span>RTM</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li style="background-color: rgb(240, 244, 246);">
                    <a  href="<?php echo base_url('c_outbound_report_cdr/extra_report'); ?>">
                        <i class="fa fa-info"></i>  <span>Extra Reports </span>
                    </a>
                </li>
                <li style="background-color: rgb(240, 244, 246);">
                    <a  href="<?php echo base_url('c_report/report_config'); ?>">
                        <i class="fa fa-gear"></i> <span>Report Configuration</span>
                    </a>
                </li>
                <li style="background-color: rgb(240, 244, 246);">
                    <a  href="<?php echo base_url('c_report/bridge_login'); ?>">
                        <i class="fa fa-sign-in"></i> <span>Bridge Login</span>
                    </a>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
