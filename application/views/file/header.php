<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Enhancier Pro</title>
    
     <link rel="shortcut icon" href="<?php echo base_url('images/enhancier24.ico') ;?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url('media/bootstrap/css/bootstrap.min.css')?>">
    <!-- Font Awesome -->

    <!-- Font Awesome -->

    <link rel="stylesheet" href="<?php echo base_url('media/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/font-awesome.min.css')?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url('media/ionicons.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('css/ionicons.min.css')?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('media/dist/css/AdminLTE.min.css')?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('media/dist/css/skins/_all-skins.min.css')?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('media/plugins/iCheck/flat/blue.css')?>">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo base_url('media/plugins/morris/morris.css')?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url('media/plugins/jvectormap/jquery-jvectormap-1.2.2.css')?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url('media/plugins/datepicker/datepicker3.css')?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url('media/plugins/daterangepicker/daterangepicker-bs3.css')?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url('media/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>">
	<!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url('javascript/html5shiv.min.js')?>"></script>
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <script type="text/javascript" src="<?php echo base_url('javascript/respond.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('javascript/ckeditor/ckeditor.js'); ?>"></script>
  
  </head>
