<body class="skin-blue sidebar-collapse sidebar-mini">
    <!--<div class="wrapper">-->
    <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo" style="background: transparent linear-gradient(to bottom, rgb(175, 221, 249) 0%, rgb(127, 208, 254) 100%) repeat scroll 0% 0%;height:51px;">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?php echo base_url('images/minilogo.png'); ?>" style="height: 40px;"></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"> <img src="<?php echo base_url('images/logo-mobile.png'); ?>" style="height: 40px;"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation" style="background: transparent linear-gradient(to bottom, rgb(175, 221, 249) 0%, rgb(127, 208, 254) 100%) repeat scroll 0% 0%;">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs">Supervisor</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar" style="background: transparent linear-gradient(to bottom, rgb(175, 221, 249) 0%, rgb(127, 208, 254) 100%) repeat scroll 0% 0%;height:auto;">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li style="background-color: rgb(240, 244, 246);" class="header">MAIN NAVIGATION</li>
                <li style="background-color: rgb(240, 244, 246);">
                    <?php
                    if($this->uri->segment(2)=='bridge_reset_view'){
                    ?>
       
                    <a href="<?php echo base_url('c_report/bridge_menu'); ?>">
                    <i class="fa fa-home"></i> <span>Home </span></a>

                    <?php }else if($this->uri->segment(2)=='display_did_form'){ ?>  
                    
                    <a href="<?php echo base_url('c_report/bridge_menu'); ?>">
                    <i class="fa fa-home"></i> <span>Home </span></a>
                    <?php }else if($this->uri->segment(2)=='display_contact_form'){ ?>  
                    
                    <a href="<?php echo base_url('c_report/bridge_menu'); ?>">
                    <i class="fa fa-home"></i> <span>Home </span></a>
                    <?php }else if($this->uri->segment(2)=='display_bridge_controlPanel'){ ?>  
                    
                    <a href="<?php echo base_url('c_report/bridge_menu'); ?>">
                    <i class="fa fa-home"></i> <span>Home </span></a>
                    <?php }else if($this->uri->segment(2)=='bridge_menu'){ ?>  
                    
                    <a href="<?php echo base_url('c_report/bridge_login'); ?>">
                    <i class="fa fa-home"></i> <span>Home </span></a>
                    <?php } ?>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
