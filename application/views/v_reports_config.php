<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
<!--        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-timepicker-addon.css' ?>"/>
        -->        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/datatables/dataTables.bootstrap.css'; ?>"/>


        <link rel="stylesheet" href="<?php echo $css_path . 'css/ionicons.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/fullcalendar/fullcalendar.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/daterangepicker/daterangepicker-bs3.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'; ?>"/>


        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-timepicker-addon.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/raphael.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/fullcalendar/fullcalendar.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/daterangepicker/daterangepicker.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/jquery.dataTables.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/datatables/dataTables.bootstrap.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'dashboard_report.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'admin.js'; ?>"></script>


        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>

        <script type="text/javascript" src="<?php echo $js_path . 'outbound_report_cdr.js'; ?>"></script>
        <script type="text/javascript">
            function getCampaignsList() {
                $.ajax({
                    url: BASEPATH + 'c_import_test/getCampaignsList',
                    success: function (msg) {
                        $('#tdCampaign').html(msg);
                    }
                })


            }

            function getCampaignID() {
                var val = $('#selectCampaign option:selected').val();
                if (val > 0) {
                    $('#campaign_id').val(val);
                }
            }


        </script>
    </head>
    <body class="skin-blue" onload='getReportsConfig();getQueuename();getObTeam();'>

        <br><br>
                <aside class="right-side"> 	
                    <div class="content-wrapper">
                        <section class="content">
                            <div class="row">
                                <div class="col-md-8" style="width: 80%;">
                                    <!-- Custom Tabs -->
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li><a href="#tab_2" data-toggle="tab">General Setting</a></li>
                                            <li class="active"><a href="#tab_1" data-toggle="tab">Configure User</a></li>
                                            <li onclick="getUserList();"><a href="#tab_3" data-toggle="tab">View / Edit User</a></li>
                                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                                        </ul>
                                        <div class="tab-content" style="">
                                            <div class="tab-pane active" id="tab_1">
                                                <form id="report_addUser" name="report_addUser" role="form">
                                                    <div class="box-body">
                                                        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                                            <tr>
                                                                <td>Agent Name</td>
                                                                <td> <input type="text" class="col-md-12" id="agent_name" name="agent_name" placeholder="Enter Agent Name"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Extension</td>
                                                                <td><input type="text" class="col-md-12" id="extn" name="extn" placeholder="Enter Extension"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Outbound Team</td>
                                                                <td><select class="form-control pull-left" id="oblist" name="oblist"></select></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Queue</td>
                                                                <td><select class="form-control pull-left" id="queuename" name="queuename">
                                                                        <option value="0">--choose--</option>    
                                                                </select></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div class="box-footer">
                                                        <button type="button" class="btn btn-primary btn-flat btn-block" onclick="save_addUser();">Add User</button>
                                                    </div>
                                                </form>
                                            </div><!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_2">
                                                <form id="report_config_form" name="report_config_form" role="form">
                                                    <div class="box-body">
                                                        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                                            <tr>
                                                                <td>GMT offset</td>
                                                                <td> <input type="text" class="col-md-12" id="gmt_offset" name="gmt_offset" placeholder="Enter GMT Offset"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>SL threshold</td>
                                                                <td><input type="text" class="col-md-12" id="sl_threshold" name="sl_threshold" placeholder="Enter Service Level Threshold" required></td>
                                                            </tr>
                                                            <tr>
                                                                <td>SA threshold</td>
                                                                <td><input type="text" class="col-md-12" id="sa_threshold" name="sa_threshold" placeholder="Short Abandon Threshold"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Refresh Rate</td>
                                                                <td><input type="text" class="col-md-12" id="refresh_rate" name="refresh_rate" placeholder="RTM Refresh Rate (In Seconds)"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>DID Number</td>
                                                                <td><input type="text" class="col-md-12" id="did_no" name="did_no" placeholder="Enter DID Number"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Off Hours Start Time</td>
                                                                <td><input type="text" class="col-md-12" id="off_time_start" name="off_time_start" placeholder="Enter Off Office Hours Start Time"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Off Hours End Time</td>
                                                                <td><input type="text" class="col-md-12" id="off_time_end" name="off_time_end" placeholder="Enter Off Office Hours End Time"></td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div class="box-footer">
                                                        <button type="button" class="btn btn-primary btn-flat btn-block" onclick="save_reports_config();">Update</button>
                                                    </div>
                                                </form>
                                            </div><!-- /.tab-pane -->
                                            <div class="tab-pane" id="tab_3">
                                                <table id="tbodyUser">
                                                    <thead>
                                                        <th>Queue Name</th>
                                                        <th>Agent Name</th>
                                                        <th>Extension</th>
                                                        <th>Team</th>
                                                        <th>Action</th>
                                                    </thead>
                                                    <tbody id="UserList">
                                                        
                                                    </tbody>
                                                </table>
                                            </div><!-- /.tab-pane -->
                                        </div><!-- /.tab-content -->
                                    </div><!-- nav-tabs-custom -->
                                </div>











                            </div>   <!-- /.row -->
                        </section><!-- /.content -->
                    </div>
                </aside>      
                <center><h3></h3></center>
                <script type="text/javascript">
                    $(function () {
                        $("#Campaign").dataTable();
                        $("#Choose Campaign").dataTable();
                        $('#Campaign1').dataTable({
                            "bPaginate": true,
                            "bLengthChange": true,
                            "bFilter": true,
                            "bSort": true,
                            "bInfo": true,
                            "bAutoWidth": true
                        });
                    });
                </script>


                </body>
                </html>
