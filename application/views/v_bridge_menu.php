<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript" src="<?php echo base_url('javascript/jquery-1.10.js');?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('css/jquery-ui-1.10.4.custom.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/css/font-awesome.min.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/css/AdminLTE.css'); ?>"/>
        <link rel="stylesheet" href="<?php echo base_url('css/bridge_login.css');?>">
        <script type="text/javascript" src="<?php echo base_url('javascript/js/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('javascript/js/AdminLTE/app.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('javascript/bridge_login.js')?>"></script>
        <style>.small-box {
    border-radius: 30px;
    height: 100px;
    font-size: larger;
}
     .small-box  .inner {
    padding: 30px;
}
     </style>
    </head> 
<body>
        <div class="row" style="margin-left: 57px;margin-top: 51px;">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box " style="background-color:#8064A2;">
                    <div class="inner">

                        <p> <a style="color:white;margin-left: 43px;" href="<?php echo base_url('c_report/bridge_reset_view/'); ?>">Reset Bridge Password</a></p>
                    </div>
<!--                    <div class="icon">
                        <img src="<?php echo base_url('images/ticket_r.png'); ?>">
                    </div>-->

                </div>
            </div>
            
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box " style="background-color:#645DAD;">
                    <div class="inner">

                        <p> <a style="color:white;margin-left: 43px;" href="<?php echo base_url('c_report/display_did_form/'); ?>">Add Bridge Did</a></p>
                    </div>
<!--                    <div class="icon">
                        <img src="<?php echo base_url('images/ticket_r.png'); ?>">
                    </div>-->

                </div>
            </div>
            
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box " style="background-color:#556EB7;">
                    <div class="inner">

                        <p> <a style="color:white;margin-left: 43px;" href="<?php echo base_url('c_report/display_contact_form/'); ?>">Add Contact</a></p>
                    </div>
<!--                    <div class="icon">
                        <img src="<?php echo base_url('images/ticket_r.png'); ?>">
                    </div>-->

                </div>
            </div>
            
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box " style="background-color:#4787AF;">
                    <div class="inner">

                        <p> <a style="color:white;margin-left: 43px;" href="<?php echo base_url('c_report/display_bridge_controlPanel/'); ?>">Bridge Contact Panel</a></p>
                    </div>
<!--                    <div class="icon">
                        <img src="<?php echo base_url('images/ticket_r.png'); ?>">
                    </div>-->

                </div>
            </div>
        </div>
</body>
</html>
