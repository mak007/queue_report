<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Lead Management</title>
        <script type="text/javascript">
            var BASEPATH = "<?php echo base_url(); ?>"
        </script>
        <script type="text/javascript">
            //   var user_id = 5;
        </script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-1.10.js'; ?>"></script>
        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-1.10.4.custom.css'; ?>"/>
<!--        <link rel="stylesheet" href="<?php echo $css_path . 'jquery-ui-timepicker-addon.css' ?>"/>
        -->        <link rel="stylesheet" href="<?php echo $css_path . 'agents.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/font-awesome.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/ionicons.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/AdminLTE.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/morris/morris.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/jvectormap/jquery-jvectormap-1.2.2.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/fullcalendar/fullcalendar.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo $css_path . 'css/daterangepicker/daterangepicker-bs3.css'; ?>"/>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-1.9.1.custom.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'jquery-ui-timepicker-addon.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/morris/morris.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/sparkline/jquery.sparkline.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/fullcalendar/fullcalendar.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/jqueryKnob/jquery.knob.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/daterangepicker/daterangepicker.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/plugins/iCheck/icheck.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo $js_path . 'js/AdminLTE/app.js'; ?>"></script>
        <!--<script type="text/javascript" src="<?php // echo $js_path . 'admin.js';  ?>"></script>-->
        <script src="<?php echo base_url('javascript/outbound_report_cdr.js'); ?>"></script>


        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>
        <style type="text/css">
            tr, td{
                border: 1px solid #c1c1c1;
                color: #444;
            }
            table{
                border: 1;
                border-radius:  0.5em;
                -moz-border-radius:  0.5em;
                border-collapse: collapse;
            }

            a{
                color: red;
                font-weight: bold;
            }
            a:hover{
                color: blue;
            } 
            .form-control {
                display: block;
                width: 40%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857;
                color: #555;
                vertical-align: middle;
                background-color: #FFF;
                background-image: none;
                border: 1px solid #CCC;
                border-radius: 4px;
            }
        </style>
        <script>
            $(function () {
                $("#datepickerEndDate").datetimepicker({
                    //format: "dd-mm-yyyy"
                });
                $('#datepickerEndDate').on('changeDate', function (ev) {
                    $(this).datetimepicker('hide');

                });

            });
        </script>
        <script>
            $(function () {
                $("#datepickerStartDate").datetimepicker({
                    //format: "dd-mm-yyyy"
                });

                $('#datepickerStartDate').on('changeDate', function (ev) {
                    $(this).datetimepicker('hide');

                });

            });


        </script>
    </head>
    <body class="skin-blue" onload="select_queue();get_extension_new();">
        <br></br>
        <aside class="right-side">  
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label>Queue</label>
                        <select multiple id="select_queue_list" class="form-control" style="width:200px;">


                        </select>
                    </div>

                    <div class="col-md-2" style="margin-left:20px;">
                        <label>Extension</label>
                        <select multiple id="extn_list" class="form-control" style="width:200px;">

                        </select>
                    </div>
                    <!--<div class="col-md-1"></div>-->
                    <div class="col-md-2" style="margin-left:30px;">
                        <label> Start Date & Time :</label>
                        <input style="width: 149px;" class="form-control" id="datepickerStartDate" name="datepickerStartDate" type="text">
                    </div>
                    <div class="col-md-2" style="margin-left:-20px;">
                        <label> End Date & Time :</label>
                        <input style="width: 149px;" class="form-control" id="datepickerEndDate" name="datepickerEndDate" type="text">
                    </div>
                    <div class="col-md-2">
                        <input type="text" style="display: none;" id="submit" name="submit" value=""/>
                    </div>
                    <div class="col-md-2">
                        <label> Call To Number (dst) :</label>
                        <input type="text" style="width: 149px;" onkeyUp="set_Callto_val(this.value);" class="form-control" id="call_to" name="call_to" >
                    </div>
                </div>
            </div>
        </aside>
        <br></br>
        <div id="each_call_report">
            <aside class="right-side">  
                <div class="container-fluid" style="margin:0 15px; background-color:#fff; box-shadow:0px 0px 5px #777; padding:15px;overflow-y:auto;  ">
                    <div class="row">
                        <div class="col-md-12">

                            <table id="tableEachCallReport" class="table table-bordered ">
                                <thead style="background-color: rgb(249,249,249); color: white;">
                                    <tr>
                                        <th>Report Name</th>
                                        <th>Required Field.</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_report">
                                                                         <tr>
                                        <td>Abandon Call Report</td>
                                        <td style="color: red;">
                                            * Start Date & Time required <br/>
                                            * End Date & Time required <br/>    
                                        </td>
                                        <td>
                                            <!--
                                            * Bellow $d1 to $d3 used for hidden field to store the Start,End and Call to number value
                                            * to pass controller
                                            -->
                                            <?php
//                                             $d1 = array(
//                                                'name' => 'datepickerStartDate',
//                                                'id' => 'StartDate_callrecived',
//                                                'value' => '',
//                                                'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
//                                            );
//                                            $d2 = array(
//                                                'name' => 'datepickerEndDate',
//                                                'id' => 'EndDate_callrecived',
//                                                'value' => '',
//                                                'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
//                                            );
//                                            echo form_open('c_report/generateCall_recivedExcel/');
//                                            echo form_input($d1);
//                                            echo form_input($d2);
//                                            echo form_input($d3);
//                                            '<br/>';
//                                            echo form_submit('call_received', 'Generate Excel');
//                                            echo form_close();
                                            ?>
                                            <button type="button" onclick="downloadAbandonCall();">Download Dump</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>After Working Hours Call Report</td>
                                        <td style="color: red;">* Start Date & Time required <br/>
                                            * End Date & Time required
                                        </td>
                                        <td>
                                            <?php
                                            $d1 = array(
                                                'name' => 'datepickerStartDate',
                                                'id' => 'StartDate',
                                                'value' => '',
                                                'maxlength' => '100',
                                                'size' => '50',
                                                'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                            );
                                            $d2 = array(
                                                'name' => 'datepickerEndDate',
                                                'id' => 'EndDate',
                                                'value' => '',
                                                'maxlength' => '100',
                                                'size' => '50',
                                                'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                            );
                                            $d3 = array(
                                                'name' => 'submit1',
                                                'id' => 'submit1',
                                                'value' => '',
                                                'maxlength' => '100',
                                                'size' => '50',
                                                'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                            );
                                            echo form_open('c_report/generateQueueExcel/');
                                            echo form_input($d1);
                                            echo form_input($d2);
                                            echo form_input($d3);
                                            '<br/>';
                                            echo form_submit('Login', 'Generate Excel');
                                            echo form_close();
                                            ?>

                                        </td>
                                    </tr>
                                    <!--==Added By Mohan on : 03-01-17--==
                                    * To display the Report Name and Required Field -->
                                    <tr>
                                        <td>Call Received (Specific Extension OR DID)</td>
                                        <td style="color: red;">
                                            * Start Date & Time required <br/>
                                            * End Date & Time required <br/>
                                            * Call To (dst)    
                                        </td>
                                        <td>
                                            <!--
                                            * Bellow $d1 to $d3 used for hidden field to store the Start,End and Call to number value
                                            * to pass controller
                                            -->
                                            <?php
                                             $d1 = array(
                                                'name' => 'datepickerStartDate',
                                                'id' => 'StartDate_callrecived',
                                                'value' => '',
                                                'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                            );
                                            $d2 = array(
                                                'name' => 'datepickerEndDate',
                                                'id' => 'EndDate_callrecived',
                                                'value' => '',
                                                'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                            );
                                            $d3 = array(
                                                'name' => 'callto',
                                                'id' => 'callto',
                                                'value' => '',
                                                'style' => 'display: none; border:1px solid #c1c1c1; width: 100px;',
                                            );
                                            echo form_open('c_report/generateCall_recivedExcel/');
                                            echo form_input($d1);
                                            echo form_input($d2);
                                            echo form_input($d3);
                                            '<br/>';
                                            echo form_submit('call_received', 'Generate Excel');
                                            echo form_close();
                                            ?>
                                        </td>
                                    </tr>
                                    <!--/*====End of Mohan code===*/-->
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </body>
</html>
